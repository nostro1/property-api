FROM php:7.3.20-apache

# Install modules
RUN apt-get update \
    && apt-get install -y \
        g++ \
        libicu-dev \
        libmcrypt-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libxml2-dev \
        libpq-dev \
        libzip-dev \
        zip \
        git-core
RUN pecl install mcrypt-1.0.3

# Configure extensions
RUN docker-php-ext-configure gd --with-freetype-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) intl \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-install -j$(nproc) pgsql \
    && docker-php-ext-install -j$(nproc) pdo_pgsql \
    && rm -rf /var/lib/apt/lists/*

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add apache config to enable .htaccess and do some stuff you want
COPY docker/apache/apache_default /etc/apache2/sites-available/000-default.conf

# Enable mod rewrite and listen to localhost
RUN a2enmod rewrite && \
	echo "ServerName localhost" >> /etc/apache2/apache2.conf

ENV APP_HOME /var/www/html
WORKDIR ${APP_HOME}

# Install project dependencies
COPY composer.* ${APP_HOME}/
RUN composer install && composer dump-autoload -o

COPY . ${APP_HOME}
CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]