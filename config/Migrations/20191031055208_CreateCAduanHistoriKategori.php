<?php
use Migrations\AbstractMigration;

class CreateCAduanHistoriKategori extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('c_aduan_histori_kategori');
        $table->addColumn('c_aduan_id', 'biginteger', [
            'null' => false,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
            'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('tgl_dibuat', 'date', [
            'null' => false,
        ]);
        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('tgl_diubah', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('kategori', 'string', [
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('dari', 'timestamp', [
            'null' => false,
        ]);
        $table->addColumn('sampai', 'timestamp', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();

        $table = $this->table('c_aduan_histori_assignment');
        $table->addColumn('c_aduan_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
            'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('tgl_dibuat', 'date', [
            'null' => false
        ]);
        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('tgl_diubah', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('pegawai_id', 'biginteger', [
            'null' => true,
        ]);
        $table->addColumn('penanggung_jawab', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('dari', 'timestamp', [
            'null' => false,
        ]);
        $table->addColumn('sampai', 'timestamp', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();

        $table = $this->table('c_aduan_histori_status');
        $table->addColumn('c_aduan_id', 'biginteger', [
            'null' => false,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
            'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('tgl_dibuat', 'date', [
            'null' => false,
        ]);
        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('tgl_diubah', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('status', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('dari', 'timestamp', [
            'null' => false,
        ]);
        $table->addColumn('sampai', 'timestamp', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
