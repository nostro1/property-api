<?php
use Migrations\AbstractMigration;

class AddChanelToCAduan extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('c_aduan');
        $table->addColumn('pegawai_id', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => true,
        ]);
        $table->addColumn('chanel', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => true,
        ]);
        $table->update();
    }
}
