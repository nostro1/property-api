<?php
use Migrations\AbstractMigration;

class AddIsKeyToDataSincDetail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('data_sinc_detail');
        $table->addColumn('is_key', 'string', [
            'default' => null,
            'limit' => 2,
            'null' => true,
        ]);
        $table->addColumn('value_type', 'string', [
            'default' => 'ref_index',
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('maping', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->update();
    }
}
