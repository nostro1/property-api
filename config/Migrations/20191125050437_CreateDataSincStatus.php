<?php
use Migrations\AbstractMigration;

class CreateDataSincStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('data_sinc_status');
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('object_id', 'biginteger', [
            'null' => false,
        ]);
        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
           'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('tgl_dibuat', 'date', [
            'null' => false,
        ]);
        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('tgl_diubah', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('status', 'boolean', [
           'default' => true,
           'null' => false,
        ]);
        $table->addColumn('detail', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
        
        $table = $this->table('data_sinc_log');
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('data_sinc_id', 'biginteger', [
            'null' => false,
        ]);
        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
           'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('tgl_dibuat', 'date', [
            'null' => false,
        ]);
        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        $table->addColumn('tgl_diubah', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('log', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
