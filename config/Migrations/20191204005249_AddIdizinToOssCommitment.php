<?php
use Migrations\AbstractMigration;

class AddIdizinToOssCommitment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('oss_commitment');
        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('id_izin', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);
        $table->update();
        
        $table = $this->table('oss_lampiran');
        
        $table->addColumn('oss_commitment_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('oss_documment_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
