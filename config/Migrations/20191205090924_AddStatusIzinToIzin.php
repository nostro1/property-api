<?php
use Migrations\AbstractMigration;

class AddStatusIzinToIzin extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('izin');
        $table->addColumn('status_izin', 'string', [
            'default' => 'valid',
            'limit' => 25,
            'null' => false,
        ]);
        $table->addColumn('keterangan_status', 'string', [
            'limit' => 1000,
            'null' => true,
        ]);
        $table->update();
    }
}
