<?php
use Migrations\AbstractMigration;

class AddTokenToServiceEksternal extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('service_eksternal');
        $table->addColumn('token', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        $table->addColumn('waktu_berlaku_token', 'datetime', [
            'null' => true,
        ]);
        $table->update();
    }
}
