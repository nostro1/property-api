<?php
use Migrations\AbstractMigration;

class CreateSubmissionStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('oss_submission_status');
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('nib_id', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('oss_id', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('kode_izin', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('id_izin', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('kode_daerah', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('status_permohonan', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('tgl_status', 'date', [
            'null' => true,
        ]);
        
        $table->create();
    }
}
