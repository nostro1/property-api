<?php
use Migrations\AbstractMigration;

class CreateSendPtp extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('oss_send_ptp');
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('nib_id', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('oss_id', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('kode_izin', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('id_izin', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('kode_daerah', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('nomor_ptp', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('tgl_terbit_ptp', 'date', [
            'null' => true,
        ]);

        $table->addColumn('status_ptp', 'string', [
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('pemohon', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('kantah_kabupaten', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('nama_provinsi', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('keterangan', 'string', [
            'limit' => 100,
            'null' => true,
        ]);

        $table->create();
        
        $table = $this->table('oss_lampiran');
        $table->addColumn('oss_send_ptp_id', 'biginteger', [
            'null' => true,
        ]);
        $table->update();
    }
}
