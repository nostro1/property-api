<?php
use Migrations\AbstractMigration;

class AddDataIdToPesan extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pesan');
        $table->addColumn('template_id', 'biginteger', [ //template data id & form_id
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('proses_id', 'biginteger', [//proses_permohonan_id
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
