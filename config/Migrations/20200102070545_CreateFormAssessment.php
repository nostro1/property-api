<?php
use Migrations\AbstractMigration;

class CreateFormAssessment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        //Begin Template form assessment
        $table = $this->table('form_assessment');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('description', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('assesor_type', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('assessment_type', 'string', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->create();

        //Detail question
        $table = $this->table('form_assessment_question');

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('question_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('form_assessment_id', 'biginteger', [
            'null' => false,
        ]);

        $table->addColumn('sequence', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();

        //Master Question
        $table = $this->table('question');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('question', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);

        $table->addColumn('options_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('answer', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);
        $table->create();
        //End Template form assessment

        //Begin Assessment Event
        $table = $this->table('assessment_event');
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('form_assessment_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('assesor_type', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('publish_begin', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('publish_end', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('status', 'string', [
            'default' => 'AKTIF',
            'limit' => 100,
            'null' => true,
        ]);

        $table->create();

        //Participant list
        $table = $this->table('assessment_participant');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('assessment_event_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('participant_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('participant_name', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('assessment_date', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->create();

        //participant respon/result
        $table = $this->table('assessment_participant_result');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('assessment_participant_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('assessment_question_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('answer', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->create();
    }
}
