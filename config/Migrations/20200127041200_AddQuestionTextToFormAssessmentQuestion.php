<?php
use Migrations\AbstractMigration;

class AddQuestionTextToFormAssessmentQuestion extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('form_assessment_question');

        $table->addColumn('question_text', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);

        $table->addColumn('answer_type', 'string', [
            'default' => 'option',
            'limit' => 100,
            'null' => false,
        ]);

        $table->update();
    }
}
