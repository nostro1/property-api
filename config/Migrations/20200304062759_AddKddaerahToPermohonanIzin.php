<?php
use Migrations\AbstractMigration;

class AddKddaerahToPermohonanIzin extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('permohonan_izin');
        $table->addColumn('kd_daerah', 'string', [
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
