<?php
use Migrations\AbstractMigration;

class AddProsesToDataSincStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('data_sinc_status');
        $table->addColumn('proses', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('object_id2', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
