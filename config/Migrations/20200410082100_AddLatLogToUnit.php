<?php
use Migrations\AbstractMigration;

class AddLatLogToUnit extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('unit');
        $table->addColumn('lat', 'string', [
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('long', 'string', [
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
