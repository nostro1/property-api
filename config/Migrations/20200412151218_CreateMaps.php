<?php
use Migrations\AbstractMigration;

class CreateMaps extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('maps');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ])->addForeignKey('instansi_id', 'unit', 'id', [
            'delete'=> 'RESTRICT',
            'update'=> 'NO_ACTION'
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ])->addIndex('del');

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'limit' => 25,
            'null' => false,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'limit' => 20,
            'null' => false,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('title', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('module', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])->addIndex(['module', 'instansi_id']);

        $table->addColumn('lat', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('long', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->create();
    }
}
