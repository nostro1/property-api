<?php
use Migrations\AbstractMigration;

class CreateMapLayers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('map_layers');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ])->addForeignKey('instansi_id', 'unit', 'id', [
            'delete'=> 'RESTRICT',
            'update'=> 'NO_ACTION'
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ])->addIndex('del');

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'limit' => 25,
            'null' => false,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'limit' => 20,
            'null' => false,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('maps_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ])->addForeignKey('maps_id', 'maps', 'id', [
            'delete'=> 'RESTRICT',
            'update'=> 'NO_ACTION'
        ]);

        $table->addColumn('layer', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('layer_type', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('layer_label', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('url', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('layer_option', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->create();
    }
}
