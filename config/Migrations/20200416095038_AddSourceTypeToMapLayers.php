<?php
use Migrations\AbstractMigration;

class AddSourceTypeToMapLayers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('map_layers');
        $table->addColumn('source_type', 'string', [
            'default' => 'geojson',
            'limit' => 255,
            'null' => false,
        ]);
        $table->update();
    }
}
