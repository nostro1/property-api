<?php
use Migrations\AbstractMigration;

class AlterDataSincDetailChangeOssKolom extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('data_sinc_detail');
        $table->changeColumn('oss_kolom', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        $table->update();
    }
}
