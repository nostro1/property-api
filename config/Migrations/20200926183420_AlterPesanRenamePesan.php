<?php
use Cake\Datasource\ConnectionManager;
use Migrations\AbstractMigration;

class AlterPesanRenamePesan extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pesan');
        $table->renameColumn('pesan', 'isi');
        $table->update();

        $conn = ConnectionManager::get('default');
        $sql = "
            UPDATE data_kolom AS dk
            SET data_kolom = 'isi', label='isi'
            FROM datatabel AS d
            WHERE
                d.id = dk.datatabel_id
                AND dk.data_kolom='pesan'
                AND d.nama_datatabel = 'pesan'
        ";
        $stmt = $conn->execute($sql);
    }
}
