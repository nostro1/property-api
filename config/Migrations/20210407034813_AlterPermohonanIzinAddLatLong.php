<?php
use Migrations\AbstractMigration;

class AlterPermohonanIzinAddLatLong extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('permohonan_izin');

        $table->addColumn('lat', 'string', [
            'default' => null,
            'limit' => 500,
            'null' => true
        ]);

        $table->addColumn('long', 'string', [
            'default' => null,
            'limit' => 500,
            'null' => true
        ]);

        $table->update();
    }
}
