<?php
use Migrations\AbstractMigration;

class AlterJenisIzinAddDel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('jenis_izin');

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'limit'   => 16,
            'null'    => true,
        ]);
        $table->update();
    }
}
