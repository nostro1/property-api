<?php
use Migrations\AbstractMigration;

class AlterPenanggungJawabPeranIsnull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('penanggung_jawab_peran');
        $table->changeColumn('instansi_id', 'biginteger', [
            'null' => true,
        ]);
        $table->update();
    }
}
