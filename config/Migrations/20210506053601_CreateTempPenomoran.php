<?php
use Migrations\AbstractMigration;

class CreateTempPenomoran extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_penomoran');
        $table->addColumn('no_permohonan', 'string', [
            'default' => null,
            'limit'   => 50,
            'null'    => false,
        ]);

        $table->addColumn('pengguna_id', 'integer', [
            'default' => null,
            'null'    => true,
        ]);

        $table->addColumn('jenis_izin_id', 'integer', [
            'default' => null,
            'null'    => true,
        ]);

        $table->create();
    }
}
