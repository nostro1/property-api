<?php
use Migrations\AbstractMigration;

class AddIsPermitToJenisIzin extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('jenis_izin');
        $table->addColumn('is_permit', 'boolean', [
            'default' => false
        ]);
        $table->update();
    }
}
