<?php
use Migrations\AbstractMigration;

class CreateDokumenShare extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('document_share');

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('document_repository_id', 'biginteger', [
            'null' => false,
        ]);

        $table->addColumn('pengguna_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('is_public', 'boolean', [
            'default' => false,
            'null' => true,
        ]);

        $table->create();

        $table = $this->table('document_assign');

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('document_repository_id', 'biginteger', [            
            'null' => false,
        ]);

        $table->addColumn('pengguna_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ]);

        $table->addColumn('status', 'string', [
            'default' => 'open',
            'limit' => 20,
            'null' => false,
        ]);

        $table->create();

        $table = $this->table('document_comment');

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('document_repository_id', 'biginteger', [            
            'null' => false,
        ]);

        $table->addColumn('pengguna_id', 'biginteger', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('comment', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);

        $table->create();
    }
}
