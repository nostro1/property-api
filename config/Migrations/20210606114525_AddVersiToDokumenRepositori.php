<?php
use Migrations\AbstractMigration;

class AddVersiToDokumenRepositori extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('document_repository');
        $table->addColumn('version', 'string', [
            'default' => '1.0',
            'limit' => 10,
            'null' => true,
        ]);
        $table->update();
    }
}
