<?php
use Migrations\AbstractMigration;

class AddChanelToServiceEksternal extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('service_eksternal');

        $table->addColumn('chanel', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('status', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => true,
        ]);

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->update();
    }
}
