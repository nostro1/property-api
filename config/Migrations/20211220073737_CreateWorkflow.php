<?php
use Migrations\AbstractMigration;

class CreateWorkflow extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('workflow');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        
        $table->addColumn('objek_tipe', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('proses_tipe', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);
        $table->addColumn('objek_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('jenis_proses_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('nama_proses', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('tautan', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        
        $table->addColumn('form_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('template_data_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('pengguna_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('flow_type', 'string', [
            'default' => 'approver', // 'reviewer'
            'limit' => 25,
            'null' => true,
        ]);


        $table->addColumn('status', 'string', [
            'default' => 'hold',
            'limit' => 25,
            'null' => true,
        ]);
        

        $table->create();
    }
}
