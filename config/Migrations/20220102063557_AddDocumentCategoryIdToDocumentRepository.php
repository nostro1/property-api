<?php
use Migrations\AbstractMigration;

class AddDocumentCategoryIdToDocumentRepository extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('document_repository');
        $table->addColumn('document_category_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
