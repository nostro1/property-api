<?php
use Migrations\AbstractMigration;

class CreateMail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('mail');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('mail_category', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('mail_type', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('mail_no', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => true,
        ]);

        $table->addColumn('mail_subject', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('mail_to', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        
        $table->addColumn('mail_from', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        
        $table->addColumn('content_formated', 'string', [
            'default' => null,
            'limit' => 10000,
            'null' => true,
        ]);

        
        $table->addColumn('content', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        
        $table->create();

        $table = $this->table('document_repository');
        
        $table->addColumn('mail_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
