<?php
use Migrations\AbstractMigration;

class CreateTaskManagement extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('task_management');
        
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('label', 'string', [
            'default' => null,
            'limit' => 10000,
            'null' => true,
        ]);

        $table->addColumn('tgl_mulai', 'datetime', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('tgl_selesai', 'datetime', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('jumlah', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('lft_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('rght_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('parent_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();
    }
}
