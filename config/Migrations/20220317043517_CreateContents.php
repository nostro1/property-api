<?php
use Migrations\AbstractMigration;

class CreateContents extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('contents');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('title', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);
        
        $table->addColumn('content_category_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('status', 'string', [
            'default' => 'draft',
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('publish_begin', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('publish_end', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => true,
        ]);

        
        $table->addColumn('short_desc', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();

        $table = $this->table('content_category');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => false,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('category_code', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);

        $table->addColumn('lft_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('rght_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('parent_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('category_label', 'string', [
            'default' => null,
            'limit' => 500,
            'null' => false,
        ]);

        $table->create();

        $table = $this->table('content_detail');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('content_path', 'string', [
            'limit' => 1000,
            'null' => false,
        ]);        
        
        $table->addColumn('content_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('short_desc', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();
    }
}
