<?php
use Migrations\AbstractMigration;

class CreateDocumentSloc extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('document_sloc');
        
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('document_repository_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ]);

        $table->addColumn('tipe_pekerjaan', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('unit_kerja', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('no_box_lama', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('no_box', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('klasifikasi_arsip', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('document_Category', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('kode_arsip', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('keterangan', 'string', [
            'default' => null,
            'limit' => 10000,
            'null' => true,
        ]);

        $table->addColumn('tahun_dari', 'string', [
            'default' => null,
            'limit' => 4,
            'null' => true,
        ]);

        $table->addColumn('tahun_sampai', 'string', [
            'default' => null,
            'limit' => 4,
            'null' => true,
        ]);

        $table->addColumn('media', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tingkat_keaslian', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('jumlah', 'biginteger', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('satuan', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('kondisi', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('foto_sebelum', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);

        $table->addColumn('foto_sesudah', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);

        $table->create();
    }
}
