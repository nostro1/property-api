<?php
use Migrations\AbstractMigration;

class AddSlugToContent extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('contents');

        $table->addColumn('slug', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => false,
        ]);

        $table->addColumn('img_cover', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);

        $table->update();

        $table = $this->table('content_comment');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);      
        
        $table->addColumn('content_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('pengguna_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        
        $table->addColumn('comment', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();
    }
}
