<?php
use Migrations\AbstractMigration;

class CreateBulkProcess extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bulk_process');
        
        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('process_type', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('status', 'string', [
            'default' => 'inprogres',
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('number_of_thread', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('total_process', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('running_process', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->create();
    }
}
