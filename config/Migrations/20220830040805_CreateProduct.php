<?php
use Migrations\AbstractMigration;

class CreateProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('product');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('data_labels', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('product_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('product_type', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('description', 'string', [
            'default' => null,
            'limit' => 10000,
            'null' => true,
        ]);
        
        $table->create();

        $table = $this->table('product_image');

        $table->addColumn('product_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('image_src', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('caption', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);
        
        $table->create();

        $table = $this->table('ticket');

        $table->addColumn('product_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('venue', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('type', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('available_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('duration', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('unit', 'string', [
            'default' => 'day',
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('price', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('quantity', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('booked', 'integer', [
            'default' => 0,
            'null' => true,
        ]);
        
        $table->create();

        $table = $this->table('product_price');

        $table->addColumn('product_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        
        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('price_cathegory_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('begin_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('end_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('unit', 'string', [
            'default' => '',
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('price', 'integer', [
            'default' => 0,
            'null' => true,
        ]);
        
        $table->create();
    }
}
