<?php
use Migrations\AbstractMigration;

class CreatePriceCathegory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('price_cathegory');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('price_cathegory', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);

        $table->addColumn('begin_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('end_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('label1', 'string', [
            'default' => '',
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('label2', 'string', [
            'default' => '',
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('label3', 'string', [
            'default' => '',
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('label4', 'string', [
            'default' => '',
            'limit' => 255,
            'null' => true,
        ]);

        $table->create();
    }
}
