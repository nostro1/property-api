<?php
use Migrations\AbstractMigration;

class CreatePerson extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        //PERSON
        $table = $this->table('person');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('idcard', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('address', 'string', [
            'default' => null,
            'limit' => 1000,
            'null' => true,
        ]);

        $table->addColumn('contact', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('otp', 'string', [
            'default' => null,
            'limit' => 6,
            'null' => true,
        ]);

        $table->create();

        //BILLING
        $table = $this->table('billing');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        
        $table->addColumn('person_id', 'biginteger', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('periode', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('status', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('total', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('due_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('payment_date', 'date', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('paid', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('tax', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();

        //BILLING DETAIL
        $table = $this->table('billing_detail');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);
        
        $table->addColumn('billing_id', 'biginteger', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('billing_item', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('qty', 'integer', [
            'default' => 1,
            'null' => true,
        ]);

        $table->addColumn('unit', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);

        $table->addColumn('amount', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();
    }
}
