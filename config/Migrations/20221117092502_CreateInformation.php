<?php
use Migrations\AbstractMigration;

class CreateInformation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('information');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('title', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        

        $table->addColumn('type', 'string', [
            'default' => 'announcement',
            'limit' => 25,
            'null' => true,
        ]);



        $table->addColumn('content', 'string', [
            'default' => null,
            'limit' => 10000,
            'null' => true,
        ]);

        $table->addColumn('picture', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('status', 'string', [
            'default' => 'draft',
            'limit' => 25,
            'null' => true,
        ]);

        $table->create();
    }
}
