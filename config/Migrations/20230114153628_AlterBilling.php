<?php
use Migrations\AbstractMigration;

class AlterBilling extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('billing');
        
        $table->addColumn('year', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        //harian, bulanan
        $table->addColumn('charging_type', 'string', [
            'default' => 'bulanan',
            'limit' => 25,
            'null' => true,
        ]);

        $table->update();

    }
}
