<?php
use Migrations\AbstractMigration;

class CreateBooking extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('booking');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('person_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);
        //menentukan tipe object yang dapat di booking
        $table->addColumn('object_type', 'string', [
            'default' => 'space',
            'limit' => 50,
            'null' => true,
        ]);

        $table->addColumn('object_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('begin', 'datetime', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('end', 'datetime', [
            'default' => null,
            'null' => true,
        ]);


        $table->create();

        $table = $this->table('property_unit');
        
        $table->addColumn('public_area', 'boolean', [
            'default' => 'false',
            'null' => true,
        ]);
        
        $table->addColumn('is_free', 'boolean', [
            'default' => 'false',
            'null' => true,
        ]);
        
        $table->addColumn('need_booking', 'boolean', [
            'default' => 'false',
            'null' => true,
        ]);

        $table->update();

        $table = $this->table('person_project');

        $table->addColumn('instansi_id', 'biginteger', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('del', 'integer', [
            'default' => 0,
            'null' => true,
        ]);

        $table->addColumn('dibuat_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('tgl_dibuat', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('tgl_diubah', 'datetime', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);

        $table->addColumn('diubah_oleh', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->addColumn('person_id', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('project_id', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('credit', 'integer', [
            'default' => null,
            'limit' => 25,
            'null' => true,
        ]);

        $table->create();
    }
}
