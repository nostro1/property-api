<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

class HouseKeepingCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('-----Start housekeeping tasks-----');

        $io->out('Rebuilding unit tree...');
        $this->rebuildTree();

        $io->out('-----Finish housekeeping tasks-----');
    }

    private function rebuildTree()
{
        $unitTable = TableRegistry::getTableLocator()->get('Unit');
        $unitTable->recover();
    }
}