<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Billing Controller
 *
 * @property \App\Model\Table\BillingTable $Billing
 *
 * @method \App\Model\Entity\Billing[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillingController extends ApiController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Billing->setInstansi($this->getCurrentInstansi());
        $this->Billing->setUser($this->getCurrentUser());
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        
        $this->paginate = [
            'contain' => ['Unit', 'Person'],
            'conditions' => [
                'OR' => [
                    'LOWER(Billing.periode) ILIKE' => '%' . $this->_apiQueryString . '%'
                ]
            ]
        ];

        $billing = $this->paginate($this->Billing);
        $paging = $this->request->params['paging']['Billing'];
        $billing = $this->addRowNumber($billing);
        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $billing,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Billing id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $billing = $this->Billing->get($id, [
            'contain' => ['Unit', 'Person', 'BillingDetail']
        ]);

        $success = true;
        $message = '';
       
        $this->setResponseData($billing, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';

        $billing = $this->Billing->newEntity();
        if ($this->request->is('post')) {
            $billing = $this->Billing->patchEntity($billing, $this->request->getData());
            if ($this->Billing->save($billing)) {
                $success = true;
                $message = __('Billing berhasil disimpan.');
            } else {
                debug($billing);exit();
                $message = __('Billing tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($billing, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Billing id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $billing = $this->Billing->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $billing = $this->Billing->patchEntity($billing, $this->request->getData());
            if ($this->Billing->save($billing)) {
                $this->Flash->success(__('The billing has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The billing could not be saved. Please, try again.'));
        }
        $instansis = $this->Billing->Instansis->find('list', ['limit' => 200]);
        $Person = $this->Billing->Person->find('list', ['limit' => 200]);
        $this->set(compact('billing', 'instansis', 'Person'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Billing id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $billing = $this->Billing->get($id);
        if ($this->Billing->delete($billing)) {
            $this->Flash->success(__('The billing has been deleted.'));
        } else {
            $this->Flash->error(__('The billing could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function billingList ($id = null){
        $success = true;
        $message = '';
        
        $this->paginate = [
            'contain' => ['Unit', 'Person'],
            'conditions' => [
                'Billing.person_id' => $id
            ]
        ];

        $billing = $this->paginate($this->Billing);
        $paging = $this->request->params['paging']['Billing'];
        $billing = $this->addRowNumber($billing);
        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $billing,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }
}
