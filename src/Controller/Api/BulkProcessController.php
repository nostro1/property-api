<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * BulkProcess Controller
 *
 * @property \App\Model\Table\BulkProcessTable $bulkProcesss
 *
 * @method \App\Model\Entity\BulkProces[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BulkProcessController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
        //$this->Auth->allow(['getWebsite', 'List', 'categoryByLabel', 'hierarchyByLabel']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Unit']
        ];
        $bulkProcesss = $this->paginate($this->BulkProcess);

        $this->set(compact('bulkProcess'));
    }

    /**
     * View method
     *
     * @param string|null $id Bulk Proces id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bulkProcess = $this->BulkProcess->get($id, [
            'contain' => ['Unit']
        ]);

        $this->set('bulkProces', $bulkProcess);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->BulkProcess->setInstansi($this->getCurrentInstansi());

        $bulkProcess = $this->BulkProcess->newEntity();

        if ($this->request->is('post')) {
            $bulkProcess = $this->BulkProcess->patchEntity($bulkProcess, $this->request->getData());
            $bulkProcess->del = 0;
            if ($this->BulkProcess->save($bulkProcess)) {
                $success = true;
                $message = __('Bulk process created');
            } else {
                $message = __('Error create bulk process');
            }
        }
        
        $this->setResponseData($bulkProcess, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bulk Proces id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $data = null)
    {
        $success = false;
        $message = '';
        $this->BulkProcess->setInstansi($this->getCurrentInstansi());

        if(!$data) {
            $editData = $data;
        }else{
            $editData = $this->request->getData();
        }

        $bulkProcess = $this->BulkProcess->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bulkProcess = $this->BulkProcess->patchEntity($bulkProcess, $editData);
            if ($this->BulkProcess->save($bulkProcess)) {
                $success = true;
                $message = __('Bulk process created');
            } else {
                $message = __('Error create bulk process');
            }
        }
        
        $this->setResponseData($bulkProcess, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bulk Proces id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bulkProcess = $this->BulkProcess->get($id);
        if ($this->BulkProcess->delete($bulkProcess)) {
            $this->Flash->success(__('The bulk proces has been deleted.'));
        } else {
            $this->Flash->error(__('The bulk proces could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getActiveProc($processType = null)
    {
        $success = true;
        $message = '';

        $bulkProcess = $this->BulkProcess->find('all')
            ->where([
                'process_type' => $processType,
                'status' => 'running', 
                'del' => 0
            ])
            ->order(['id' => 'DESC'])
            ->limit(1);

        $this->setResponseData($bulkProcess, $success, $message);
    }

    public function setTotalProcess($id = null, $jumlah = 0)
    {
        $success = false;
        $message = '';
        $this->BulkProcess->setInstansi($this->getCurrentInstansi());

        $editData = ['id' => $id];

        $bulkProcess = $this->BulkProcess->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bulkProcess = $this->BulkProcess->patchEntity($bulkProcess, $editData);
            $bulkProcess->total_process = $bulkProcess->total_process + $jumlah;
            if ($this->BulkProcess->save($bulkProcess)) {
                $success = true;
                $message = __('Bulk process updated');
            } else {
                $message = __('Error update bulk process');
            }
        }
        
        //$this->setResponseData($bulkProcess, $success, $message);
    }

    public function setRunningProc($id = null)
    {
        $success = false;
        $message = '';
        $this->BulkProcess->setInstansi($this->getCurrentInstansi());

        $editData = ['id' => $id];

        $bulkProcess = $this->BulkProcess->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bulkProcess = $this->BulkProcess->patchEntity($bulkProcess, $editData);
            $bulkProcess->running_process = $bulkProcess->running_process + 1;
            if ($this->BulkProcess->save($bulkProcess)) {
                $success = true;
                $message = __('Bulk process updated');
            } else {
                $message = __('Error update bulk process');
            }
        }
        
        //$this->setResponseData($bulkProcess, $success, $message);
    }

    public function stopProc($id = null)
    {
        $success = false;
        $message = '';
        $this->BulkProcess->setInstansi($this->getCurrentInstansi());

        $editData = ['id' => $id];

        $bulkProcess = $this->BulkProcess->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bulkProcess = $this->BulkProcess->patchEntity($bulkProcess, $editData);
            $bulkProcess->status = 'finish';
            if ($this->BulkProcess->save($bulkProcess)) {
                $success = true;
                $message = __('Bulk process updated');
            } else {
                $message = __('Error update bulk process');
            }
        }
        
        $this->setResponseData($bulkProcess, $success, $message);
    }
}
