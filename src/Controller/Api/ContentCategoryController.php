<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * ContentCategory Controller
 *
 * @property \App\Model\Table\ContentCategoryTable $ContentCategory
 *
 * @method \App\Model\Entity\ContentCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentCategoryController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['getWebsite', 'List', 'categoryByLabel', 'hierarchyByLabel']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());
        $category_id = $this->request->getQuery('content_category_id') ? $this->request->getQuery('content_category_id') : null;

        $conditions = [
            'ContentCategory.del' => 0,
            'OR' => [
                'ContentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['ContentCategory.parent_id' => $category_id]);
        } else {
            array_push($conditions, ['ContentCategory.parent_id is' => null]);
        }
        
        // Expanded default values
        /* $query = $this->ContentCategory->find('threaded', [
            'keyField' => $this->ContentCategory->primaryKey(),
            'parentField' => 'parent_id'
        ]); */
        $this->paginate = [
            'contain' => ['ChildContentCategory', 'Contents'],
            'conditions' => $conditions,
        ];

        //$contentCategories = $query->toArray();
        $contentCategories = $this->paginate($this->ContentCategory);
        $paging = $this->request->params['paging']['ContentCategory'];
        $contentCategories = $this->addRowNumber($contentCategories);
 
        $this->setResponseData($contentCategories, $success, $message);
    }

    public function getWebsite()
    {
        $success = true;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());
        $category_id = $this->request->getQuery('content_category_id') ? $this->request->getQuery('content_category_id') : null;

        $conditions = [
            'ContentCategory.del' => 0,
            'OR' => [
                'ContentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['ContentCategory.parent_id' => $category_id]);
        } else {
            array_push($conditions, ['ContentCategory.parent_id is' => null]);
        }
        
        $this->paginate = [
            'contain' => ['ChildContentCategory', 'Contents'],
            'conditions' => $conditions,
        ];

        //$contentCategories = $query->toArray();
        $contentCategories = $this->paginate($this->ContentCategory);
        $paging = $this->request->params['paging']['ContentCategory'];
        $contentCategories = $this->addRowNumber($contentCategories);
 
        $this->setResponseData($contentCategories, $success, $message);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function list()
    {
        $success = true;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;
        $category_id = $this->request->getQuery('content_category_id') ? $this->request->getQuery('content_category_id') : null;

        $conditions = [
            'ContentCategory.del' => 0,
            'OR' => [
                'ContentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['ContentCategory.parent_id' => $category_id]);
        } else {
            array_push($conditions, ['ContentCategory.parent_id is' => null]);
        }
        
        $contentCategory = $this->ContentCategory->find('all', [
            'fields' => ['id', 'category_code', 'category_label', 'instansi_id'],
            'contain' => ['Contents'],
            'conditions' => $conditions
        ]);

        $data = array(
            'items' => $contentCategory
        );
 
        $this->setResponseData($data, $success, $message);
    }

    public function listAC()
    {
        $success = true;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());

        $conditions = [
            'ContentCategory.del' => 0,
            'OR' => [
                'ContentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];
        
        $contentCategory = $this->ContentCategory->find('all', [
            'fields' => ['id', 'category_code', 'category_label', 'instansi_id'],
            'conditions' => $conditions
        ]);

        $data = array(
            'items' => $contentCategory
        );
 
        $this->setResponseData($data, $success, $message);
    }
    /**
     * View method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $contentCategory = $this->ContentCategory->get($id, [
            'contain' => []
        ]);

        $this->setResponseData($contentCategory, $success, $message);
    }

    public function categoryByLabel($label = null)
    {
        $success = true;
        $message = '';

        $this->ContentCategory->setInstansi($this->getCurrentInstansi());
        
        $contentCategory = $this->ContentCategory->find('all', [
            'fields' => ['id', 'category_code', 'category_label', 'instansi_id'],
            'contain' => [
                'Contents' =>  function (\Cake\ORM\Query $query) {
                    return $query
                        ->where(['Contents.status' => 'publish'])
                        ->order(['Contents.publish_begin ' => 'DESC'])
                        ->limit(5);
                }
            ],
            'conditions' => [
                'ContentCategory.del' => 0,
                'ContentCategory.slug' => $label,
            ]
        ]);

        $data = array(
            'items' => $contentCategory
        );
 
        $this->setResponseData($data, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());

        $contentCategory = $this->ContentCategory->newEntity();

        if ($this->request->is('post')) {
            $contentCategory = $this->ContentCategory->patchEntity($contentCategory, $this->request->getData());
            $contentCategory->del = 0;
            if ($this->ContentCategory->save($contentCategory)) {
                $success = true;
                $message = __('Kategori berhasil disimpan');
            } else {
                $message = __('Kategori tidak berhasil disimpan');
            }
        }
        
        $this->setResponseData($contentCategory, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';
        $this->ContentCategory->setInstansi($this->getCurrentInstansi());
        
        $contentCategory = $this->ContentCategory->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentCategory = $this->ContentCategory->patchEntity($contentCategory, $this->request->getData());
            if ($this->ContentCategory->save($contentCategory)) {
                $success = true;
                $message = __('Kategori berhasil dirubah');
            } else {
                $message = __('Kategori tidak berhasil dirubah');
            }
        }
        
        $this->setResponseData($contentCategory, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $success = true;
        $message = '';

        $this->request->allowMethod(['post', 'delete']);

        $contentCategory = $this->ContentCategory->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentCategory = $this->ContentCategory->patchEntity($contentCategory, $this->request->getData());
            if ($this->ContentCategory->save($contentCategory)) {
                $success = true;
                $message = __('Kategori berhasil di hapus');
            } else {
                $message = __('Kategori gagal di hapus');
            }
        }
        
        $this->setResponseData($contentCategory, $success, $message);
    }

    public function getHierarchy()
    {
        $success = true;
        $message = '';
        $data = array();
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;

        $conditions = [
            'ContentCategory.del' => 0,
            'OR' => [
                'ContentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($kategori)) {
            array_push($conditions, ['LOWER(ContentCategory.category_label)' => $kategori]);
        }
        // Get All Menu
        $allCategory = $this->ContentCategory->find('threaded', [
            'fields' => [
                'ContentCategory.id',
                'label' => 'ContentCategory.category_label',
                'ContentCategory.parent_id',
            ],
            'coditions' => $conditions,
            'order' => [
                'ContentCategory.category_label' => 'ASC',
            ],
        ]);

        $allDocumentCategory = $allCategory->toArray();
        $data['hierarchy'] = $allDocumentCategory;

        $this->setResponseData($data, $success, $message);
    }

    public function hierarchyByLabel($label = null)
    {
        $success = true;
        $message = '';
        $data = array();
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;

        if(!$label) {
            $conditions = [
                'ContentCategory.del' => 0,
                'ContentCategory.slug' => $label
            ];
        } else {
            $conditions = [
                'ContentCategory.del' => 0,
                'ContentCategory.slug is' => null
            ];
        }
        

        if(!is_null($kategori)) {
            array_push($conditions, ['LOWER(ContentCategory.category_label)' => $kategori]);
        }
        // Get All Menu
        $allCategory = $this->ContentCategory->find('threaded', [
            'contain' => [
                'Contents' =>  function (\Cake\ORM\Query $query) {
                    return $query
                        ->where(['Contents.status' => 'publish'])
                        ->order(['Contents.publish_begin ' => 'DESC'])
                        ->limit(5);
                }],
            'coditions' => $conditions,
            'order' => [
                'ContentCategory.category_label' => 'ASC',
            ],
        ]);

        $allDocumentCategory = $allCategory->toArray();
        $data['hierarchy'] = $allDocumentCategory;

        $this->setResponseData($data, $success, $message);
    }

}
