<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Contents Controller
 *
 * @property \App\Model\Table\ContentsTable $Contents
 *
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['viewBySlug', 'latestContent']);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->Contents->setInstansi($this->getCurrentInstansi());

        $conditions = [
            'Contents.del' => 0,
            'OR' => [
                'Contents.title ILIKE' => '%' . $this->_apiQueryString . '%',
                'Contents.content ILIKE' => '%' . $this->_apiQueryString . '%',
                'Contents.short_desc ILIKE' => '%' . $this->_apiQueryString . '%',
                'ContentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%'
            ]
        ];

        $this->paginate = [
            'contain' => ['Unit', 'ContentCategory'],
            'conditions' => $conditions,
            'order' => [
                'Contents.id DESC'
            ]
        ];

        $contents = $this->paginate($this->Contents);
        $paging = $this->request->params['paging']['Contents'];
        $contents = $this->addRowNumber($contents);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $contents,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $content = $this->Contents->get($id, [
            'contain' => ['Unit', 'ContentCategory', 'ContentDetail']
        ]);

        $this->setResponseData($content, $success, $message);
    }

    public function viewBySlug($slug = null)
    {
        $success = true;
        $message = '';

        $content = $this->Contents->find('all', [
            'fields' => [],
            'contain' => [
                'ContentCategory', 
                'ContentDetail', 
                'ContentComment',
                'ContentComment.Pengguna'
            ],
            'conditions' => ['Contents.slug' => $slug]
        ])->first();

        $this->setResponseData($content, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';

        $this->Contents->setInstansi($this->getCurrentInstansi());

        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $content = $this->Contents->patchEntity($content, $this->request->data());
            if ($this->Contents->save($content)) {
                $success = true;
                $message = __('Content berhasil disimpan');
            } else {
                debug($content);exit();
                $message = __('Content tidak berhasil disimpan');
            }
        }
        $this->setResponseData($content, $success, $message);
    }

    public function addComment()
    {
        $success = false;
        $message = '';

        $this->ContentComment = $this->loadModel('ContentComment');
        $this->ContentComment->setInstansi($this->getCurrentInstansi());

        $comment = $this->ContentComment->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->ContentComment->patchEntity($comment, $this->request->getData());
            if ($this->ContentComment->save($comment)) {
                $success = true;
                $message = __('Comment berhasil disimpan');
            } else {
                $message = __('Comment tidak berhasil disimpan');
            }
        }
        $this->setResponseData($comment, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = true;
        $messages = '';

        $content = $this->Contents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            if ($this->Contents->save($content)) {
                $success = true;
                $messages = __('Content berhasil dirubah');
            } else {
                $messages = __('Content tidak berhasil dirubah');
            }
        }
        $this->setResponseData($content, $success, $messages);
    }

    public function latestContent()
    {
        $success = true;
        $message = '';
        $this->Contents->setInstansi($this->getCurrentInstansi());

        $contents = $this->Contents->find('all', [
            'contain' => ['ContentCategory'],
            'coditions' => ['Contents.status' => 'publish'],
            'order' => [
                'Contents.id' => 'DESC',
            ],
        ])
        ->limit(4)
        ->page(1);


        $contents = $contents->toArray();
        
        $this->setResponseData($contents, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /* public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Contents->get($id);
        if ($this->Contents->delete($content)) {
            $this->Flash->success(__('The content has been deleted.'));
        } else {
            $this->Flash->error(__('The content could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    } */
}
