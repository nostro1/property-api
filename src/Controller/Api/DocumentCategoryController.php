<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * DocumentCategory Controller
 *
 * @property \App\Model\Table\DocumentCategoryTable $DocumentCategory
 *
 * @method \App\Model\Entity\DocumentCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentCategoryController extends ApiController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['listFolder']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->DocumentCategory->setInstansi($this->getCurrentInstansi());
        /* $this->paginate = [
            'contain' => ['Instansis', 'Lfts', 'Rghts', 'ParentDocumentCategory']
        ];
        $documentCategory = $this->paginate($this->DocumentCategory); */
        
        // Expanded default values
        $query = $this->DocumentCategory->find('threaded', [
            'keyField' => $this->DocumentCategory->primaryKey(),
            'parentField' => 'parent_id'
        ]);
        $documentCategories = $query->toArray();
 
        $this->setResponseData($documentCategories, $success, $message);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function list()
    {
        $success = true;
        $message = '';
        $this->DocumentCategory->setInstansi($this->getCurrentInstansi());
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;
        $category_id = $this->request->getQuery('document_category_id') ? $this->request->getQuery('document_category_id') : null;

        $conditions = [
            'DocumentCategory.del' => 0,
            'OR' => [
                'DocumentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['DocumentCategory.parent_id' => $category_id]);
        } else {
            array_push($conditions, ['DocumentCategory.parent_id is' => null]);
        }
        
        $documentCategory = $this->DocumentCategory->find('all', [
            'fields' => ['id', 'category_code', 'category_label', 'instansi_id'],
            'contain' => ['DocumentRepository'],
            'conditions' => $conditions
        ]);

        $data = array(
            'items' => $documentCategory
        );
 
        $this->setResponseData($data, $success, $message);
    }

    public function listAC()
    {
        $success = true;
        $message = '';
        $this->DocumentCategory->setInstansi($this->getCurrentInstansi());

        $conditions = [
            'DocumentCategory.del' => 0,
            'OR' => [
                'DocumentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];
        
        $documentCategory = $this->DocumentCategory->find('all', [
            'fields' => ['id', 'category_code', 'category_label', 'instansi_id'],
            'conditions' => $conditions
        ]);

        $data = array(
            'items' => $documentCategory
        );
 
        $this->setResponseData($data, $success, $message);
    }
    /**
     * View method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $documentCategory = $this->DocumentCategory->get($id, [
            'contain' => []
        ]);

        $this->setResponseData($documentCategory, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->DocumentCategory->setInstansi($this->getCurrentInstansi());

        $documentCategory = $this->DocumentCategory->newEntity();

        if ($this->request->is('post')) {
            $documentCategory = $this->DocumentCategory->patchEntity($documentCategory, $this->request->getData());

            if ($this->DocumentCategory->save($documentCategory)) {
                $success = true;
                $message = __('Kategori berhasil disimpan');
            } else {
                $message = __('Kategori tidak berhasil disimpan');
            }
        }
        
        $this->setResponseData($documentCategory, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';
        $this->DocumentCategory->setInstansi($this->getCurrentInstansi());
        
        $documentCategory = $this->DocumentCategory->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentCategory = $this->DocumentCategory->patchEntity($documentCategory, $this->request->getData());
            if ($this->DocumentCategory->save($documentCategory)) {
                $success = true;
                $message = __('Kategori berhasil dirubah');
            } else {
                $message = __('Kategori tidak berhasil dirubah');
            }
        }
        
        $this->setResponseData($documentCategory, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $success = true;
        $message = '';

        $this->request->allowMethod(['post', 'delete']);

        $documentCategory = $this->DocumentCategory->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentCategory = $this->DocumentCategory->patchEntity($documentCategory, $this->request->getData());
            if ($this->DocumentCategory->save($documentCategory)) {
                $success = true;
                $message = __('Kategori berhasil di hapus');
            } else {
                $message = __('Kategori gagal di hapus');
            }
        }
        
        $this->setResponseData($documentCategory, $success, $message);
    }

    public function getHierarchy()
    {
        $success = true;
        $message = '';
        $data = array();
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;

        $conditions = [
            'DocumentCategory.del' => 0,
            'OR' => [
                'DocumentCategory.category_code ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentCategory.category_label ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($kategori)) {
            array_push($conditions, ['LOWER(DocumentCategory.category_label)' => $kategori]);
        }
        // Get All Menu
        $allCategory = $this->DocumentCategory->find('threaded', [
            'fields' => [
                'DocumentCategory.id',
                'label' => 'DocumentCategory.category_label',
                'DocumentCategory.parent_id',
            ],
            'coditions' => $conditions,
            'order' => [
                'DocumentCategory.category_label' => 'ASC',
            ],
        ]);

        $allDocumentCategory = $allCategory->toArray();
        $data['hierarchy'] = $allDocumentCategory;

        $this->setResponseData($data, $success, $message);
    }

    public function listFolder($folder = null)
    {
        $success = true;
        $message = '';

        $dir = new Folder(WWW_ROOT . 'files/' . $folder);
        $dirList = $dir->read();

        $this->setResponseData($dirList, $success, $message);
    }

}
