<?php
namespace App\Controller\Api;

use App\Controller\AppController;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Inflector;
use Cake\Utility\Xml;
use Cake\ORM\TableRegistry;
use App\Service\ReportGeneratorService;
use App\Service\GeneralService;
use App\Service\UploadService;
use App\Service\FileService;
use FPDI;


/**
 * DocumentRepository Controller
 *
 *
 * @method \App\Model\Entity\DocumentRepository[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentRepositoryController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['downloadFile', 'setQr', 'uploadClient']);
        $this->DocumentAssign = $this->loadModel('DocumentAssign');
        $this->DocumentShare = $this->loadModel('DocumentShare');
        $this->DocumentComment = $this->loadModel('DocumentComment');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->DocumentRepository->setInstansi($this->getCurrentInstansi());
        
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;
        $category_id = $this->request->getQuery('document_category_id') ? $this->request->getQuery('document_category_id') : null;

        $conditions = [
            'DocumentRepository.del' => 0,
            'OR' => [
                'DocumentRepository.dibuat_oleh' => $this->getCurrentUsername(),
                'DocumentShare.pengguna_id' => $this->getCurrentUserId(),
                'Workflow.pengguna_id' => $this->getCurrentUserId(),
            ],
            'OR' => [
                'DocumentRepository.document_type ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_category ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.title ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.summary ILIKE' => '%' . $this->_apiQueryString . '%'
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['DocumentRepository.document_category_id' => $category_id]);
        } else {
            array_push($conditions, ['DocumentRepository.document_category_id is ' => null]);
        }

        $this->paginate = [
            'contain' => [
                'DocumentShare' => [], 
                'Workflow' => []
            ],
            'conditions' => $conditions,
            'order' => [
                'DocumentRepository.id DESC'
            ]
        ];

        $DocumentRepository = $this->paginate($this->DocumentRepository);

        foreach ($DocumentRepository as $dr) {
            $dr['summary'] = substr($dr['summary'], 1, 200);
        }

        $paging = $this->request->params['paging']['DocumentRepository'];
        $DocumentRepository = $this->addRowNumber($DocumentRepository);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $DocumentRepository,
            'total_items' => $paging['count']
        );

        $this->setResponseData($DocumentRepository, $success, $message);
    }

    public function list()
    {
        $success = true;
        $message = '';
        $this->DocumentRepository->setInstansi($this->getCurrentInstansi());
        $kategori = $this->request->getQuery('kategori') ? $this->request->getQuery('kategori') : null;
        $category_id = $this->request->getQuery('document_category_id') ? $this->request->getQuery('document_category_id') : null;

        $conditions = [
            'DocumentRepository.del' => 0,
            'OR' => [
                'DocumentRepository.dibuat_oleh' => $this->getCurrentUsername(),
                'DocumentShare.pengguna_id' => $this->getCurrentUserId(),
                'Workflow.pengguna_id' => $this->getCurrentUserId(),
            ],
            'OR' => [
                'DocumentRepository.document_type ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_category ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.title ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.summary ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_path ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        if(!is_null($category_id)) {
            array_push($conditions, ['DocumentRepository.document_category_id' => $category_id]);
        } else {
            array_push($conditions, ['DocumentRepository.document_category_id is ' => null]);
        }

        $this->paginate = [
            'contain' => ['DocumentShare', 'Workflow'],
            'conditions' => $conditions,
            'order' => [
                'DocumentRepository.id DESC'
            ]
        ];

        $DocumentRepository = $this->paginate($this->DocumentRepository);

        foreach ($DocumentRepository as $dr) {
            $dr['summary'] = substr($dr['summary'], 1, 200);
        }

        $paging = $this->request->params['paging']['DocumentRepository'];
        $DocumentRepository = $this->addRowNumber($DocumentRepository);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $DocumentRepository,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    public function mylist()
    {
        $success = true;
        $message = '';
        $this->DocumentRepository->setInstansi($this->getCurrentInstansi());
        
        $conditions = [
            'DocumentRepository.del' => 0,
            'OR' => [
                'DocumentRepository.document_type ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_category ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.title ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.summary ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_path ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        array_push($conditions, ['OR' => [
            'DocumentRepository.dibuat_oleh' => $this->getCurrentUsername(),
            'DocumentShare.pengguna_id' => $this->getCurrentUserId(),
            'Workflow.pengguna_id' => $this->getCurrentUserId(),
        ]]);

        $this->paginate = [
            'contain' => [
            ],
            'conditions' => $conditions,
            'order' => [
                'DocumentRepository.id DESC'
            ]
        ];

        $DocumentRepository = $this->paginate($this->DocumentRepository);

        foreach ($DocumentRepository as $dr) {
            $dr['summary'] = substr($dr['summary'], 1, 200);
        }

        $paging = $this->request->params['paging']['DocumentRepository'];
        $DocumentRepository = $this->addRowNumber($DocumentRepository);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $DocumentRepository,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    public function myFile()
    {
        $success = true;
        $message = '';
        $this->DocumentRepository->setInstansi($this->getCurrentInstansi());

        $conditions = [
            'DocumentRepository.del' => 0,
            'OR' => [
                'DocumentRepository.document_type ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_category ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.title ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.summary ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentRepository.document_path ILIKE' => '%' . $this->_apiQueryString . '%',
            ]
        ];

        array_push($conditions, ['OR' => [
            'DocumentRepository.dibuat_oleh' => $this->getCurrentUsername(),
            'DocumentShare.pengguna_id' => $this->getCurrentUserId(),
            'Workflow.pengguna_id' => $this->getCurrentUserId(),
        ]]);

        $query = $this->DocumentRepository->find()
        ->leftJoin(['DocumentShare' => 'document_share'], ['DocumentShare.document_repository_id = DocumentRepository.id'])
        ->leftJoin(['Workflow' => 'workflow'], ['Workflow.objek_id = DocumentRepository.id'])
        ->where($conditions);

        $DocumentRepository = $this->paginate($query);

        foreach ($DocumentRepository as $dr) {
            $dr['summary'] = substr($dr['summary'], 1, 200);
        }

        $paging = $this->request->params['paging']['DocumentRepository'];
        $DocumentRepository = $this->addRowNumber($DocumentRepository);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $DocumentRepository,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }
    /**
     * View method
     *
     * @param string|null $id Dokumen Repositori id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $DocumentRepository = $this->DocumentRepository->get($id,[
            'fields' => ['id', 'title', 'document_type', 'document_category', 'document_category_id', 'document_path', 'summary']
        ]);

        $this->setResponseData($DocumentRepository, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->DocumentRepository->setInstansi($this->getCurrentInstansi());

        $DocumentRepository = $this->DocumentRepository->newEntity();

        if ($this->request->is('post')) {
            $DocumentRepository = $this->DocumentRepository->patchEntity($DocumentRepository, $this->request->getData());
            if ($this->DocumentRepository->save($DocumentRepository)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                debug($DocumentRepository);exit();
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($DocumentRepository, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dokumen Repositori id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = true;
        $messages = '';

        $DocumentRepository = $this->DocumentRepository->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $DocumentRepository = $this->DocumentRepository->patchEntity($DocumentRepository, $this->request->getData());
            if ($this->DocumentRepository->save($DocumentRepository)) {
                $success = true;
                $messages = __('Dokumen berhasil dirubah');
            } else {
                $messages = __('Dokumen tidak berhasil dirubah');
            }
        }
        $this->setResponseData($DocumentRepository, $success, $messages);
    }

    public function uploadClient() {

        $this->setResponseData(JSON_ENCODE($_FILES), '$success', JSON_ENCODE($this->request->getData()));
    }
    /**
     * Delete method
     *
     * @param string|null $id Document Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $success = true;
        $message = '';

        $this->request->allowMethod(['post', 'delete']);

        $documentRepository = $this->DocumentRepository->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentRepository = $this->DocumentRepository->patchEntity($documentRepository, $this->request->getData());
            if ($this->DocumentRepository->save($documentRepository)) {
                $success = true;
                $message = __('Dokumen berhasil di hapus');
            } else {
                $message = __('Dokumen gagal di hapus');
            }
        }
        
        $this->setResponseData($documentRepository, $success, $message);
    }
    /**
     * Upload dokumen file
     */
    public function Upload($folder = null)
    {
        $data = [];
        $success = false;
        $message = '';
        if($folder == null) {
            $folder = 'upload';
        }

        try {
            UploadService::setUser($this->getCurrentUser());
            UploadService::setInstansiID($this->getInstansiIdFromDataOrSession());
            $uploadData = UploadService::upload('file', $folder);
            $data['file_name'] = $uploadData['file_name'];
            $data['file_url'] = $uploadData['url'];
            $fileFullPath = WWW_ROOT . 'files' . DS . $folder . DS . $uploadData['file_name'];

            //$docObj = new FileService($fileFullPath);
            //$data['content']= $docObj->convertToText();

            /* if (substr($uploadData['file_name'],-3) == 'ocx') {
                $data['preview'] = ReportGeneratorService::docxToPdf($fileFullPath);
            } */

            $success = true;
            $message = 'Dokumen berhasil diupload';
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
        }

        $this->setResponseData($data, $success, $message);
    }

    public function convertToPdf() {
        $http = new Client();
        $response = $http->post(
            'http://127.0.0.1:8000/ocr_image_service/api/ocr_img_json',
            [
                'file' => fopen($wordPath, 'r')
            ]
        );
        $respJson = $response->json;

        // Download the file
        if (!empty($respJson) && isset($respJson['download_url'])) {
            file_put_contents($pdfPath, file_get_contents($respJson['download_url']));
        }
    }
    //Assign user to pengguna
    public function assign () {
        $success = false;
        $message = '';

        $documentAssign = $this->DocumentAssign->newEntity();

        if ($this->request->is('post')) {
            $documentAssign = $this->DocumentAssign->patchEntity($documentAssign, $this->request->getData());
            if ($this->DocumentAssign->save($documentAssign)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentAssign, $success, $message);
    }

    public function getAssignList($id = null)
    {
        $success = true;
        $message = '';

        $dokumenAssign = $this->DocumentAssign->find('all')
            ->contain(['Pengguna'])
            ->where(['document_repository_id' => $id, 'del' => 0]);

        $this->setResponseData($dokumenAssign, $success, $message);
    }

    public function delAssign () {
        $success = false;
        $message = '';

        $success = true;
        $message = '';

        $documentAssign = $this->DocumentAssign->get($this->request->getData()['id']);        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentAssign = $this->DocumentAssign->patchEntity($documentAssign, $this->request->getData());
            if ($this->DocumentAssign->save($documentAssign)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentAssign, $success, $message);
    }
    //END-Assign pengguna

    //Share user to pengguna
    public function share () {
        $success = false;
        $message = '';

        $documentShare = $this->DocumentShare->newEntity();

        if ($this->request->is('post')) {
            $documentShare = $this->DocumentShare->patchEntity($documentShare, $this->request->getData());
            if ($this->DocumentShare->save($documentShare)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentShare, $success, $message);
    }

    public function bulkQr($parentFolder = null, $isChild = false){
        ini_set('max_execution_time', 36000);
        ini_set('memory_limit', '7G');
        $success = true;
        $message = 'ini';

        $reqData = $this->request->getData();
        $listFolder = $reqData['folder'];
        $bulkId = $reqData['bulk_process_id'];

        $bulkProc = new \App\Controller\Api\BulkProcessController();

        foreach ($listFolder as $folder) {
            $dir = new Folder(WWW_ROOT . 'files' . DS. $parentFolder. DS. $folder);
            $dirList = $dir->read();

            $bulkProc->setTotalProcess($bulkId, count($dirList[1]));
        }

        $this->DocumentSloc = $this->loadModel('DocumentSloc');

        foreach ($listFolder as $folder) {
            $dir = new Folder(WWW_ROOT . 'files' . DS. $parentFolder. DS. $folder);
            $dirList = $dir->read();

            foreach ($dirList[1] as $file) {
                $likeFilename = str_replace(' ', '%', $file);
				
                //$documentSloc = $this->DocumentSloc->find('all')
                //->where(['media ilike' => $likeFilename, 'del' => 0])
                //->toList();

/*                 if(count($documentSloc) > 0) {
                    $data = date('d/m/Y') .';'.
							$documentSloc[0]['no_box'] .';'.
							$file;

                } else {
                    $data = date('d/m/Y') .';'.
							$file;
                } */
                
				$data = date('d/m/Y') .';'.
						$folder.';'.
						$file;
							
                $Qr = GeneralService::generateQrLogo('01', $data, null, true, 'lps.png');

                $pdfFile = WWW_ROOT . 'files' . DS . $parentFolder . DS. $folder . DS . $file;
                $outFile = WWW_ROOT . 'files' . DS . $parentFolder . DS . $folder . DS . 'stamp_' . $file;
                $generated = ReportGeneratorService::setQr($pdfFile, $Qr[1]['src'], $outFile);
                $bulkProc->setRunningProc($bulkId);
            }

            $this->childBulkQr($parentFolder . DS . $folder, $dirList[0], $bulkId);
        }
        $this->setResponseData($listFolder, $success, $message);
        
    }

    private function childBulkQr($rootFolder, $listFolder = null, $bulkId = null){
        ini_set('max_execution_time', 36000);
        ini_set('memory_limit', '6G');

        $bulkProc = new \App\Controller\Api\BulkProcessController();

        foreach ($listFolder as $folder) {
            $dir = new Folder(WWW_ROOT . 'files' . DS. $rootFolder. DS. $folder);
            $dirList = $dir->read();

            $bulkProc->setTotalProcess($bulkId, count($dirList[1]));
        }

        $this->DocumentSloc = $this->loadModel('DocumentSloc');

        foreach ($listFolder as $folder) {
            $dir = new Folder(WWW_ROOT . 'files' . DS. $rootFolder. DS. $folder);
            $dirList = $dir->read();

            foreach ($dirList[1] as $file) {

                $likeFilename = str_replace(' ', '%', $file);
	
                $documentSloc = $this->DocumentSloc->find('all')
                ->where(['media ilike' => $likeFilename, 'del' => 0])
                ->toList();

                if(count($documentSloc) > 0) {
                    $data = date('d/m/Y') .';'.
							$documentSloc[0]['no_box'] .';'. 
							$file;
                } else {
                    $data = date('d/m/Y') .';'.
							$file;
                }
                $Qr = GeneralService::generateQrLogo('01', $data, null, true, 'lps.png');

                $pdfFile = WWW_ROOT . 'files' . DS . $rootFolder . DS. $folder . DS . $file;
                $outFile = WWW_ROOT . 'files' . DS . $rootFolder . DS . $folder . DS . 'stamp_' . $file;
                $generated = ReportGeneratorService::setQr($pdfFile, $Qr[1]['src'], $outFile);
                $bulkProc->setRunningProc($bulkId);
            }

            $this->childBulkQr($rootFolder . DS . $folder, $dirList[0], $bulkId);
        }
        
    }

    public function setQr() {
        $success = true;
        $message = '';

        $reqData = $this->request->getData();
        $folder = strtolower(str_replace('', '_', $reqData['category']));

        $ekstensi = explode('.', $reqData['fileName']);
        $fileTmp = explode('_', $ekstensi[0]);
        
        if (count($fileTmp) == 1) {
            $fileName = $fileTmp[0];
        } else {
            $fileName = $fileTmp[1];
        }

        if (count($fileTmp) > 2) {
            for ($i = 2; $i < count($fileTmp); $i++) {
                $fileName = $fileName . '_' . $fileTmp[$i];
            }
        }
        $fileName = date('YmdHis') . '_' . $fileName . '.pdf';
        
        $generateQr = GeneralService::generateQrLogo('01', $fileName, null, true, 'lps.png');

        $pdfFile = WWW_ROOT . 'files' . DS . $folder . DS . $reqData['fileName'];
        $outFile = WWW_ROOT . 'files' . DS . $folder . DS . $fileName;
        $generated = ReportGeneratorService::setQr($pdfFile, $generateQr[1]['src'], $outFile);

        $documentRepository = $this->DocumentRepository->get($reqData['id']);

        $documentRepository['document_path'] = $fileName;
        $documentRepository['no_rename'] = true;

        if ($this->DocumentRepository->save($documentRepository)) {
            $success = true;
            $message = __('Dokumen berhasil disimpan');
        } else {
            $message = __('Dokumen tidak berhasil disimpan'); 
        }
        $this->setResponseData($documentRepository, $success, $message);
    }

    public function getShareList($id = null)
    {
        $success = true;
        $message = '';

        $documentShare = $this->DocumentShare->find('all')
            ->contain(['Pengguna'])
            ->where(['document_repository_id' => $id, 'del' => 0]);

        $this->setResponseData($documentShare, $success, $message);
    }

    public function delShare () {
        $success = false;
        $message = '';

        $success = true;
        $message = '';

        $documentShare = $this->DocumentShare->get($this->request->getData()['id']);        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentShare = $this->DocumentShare->patchEntity($documentShare, $this->request->getData());
            if ($this->DocumentShare->save($documentShare)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentShare, $success, $message);
    }
    //END-Share

    //Comment Documment
    public function comment () {
        $success = false;
        $message = '';

        $documentComment = $this->DocumentComment->newEntity();

        if ($this->request->is('post')) {
            $documentComment = $this->DocumentComment->patchEntity($documentComment, $this->request->getData());
            if ($this->DocumentComment->save($documentComment)) {
                $success = true;
                $message = __('Comment berhasil disimpan');
            } else {
                $message = __('Comment tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentComment, $success, $message);
    }

    public function getCommentList($id = null)
    {
        $success = true;
        $message = '';

        $documentComment = $this->DocumentComment->find('all')
            ->contain(['Pengguna'])
            ->where(['document_repository_id' => $id, 'del' => 0])
            ->order(['DocumentComment.dibuat_oleh' => 'DESC']);

        $this->setResponseData($documentComment, $success, $message);
    }

    public function delComment () {
        $success = false;
        $message = '';

        $documentComment = $this->DocumentComment->get($this->request->getData()['id']);        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentComment = $this->DocumentComment->patchEntity($documentComment, $this->request->getData());
            if ($this->DocumentComment->save($documentComment)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentComment, $success, $message);
    }
    //END-Share
}
