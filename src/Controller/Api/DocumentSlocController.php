<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * DocumentSloc Controller
 *
 * @property \App\Model\Table\DocumentSlocTable $DocumentSloc
 *
 * @method \App\Model\Entity\DocumentSloc[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentSlocController extends ApiController
{
    public function initialize()
    {
        parent::initialize();
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->DocumentSloc->setInstansi($this->getCurrentInstansi());

        $documentSloc = [
            'DocumentSloc.del' => 0,
            'OR' => [
                'DocumentSloc.unit_kerja ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentSloc.no_box_lama ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentSloc.no_box ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentSloc.klasifkasi_arsip ILIKE' => '%' . $this->_apiQueryString . '%',
                'DocumentSloc.kode_arsip ILIKE' => '%' . $this->_apiQueryString . '%'
            ]
        ];

        $this->paginate = [
            'conditions' => $documentSloc,
            'order' => [
                'DocumentSloc.id DESC'
            ]
        ];

        $documentSlocs = $this->paginate($this->DocumentSloc);
        $paging = $this->request->params['paging']['DocumentSloc'];
        $documentSlocs = $this->addRowNumber($documentSlocs);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $documentSlocs,
            'total_items' => $paging['count']
        );

        $this->setResponseData($documentSlocs, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Document Sloc id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $documentSloc = $this->DocumentSloc->get($id, [
            'contain' => ['Unit', 'ContentCategory', 'ContentDetail']
        ]);

        $this->setResponseData($documentSloc, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->DocumentSloc->setInstansi($this->getCurrentInstansi());

        $documentSloc = $this->DocumentSloc->newEntity();
        if ($this->request->is('post')) {
            $documentSloc = $this->DocumentSloc->patchEntity($documentSloc, $this->request->getData());
            if ($this->DocumentSloc->save($documentSloc)) {
                $success = true;
                $message = __('Data berhasil disimpan');
            } else {
                $message = __('Data tidak berhasil disimpan');
            }
        }
        $this->setResponseData($documentSloc, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Document Sloc id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = true;
        $messages = '';

        $documentSloc = $this->DocumentSloc->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentSloc = $this->DocumentSloc->patchEntity($documentSloc, $this->request->getData());
            if ($this->DocumentSloc->save($documentSloc)) {
                $success = true;
                $messages = __(Data berhasil dirubah');
            } else {
                $messages = __(Data tidak berhasil dirubah');
            }
        }
        $this->setResponseData($documentSloc, $success, $messages);
    }

    /**
     * Delete method
     *
     * @param string|null $id Document Sloc id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /* public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $documentSloc = $this->DocumentSloc->get($id);
        if ($this->DocumentSloc->delete($documentSloc)) {
            $this->Flash->success(__('The document sloc has been deleted.'));
        } else {
            $this->Flash->error(__('The document sloc could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    } */
}
