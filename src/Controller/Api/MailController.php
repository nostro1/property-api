<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Mail Controller
 *
 *
 * @method \App\Model\Entity\Mail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MailController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->Mail->setInstansi($this->getCurrentInstansi());

        $this->paginate = [
            'fields' => ['id', 'mail_category', 'mail_no', 'mail_subject', 'mail_from', 'mail_to'],
            'conditions' => [
                'del' => 0,
                'OR' => [
                    'LOWER(mail_subject) LIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(mail_category) LIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(mail_no) LIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(mail_from) LIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(mail_to) LIKE' => '%' . $this->_apiQueryString . '%',
                ]
            ],
            'order' => [
                'Mail.id' => 'DESC'
            ]
        ];

        $mail = $this->paginate($this->Mail);

        $paging = $this->request->params['paging']['Mail'];

        $mail = $this->addRowNumber($mail);

        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $mail,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Mail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $mail = $this->Mail->get($id, [
            'fields' => ['id', 'mail_category', 'mail_no', 'mail_subject', 'mail_from', 'mail_to', 'content_formated'],
            'contain' => [
                'DocumentRepository' => [
                    'fields' => ['id', 'mail_id', 'document_path', 'title', 'document_category', 'document_type', 'del'],
                    'conditions' => ['del' => 0]
                ]
            ]
        ]);

        $this->setResponseData($mail, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->Mail->setInstansi($this->getCurrentInstansi());

        $mail = $this->Mail->newEntity();

        if ($this->request->is('post')) {
            $mail = $this->Mail->patchEntity($mail, $this->request->getData());
            if ($this->Mail->save($mail)) {
                $success = true;
                $message = __('Surat berhasil disimpan');
            }else {
                $message = __('Surat tidak berhasil disimpan');
            }
        }

        $this->setResponseData($mail, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';
        $this->Mail->setInstansi($this->getCurrentInstansi());
        
        $mail = $this->Mail->get($id, [
            'contain' => ['DocumentRepository']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $mail = $this->Mail->patchEntity(
                $mail, 
                $this->request->getData(),
                [
                    'associated' => [
                        'DocumentRepository'
                    ]
                ]
            );

            if ($this->Mail->save($mail)) {
                $success = true;
                $messages = __('Surat berhasil dirubah');
            } else {
                debug($mail);exit();
                $messages = __('Surat tidak berhasil dirubah');
            }
        }
        $this->setResponseData($mail, $success, $messages);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mail = $this->Mail->get($id);
        if ($this->Mail->delete($mail)) {
            $this->Flash->success(__('The mail has been deleted.'));
        } else {
            $this->Flash->error(__('The mail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
