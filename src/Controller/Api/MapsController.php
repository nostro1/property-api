<?php
namespace App\Controller\Api;

use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use App\Model\Entity\PermohonanIzin;

// TODO: create model for Map
// Mengaktifkan korelasi dengan model
// penambahan fungsi untuk get center overide data jika ada data di table maping

class MapsController extends ApiController
{
    public function sicantik() {
        $http = new Client();
        $response = $http->get(
            'https://sicantikui.layanan.go.id/peta/data/data.csv',
            null,
            [
                'headers' => ['Origin' => Router::fullBaseUrl()]
            ]
        );

        $this->setResponseData($response->body, true);
    }

    public function getCenter() {
        $unitTable = TableRegistry::get('unit');

        $unit = $unitTable->find()
            ->select(['lat', 'long'])
            ->where(['id' => $this->getCurrentInstansi()->id])
            ->first();

        $this->setResponseData($unit, true);
    }

    public function getLayers() {
        // TODO: create model for MapLayer
        $mapLayersTable = TableRegistry::get('map_layers');

        // TODO: use model instead of manually joining tables
        $mapLayers = $mapLayersTable->find()
            ->select()
            ->innerJoin(
                    ['b' => 'maps'],
                    ['b.id = map_layers.maps_id']
                )
            ->where(['OR' => [
                        'map_layers.instansi_id' => $this->getCurrentInstansi()->id,
                        'map_layers.instansi_id IS' => null
                    ],
                    'b.module' => $this->request->getQuery('module')
                ])
            ->toList();

        $this->setResponseData($mapLayers, true);
    }
}
