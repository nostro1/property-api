<?php
namespace App\Controller\Api;;

use App\Controller\AppController;

/**
 * MerchantDetail Controller
 *
 *
 * @method \App\Model\Entity\MerchantDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MerchantDetailController extends ApiController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        
        $this->MerchantDetail->setInstansi($this->getCurrentInstansi());

        $merchantDetail = $this->paginate($this->MerchantDetail);

        $this->setResponseData($merchantDetail, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Merchant Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $merchantDetail = $this->MerchantDetail->get($id, [
            'contain' => []
        ]);

        $this->set('merchantDetail', $merchantDetail);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $merchantDetail = $this->MerchantDetail->newEntity();
        if ($this->request->is('post')) {
            $merchantDetail = $this->MerchantDetail->patchEntity($merchantDetail, $this->request->getData());
            if ($this->MerchantDetail->save($merchantDetail)) {
                $this->Flash->success(__('The merchant detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant detail could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantDetail'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Merchant Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $merchantDetail = $this->MerchantDetail->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $merchantDetail = $this->MerchantDetail->patchEntity($merchantDetail, $this->request->getData());
            if ($this->MerchantDetail->save($merchantDetail)) {
                $this->Flash->success(__('The merchant detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The merchant detail could not be saved. Please, try again.'));
        }
        $this->set(compact('merchantDetail'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Merchant Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $merchantDetail = $this->MerchantDetail->get($id);
        if ($this->MerchantDetail->delete($merchantDetail)) {
            $this->Flash->success(__('The merchant detail has been deleted.'));
        } else {
            $this->Flash->error(__('The merchant detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
