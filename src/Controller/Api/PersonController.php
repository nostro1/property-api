<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiController;

use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use App\Service\AuthService;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Person Controller
 *
 * @property \App\Model\Table\PersonTable $Person
 *
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PersonController extends ApiController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Person->setInstansi($this->getCurrentInstansi());
        $this->Person->setUser($this->getCurrentUser());
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'signup', 'validateOTP', 'view'
        ]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';

        $this->paginate = [
            'contain' => [],
            'conditions' => [
                'OR' => [
                    'LOWER(Person.name) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.address) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.contact) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.email) ILIKE' => '%' . $this->_apiQueryString . '%',

                ]
            ]
        ];

        $person = $this->paginate($this->Person);
        $paging = $this->request->params['paging']['Person'];
        $person = $this->addRowNumber($person);

        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $person,
            'total_items' => $paging['count']
        );
        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';
        
        $person = $this->Person->get($id, [
            'contain' => [
                'Unit', 
                'Pengguna', 
                'Billing', 
                'Billing.BillingDetail',
                'PersonProperty' => [
                    'conditions' => ['PersonProperty.status' => 'confirmed']
                ],
                'PersonProperty.PropertyUnit',
                'PersonProperty.PropertyUnit.Project'
            ]
        ]);

        $this->setResponseData($person, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = true;
        $message = '';

        $person = $this->Person->newEntity();
        if ($this->request->is('post')) {
            $person = $this->Person->patchEntity($person, $this->request->getData());
            if ($this->Person->save($person)) {
                $success = true;
                $message = __('Account berhasil dibentuk.');
            } else {
                $message = __('Account gagal disimpan');
            }
        }
        $this->setResponseData($person, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = true;
        $message = '';

        $person = $this->Person->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $person = $this->Person->patchEntity($person, $this->request->getData());
            if ($this->Person->save($person)) {
                $product = $this->Product->patchEntity($product, $this->request->getData());
                if ($this->Product->save($product)) {
                    $success = true;
                    $message = __('Product berhasil disimpan.');
                } else {
                    $message = __('Product tidak berhasil disimpan. Silahkan coba kembali.');
                }
            }
        }
        $this->setResponseData($product, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $person = $this->Person->get($id);
        if ($this->Person->delete($person)) {
            $this->Flash->success(__('The person has been deleted.'));
        } else {
            $this->Flash->error(__('The person could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function signup()
    {
        $success = false;
        $message = '';
        $data = [];

        
        $person = $this->Person->newEntity();
        

        if ($this->request->is(['patch', 'post', 'put'])) {
            // Get Peran 'pemohon'
            $peranTable = TableRegistry::get('Peran');
            $peran = $peranTable->find('all', [
                'conditions' => [
                    'label_peran' => 'person property'
                ]
            ])->first();

            $person = $this->Person->patchEntity($person, $this->request->data);
            $person->otp = rand(1000, 9999);

            if ($this->Person->save($person)) {
                $this->Pengguna = $this->loadModel('Pengguna');
                $pengguna = $this->Pengguna->newEntity();
                $pengguna = $this->Pengguna->patchEntity($pengguna, $this->request->data['pengguna']);

                $peranTable = TableRegistry::get('Peran');
                $peran = $peranTable->find('all', [
                    'conditions' => [
                        'label_peran' => 'person property'
                    ]
                ])->first();

                $pengguna->peran_id = $peran->id;
                $pengguna->related_object_id = $person->id;
                $pengguna->related_object_name = 'Person';
                $pengguna->is_active = false;
                
                if ($this->Pengguna->save($pengguna)) {
                    $success = true;
                    $message = __('Registrasi berhasil.');
                }

                // TODO Send registration email
                /* \App\Service\NotificationService::sendMessage(
                    $person->email,
                    'Registrasi',
                    'Terima kasih telah melakukan pendaftaran. 
                    Anda akan mendapatkan notifikasi jika pendaftaran anda telah disetujui.'
                ); */
            } else {
                $this->setErrors($person->errors());
                $message = __('Registrasi tidak berhasil. Silahkan coba kembali.');
            }
        }

        $this->setResponseData($data, $success, $message);
    }

    public function validateOtp() {
        $success = false;
        $message = '';
        $data = '';
        
        $person = $this->Person->find('ALL', [
            'contain' => ['Pengguna'],
            'conditions' => [
                'otp' => $this->request->data['otp']
            ]
            ]);

        if(!empty($person)) {
            $dataCek = $person->toArray();

            if (count($dataCek) == 1) {
                /* $person = $this->Person->get($dataCek[0]['id'],[
                    'contain' => ['Pengguna']
                ]);

                $person = $this->Person->patchEntity($person, $dataCek);
                $person->pengguna['is_active'] = true; */

                $penggunaTbl = $this->loadModel('Pengguna');
                $pengguna = $penggunaTbl->get($dataCek[0]['pengguna']['id']);
                $pengguna['is_active'] = true;

                if ($penggunaTbl->save($pengguna)) {
                    $success = true;
                    $message = __('OTP Valid');
                    
                    $expireTime = 12 * 3600;

                    $data = JWT::encode([
                        'sub' => $dataCek[0]->pengguna['id'],
                        'exp' =>  time() + $expireTime
                    ], Security::salt());

                } else {
                    $this->setErrors($person->errors());
                    $message = __('Otp tidak valid periksa kembali otp anda');
                }
            }
        } else {
            $message = __('Otp tidak valid periksa kembali otp anda');
        }

        

        $this->setResponseData($data, $success, $message);
    }

    public function reqUnit() {
        $success = false;
        $message = '';
        $project = '';
        $data = $this->request->getData();
        $personUnit = array();

        $personUnitTbl = $this->loadModel('PersonProperty');
        $cekRequest = $personUnitTbl->find('all', [
            'conditions' => [
                'person_id' => $data['person_id'],
                'property_unit_id' => $data['property_unit_id'],
            ]
        ])->toArray();

        if (count($cekRequest) == 0) {
            $personUnit = $personUnitTbl->newEntity();
            if ($this->request->is('post')) {
                $personUnit = $personUnitTbl->patchEntity($personUnit, $this->request->getData());
                if ($personUnitTbl->save($personUnit)) {
                    $PersonProject = $this->loadModel('PersonProject');

                    $personProject = $PersonProject->find('all', [
                        'conditions' => [
                            'person_id' => $personUnit->person_id
                        ]
                    ])->toArray();

                    if(count($personProject) == 0){
                        $personProject = $PersonProject->newEntity();
                        $pp = array("person_id"=>$personUnit->person_id, "project_id"=>$personUnit->project_id);
                        $personProject = $PersonProject->patchEntity($personProject, $pp);

                        if($PersonProject->save($personProject)){
                            $project = ' dan project berhasil dibentuk';
                        } else {
                            $project = ' dan project gatal dibentuk';
                        }
                    }

                    $success = true;
                    $message = __('Request berhasil dibuat.' . $project);
                } else {
                    $message = __('Request gagal disimpan');
                }
            }
        }else{
            $message = __('Request sudah pernah dibuat');
        }

        $this->setResponseData($personUnit, $success, $message);


    }
}
