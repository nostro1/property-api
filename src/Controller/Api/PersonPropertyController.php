<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiController;

/**
 * PersonProperty Controller
 *
 * @property \App\Model\Table\PersonPropertyTable $PersonProperty
 *
 * @method \App\Model\Entity\PersonProperty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PersonPropertyController extends ApiController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->PersonProperty->setInstansi($this->getCurrentInstansi());
        $this->PersonProperty->setUser($this->getCurrentUser());
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            
        ]);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';

        $this->paginate = [
            'contain' => ['Unit', 'Person', 'PropertyUnit'],
            'conditions' => [
                'OR' => [
                    'LOWER(Person.name) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.address) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.contact) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.email) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(PropertyUnit.code) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(PropertyUnit.description) ILIKE' => '%' . $this->_apiQueryString . '%',
                ]
            ]
        ];

        $personProperty = $this->paginate($this->PersonProperty);
        $paging = $this->request->params['paging']['PersonProperty'];
        $personProperty = $this->addRowNumber($personProperty);

        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $personProperty,
            'total_items' => $paging['count']
        );
        $this->setResponseData($data, $success, $message);
    }

    public function ListOpenRequest() {
        $success = true;
        $message = '';

        $this->paginate = [
            'contain' => ['Unit', 'Person', 'PropertyUnit'],
            'conditions' => [
                'PersonProperty.status' => 'request',
                'OR' => [
                    'LOWER(Person.name) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.address) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.contact) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Person.email) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(PropertyUnit.code) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(PropertyUnit.description) ILIKE' => '%' . $this->_apiQueryString . '%',
                ]
            ]
        ];

        $personProperty = $this->paginate($this->PersonProperty);
        $paging = $this->request->params['paging']['PersonProperty'];
        $personProperty = $this->addRowNumber($personProperty);

        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $personProperty,
            'total_items' => $paging['count']
        );
        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Person Property id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $personProperty = $this->PersonProperty->get($id, [
            'contain' => ['Unit', 'Person', 'PropertyUnit']
        ]);

        $this->setResponseData($personProperty, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        
        $personProperty = $this->PersonProperty->newEntity();
        if ($this->request->is('post')) {
            $personProperty = $this->PersonProperty->patchEntity($personProperty, $this->request->getData());
            if ($this->PersonProperty->save($personProperty)) {
                $personProperty = $this->Product->patchEntity($personProperty, $this->request->getData());
                if ($this->PersonProperty->save($personProperty)) {
                    $success = true;
                    $message = __('Person Unit berhasil disimpan.');
                } else {
                    $message = __('Person Unit tidak berhasil disimpan. Silahkan coba kembali.');
                }
            }
        }
        $this->setResponseData($personProperty, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Person Property id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';
        
        $personProperty = $this->PersonProperty->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $personProperty = $this->PersonProperty->patchEntity($personProperty, $this->request->getData());
            if ($this->PersonProperty->save($personProperty)) {
                $personProperty = $this->Product->patchEntity($personProperty, $this->request->getData());
                if ($this->PersonProperty->save($personProperty)) {
                    $success = true;
                    $message = __('Unit berhasil dirubah.');
                } else {
                    $message = __('Unit tidak berhasil dirubah. Silahkan coba kembali.');
                }
            }
        }
        $this->setResponseData($personProperty, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Person Property id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $personProperty = $this->PersonProperty->get($id);
        if ($this->PersonProperty->delete($personProperty)) {
            $this->Flash->success(__('The person property has been deleted.'));
        } else {
            $this->Flash->error(__('The person property could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
