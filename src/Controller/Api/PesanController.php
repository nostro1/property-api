<?php
namespace App\Controller\Api;

use App\Service\UploadService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Pesan Controller
 *
 * @property \App\Model\Table\PesanTable $Pesan
 *
 * @method \App\Model\Entity\Pesan[] paginate($object = null, array $settings = [])
 */
class PesanController extends ApiController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->PesanDibaca = $this->loadModel('PesanDibaca');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';

        $user       = $this->getCurrentUser();
        $conditions = [
            "(pengguna_id = {$user->id} OR pengguna_id IS NULL)",
        ];
        $tipe = $this->request->getQuery('tipe');

        if ($tipe) {
            $conditions['tipe'] = $tipe;
        }

        $grup_notifikasi = $this->request->getQuery('grup_notifikasi');

        if ($grup_notifikasi) {
            $conditions['grup_notifikasi'] = $grup_notifikasi;
        }

        $conditions['OR'] = [
            'LOWER(judul) ILIKE' => '%' . $this->_apiQueryString . '%',
            'LOWER(pesan) ILIKE' => '%' . $this->_apiQueryString . '%',
            'LOWER(tipe) ILIKE'  => '%' . $this->_apiQueryString . '%',
        ];

        $this->paginate = [
            'fields'     => [
                'id', 'judul', 'pesan', 'tgl_dibuat', 'dibuat_oleh',
            ],
            'conditions' => $conditions,
        ];

        \Cake\I18n\Time::setJsonEncodeFormat('dd-MM-Y HH:mm:ss');
        $pesan  = $this->paginate($this->Pesan);
        $paging = $this->request->params['paging']['Pesan'];

        $pesan = $pesan->each(function ($value, $key) {
            $value->isi = \Cake\Utility\Text::truncate($value->isi, 50);
            return $value;
        });
        $pesan = $this->addRowNumber($pesan);

        $data = array(
            'limit'       => $paging['perPage'],
            'page'        => $paging['page'],
            'items'       => $pesan,
            'total_items' => $paging['count'],
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function getNotifikasi()
    {
        $success = true;
        $message = '';

        $user       = $this->getCurrentUser();
        $conditions = [
            "(pengguna_id = {$user->id} OR pengguna_id IS NULL)",
        ];

        $id   = $this->request->getQuery('id');
        $full = $this->request->getQuery('full');

        if ($id) {
            $conditions['id'] = $id;
        } else {
            $tipe = $this->request->getQuery('tipe');

            if ($tipe) {
                $conditions['tipe'] = $tipe;
            }

            $grup_notifikasi = $this->request->getQuery('grouped');

            if ($grup_notifikasi && $grup_notifikasi != 'F' && $grup_notifikasi != 'T') {
                $conditions['grup_notifikasi'] = $grup_notifikasi;
            }

            $conditions['OR'] = [
                'LOWER(Pesan.judul) ILIKE'           => '%' . $this->_apiQueryString . '%',
                'LOWER(Pesan.isi) ILIKE'             => '%' . $this->_apiQueryString . '%',
                'LOWER(Pesan.grup_notifikasi) ILIKE' => '%' . $this->_apiQueryString . '%',
                'pengguna_id'                        => $user->id,
            ];
        }

        $this->paginate = [
            'contain'    => [
                'PesanDibaca' => [
                    'fields' => [
                        'PesanDibaca.id', 'PesanDibaca.pesan_id', 'PesanDibaca.pegawai_id',
                    ],
                ],
            ],
            'fields'     => [
                'Pesan.id', 'Pesan.judul', 'Pesan.isi', 'Pesan.grup_notifikasi',
                'Pesan.tgl_dibuat', 'Pesan.dibuat_oleh', 'Pesan.file_lampiran',
                'Pesan.tautan', 'Pesan.object_id', 'Pesan.template_id', 'Pesan.proses_id',
            ],
            'conditions' => $conditions,
            'order' => [
                'Pesan.id DESC'
            ]
        ];

        \Cake\I18n\Time::setJsonEncodeFormat('dd-MM-Y HH:mm:ss');
        $pesan  = $this->paginate($this->Pesan);
        $paging = $this->request->params['paging']['Pesan'];

        $total = 0;
        if ($pesan->count() > 0) {
            foreach ($pesan as $key => $value) {
                if (count($value->pesan_dibaca) == 0) {
                    $total = $total + 1;
                }
                $value->full = $full;
            }
        }

        $pesan = $pesan->each(function ($value, $key) {
            $value->url_lampiran = Router::url(
                '/api/Pesan/downloadlampiran/' . $value->id,
                true
            );

            if ($value->full == 'F') {
                $value->isi = \Cake\Utility\Text::truncate($value->isi, 50);
            }
            return $value;
        });

        // Group By
        if ($this->request->getQuery('grouped') == 'T') {
            $pesan = $pesan->groupBy('grup_notifikasi');
        } else {
            $pesan = $this->addRowNumber($pesan);
        }

        $data = array(
            'limit'       => $paging['perPage'],
            'page'        => $paging['page'],
            'items'       => $pesan,
            'total_items' => $total,
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Notifikasi Pengguna id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pesan               = $this->Pesan->get($id);
        $pesan->url_lampiran = Router::url(
            '/api/Pesan/downloadlampiran/' . $pesan->id,
            true
        );

        $this->setResponseData($pesan, true);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $pesan   = $this->Pesan->newEntity();

        if ($this->request->is('post')) {
            $pesan = $this->Pesan->patchEntity($pesan, $this->request->getData());

            if ($this->Pesan->save($pesan)) {
                $message = __('Pesan berhasil disimpan.');
                $success = true;
            } else {
                $this->setErrors($pesan->errors());
                $message = __('Pesan tidak dapat disimpan, silahkan coba kembali.');
            }
        }

        $this->setResponseData($pesan, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Notifikasi Pengguna id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';
        $pesan   = $this->Pesan->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $pesan = $this->Pesan->patchEntity($pesan, $this->request->getData());

            if ($this->Pesan->save($pesan)) {
                $message = __('Pesan berhasil disimpan.');
                $success = true;
            } else {
                $this->setErrors($pesan->errors());
                $message = __('Pesan tidak dapat disimpan, silahkan coba kembali.');
            }
        }

        $this->setResponseData($pesan, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Notifikasi Pengguna id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pesan = $this->Pesan->get($id);

        if ($this->Pesan->delete($pesan)) {
            $this->Flash->success(__('The pesan has been deleted.'));
        } else {
            $this->Flash->error(__('The pesan could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Download Lampiran
     * @param int $id Notifikasi Pengguna Id
     */
    public function downloadLampiran($id)
    {
        $pesan = $this->Pesan->get($id);
        return $this->_downloadFile($pesan->file_lampiran);
    }

    /**
     * Uplaod Template Data file
     */
    public function upload()
    {
        $data    = [];
        $success = false;
        $message = '';

        try {
            UploadService::setUser($this->getCurrentUser());
            UploadService::setInstansiID($this->getInstansiIdFromDataOrSession());
            $uploadData        = UploadService::upload('file');
            $data['file_name'] = $uploadData['file_name'];
            $data['file_url']  = $uploadData['url'];

            $success = true;
            $message = 'Lampiran berhasil diupload';
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
        }

        $this->setResponseData($data, $success, $message);
    }

    public function readNotif($group = null)
    {

    }

    public function insertPesanDibaca()
    {
        $success    = false;
        $message    = '';
        $conditions = [];
        $user       = $this->getCurrentUser();
        $pesanId    = $this->request->getData('id');
        $conditions = [
            'id'          => $pesanId,
            'pengguna_id' => $user->id,
        ];

        $this->paginate = [
            'contain'    => [
                'PesanDibaca' => [
                    'fields' => [
                        'PesanDibaca.id', 'PesanDibaca.pesan_id', 'PesanDibaca.pegawai_id',
                    ],
                ],
            ],
            'fields'     => [
                'id', 'judul', 'grup_notifikasi',
            ],
            'conditions' => $conditions,
        ];

        $pesan            = $this->paginate($this->Pesan);
        $paging           = $this->request->params['paging']['Pesan'];
        $pesanDibacaTable = TableRegistry::get('PesanDibaca');

        if ($pesan->count() > 0) {
            $data_pesan_dibaca = [];
            $pesan_dibaca      = $pesanDibacaTable->newEntity();
            $save              = 0;
            foreach ($pesan as $idx => $value) {
                if (count($value->pesan_dibaca) == 0) {
                    $pesan_dibaca->pesan_id   = $value->id;
                    $pesan_dibaca->pegawai_id = $user->pegawai_id;
                    $pesan_dibaca->keterangan = 'sudah_dibaca';

                    $save = $pesanDibacaTable->save($pesan_dibaca);
                }
            }

            if ($save) {
                $success = true;
                $message = __('pesan_dibaca berhasil disimpan.');
            } else {
                $success = true;
                $message = __('pesan sudah dibaca. Belum ada notifikasi terbaru.');
            }
        }

        $this->setResponseData($pesan_dibaca, $success, $message);
    }
}
