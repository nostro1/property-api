<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Product Controller
 *
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
        //$this->Auth->allow(['getList']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->Product->setInstansi($this->getCurrentInstansi());

        $this->paginate = [
            'contain' => ['ProductImage', 'Ticket'],
            'conditions' => [
                'OR' => [
                    'LOWER(Product.product_name) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(Product.description) ILIKE' => '%' . $this->_apiQueryString . '%'
                ]
            ]
        ];
        $product = $this->paginate($this->Product);
        $paging = $this->request->params['paging']['Product'];
        $product = $this->addRowNumber($product);
        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $product,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Api/product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $product = $this->Product->get($id, [
            //'fields' => ['product_name', 'description', 'product_type'],
            'contain' => ['ProductImage', 'Ticket']
        ]);

        $this->setResponseData($product, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = true;
        $message = '';

        $product = $this->Product->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Product->patchEntity($product, $this->request->getData());
            if ($this->Product->save($product)) {
                $success = true;
                $message = __('Product berhasil disimpan.');
            } else {
                $message = __('Product tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($product, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Product->get($id, [
            'contain' => ['ProductImage', 'Ticket']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Product->patchEntity($product, $this->request->getData());
            if ($this->Product->save($product)) {
                $success = true;
                $message = __('Product berhasil disimpan.');
            } else {
                $message = __('Product tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($product, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $product = $this->Product->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Product->patchEntity($product, $this->request->getData());
            if ($this->Product->save($product)) {
                $success = true;
                $message = __('Product berhasil dihapus.');
            } else {
                $message = __('Product tidak berhasil dihapus. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($product, $success, $message);
    }
}
