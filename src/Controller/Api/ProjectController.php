<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Project Controller
 *
 * @property \App\Model\Table\ProjectTable $Project
 *
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProjectController extends ApiController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Project->setInstansi($this->getCurrentInstansi());
        $this->Project->setUser($this->getCurrentUser());
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';

        $this->paginate = [
            'contain' => ['PropertyUnit'],
            'conditions' => [
                'OR' => [
                    'LOWER(Project.project_name) ILIKE' => '%' . $this->_apiQueryString . '%'
                ]
            ]
        ];

        $project = $this->paginate($this->Project);
        $paging = $this->request->params['paging']['Project'];
        $project = $this->addRowNumber($project);
        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $project,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $project = $this->Project->get($id, [
            'contain' => ['PropertyUnit']
        ]);

        $this->setResponseData($project, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = true;
        $message = '';

        $project = $this->Project->newEntity();
        if ($this->request->is('post')) {
            $project = $this->Project->patchEntity($project, $this->request->getData());
            if ($this->Project->save($project)) {
                $success = true;
                $message = __('Project berhasil disimpan.');
            } else {
                $message = __('Project tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($project, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = true;
        $message = '';

        $project = $this->Project->get($id, [
            'contain' => ['PropertyUnit']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Project->patchEntity($project, $this->request->getData());
            if ($this->Project->save($project)) {
                $success = true;
                $message = __('Project berhasil diupdate.');
            } else {
                $message = __('Project tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        $this->setResponseData($project, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Project->get($id);
        if ($this->Project->delete($project)) {
            $this->Flash->success(__('The project has been deleted.'));
        } else {
            $this->Flash->error(__('The project could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
