<?php
namespace App\Controller\Api;
use App\Service\NotificationService;

/**
 * ServiceEksternal Controller
 *
 * @property \App\Model\Table\ServiceEksternalTable $ServiceEksternal
 */
class ServiceEksternalController extends ApiController
{

    public function initialize()
    {
        parent::initialize();  
        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->ServiceEksternal->setInstansi($this->getCurrentInstansi());

        $this->paginate = [
            'fields' => ['id', 'nama', 'tipe_otentikasi', 'chanel', 'status', 'base_url', 'instansi_id'],
            'conditions' => [
                'OR' => [
                    'LOWER(nama) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(deskripsi) ILIKE' => '%' . $this->_apiQueryString . '%'
                ]
            ]
        ];

        $serviceEksternal = $this->paginate($this->ServiceEksternal);
        $paging = $this->request->params['paging']['ServiceEksternal'];
        $serviceEksternal = $this->addRowNumber($serviceEksternal);
        
        $data = array(
            'limit' => $paging['perPage'],
            'page' => $paging['page'],
            'items' => $serviceEksternal,
            'total_items' => $paging['count']
        );

        $this->setResponseData($data, $success, $message);
    }

    public function getList()
    {
        $success = true;
        $message = '';
        $this->ServiceEksternal->setInstansi($this->getCurrentInstansi());

        $serviceEksternal = $this->ServiceEksternal->find('all', [
            'fields' => ['ServiceEksternal.id', 'ServiceEksternal.nama', 'ServiceEksternal.deskripsi'],
            'conditions' => [
                'OR' => [
                    'LOWER(ServiceEksternal.nama) ILIKE' => '%' . $this->_apiQueryString . '%',
                    'LOWER(ServiceEksternal.deskripsi) ILIKE' => '%' . $this->_apiQueryString . '%'
                ]
            ],
            'limit' => $this->_autocompleteLimit
        ]);

        $data = array(
            'items' => $serviceEksternal
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * Get List of Tipe Otentikasi
     *
     * @return void
     */
    public function getTipeOtentikasiList()
    {
        $success = true;
        $message = '';

        $tipeOtentikasiList = [];
        
        $tipeOtentikasiList[] = array(
            'kode' => 'No Authentication',
            'label' => 'No Authentication',
        );

        $tipeOtentikasiList[] = array(
            'kode' => 'Basic Authentication',
            'label' => 'Basic Authentication',
        );

        $data = array(
            'items' => $tipeOtentikasiList
        );

        $this->setResponseData($data, $success, $message);
    }

    /**
     * Get List of Tipe Otentikasi
     *
     * @return void
     */
    public function getChanelList()
    {
        $success = true;
        $message = '';

        $tipeOtentikasiList = [];
        
        $tipeOtentikasiList[] = array(
            'kode' => null,
            'label' => '',
        );

        $tipeOtentikasiList[] = array(
            'kode' => 'SMSGateway',
            'label' => 'SMS Gateway',
        );

        $data = array(
            'items' => $tipeOtentikasiList
        );

        $this->setResponseData($data, $success, $message);
    }
    /**
     * View method
     *
     * @param string|null $id Service Eksternal id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $success = true;
        $message = '';

        $serviceEksternal = $this->ServiceEksternal->get($id, [
            'fields' => [
                'id', 'nama', 'deskripsi', 'base_url', 'tipe_otentikasi', 'username', 'chanel'
            ],
            'contain' => []
        ]);

        $this->setResponseData($serviceEksternal, $success, $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->ServiceEksternal->setInstansi($this->getCurrentInstansi());

        $serviceEksternal = $this->ServiceEksternal->newEntity();
        
        if ($this->request->is('post')) {
            $serviceEksternal = $this->ServiceEksternal->patchEntity($serviceEksternal, $this->request->getData());
            
            if ($this->ServiceEksternal->save($serviceEksternal)) {
                $success = true;
                $message = __('Service eksternal berhasil disimpan.');
            } else {
                $message = __('Service eksternal tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }
        
        $this->setResponseData($serviceEksternal, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Service Eksternal id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $message = '';

        $serviceEksternal = $this->ServiceEksternal->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $serviceEksternal = $this->ServiceEksternal->patchEntity($serviceEksternal, $this->request->getData());
            
            if ($this->ServiceEksternal->save($serviceEksternal)) {
                $success = true;
                $message = __('Service eksternal berhasil disimpan.');
            } else {
                $message = __('Service eksternal tidak berhasil disimpan. Silahkan coba kembali.');
            }
        }

        $this->setResponseData($serviceEksternal, $success, $message);
    }

    /**
     * Delete method
     *
     * @param string|null $id Service Eksternal id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $success = false;
        $message = '';
        $data = [];
        $this->ServiceEksternal->setInstansi($this->getCurrentInstansi());

        $this->request->allowMethod(['post', 'delete']);
        $serviceEksternal = $this->ServiceEksternal->get($id);
        
        if ($this->ServiceEksternal->delete($serviceEksternal)) {
            $success = true;
            $message = __('Service eksternal berhasil dihapus.');
        } else {
            $message = __('Service eksternal tidak berhasil dihapus. Silahkan coba kembali.');
        }

        $this->setResponseData($data, $success, $message);
    }

    public function connect() {
        $success = true;
        $message = '';

        $data = $this->request->getData();
        $chanel =  $this->getInstansiIdFromDataOrSession() . "-" . $data['chanel'];

        if ($this->getInstansiIdFromDataOrSession() == $data['instansi_id']) {
            $success = NotificationService::pushNotif(
                $data['nama'], 
                'service_eksternal', 
                $chanel, 
                "Init Service", 
                null, 
                $data['base_url']
            );
    
            if ($success == false) {
                $message = "Ada masalah koneksi ke server silahkan cek service yang ada";
            } else {
                $serviceEksternal = $this->ServiceEksternal->get($data['id']);
                $data['status'] = 'aktif';
        
                if ($this->request->is(['post'])) {
                    $serviceEksternal = $this->ServiceEksternal->patchEntity($serviceEksternal, $data);
                    if ($this->ServiceEksternal->save($serviceEksternal)) {
                        $success = true;
                        $message = __('Service eksternal Aktif');
                    } else {
                        $success = false;
                        $message = __('Anda tidak berhak mengaktifkan service ini');
                    }
                }
            }
        } else {
            $success = false;
            $message = __('Anda tidak berhak mengaktifkan service ini');
        }
        
        $this->setResponseData($data, $success, $message); 
    }
}
