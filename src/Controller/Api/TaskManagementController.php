<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * TaskManagement Controller
 *
 * @property \App\Model\Table\TaskManagementTable $TaskManagement
 *
 * @method \App\Model\Entity\TaskManagement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TaskManagementController extends ApiController
{

    public function initialize()
    {
        parent::initialize();
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $success = true;
        $message = '';
        $this->TaskManagement->setInstansi($this->getCurrentInstansi());

        $this->paginate = [
            'contain' => []
        ];
        $taskManagement = $this->paginate($this->TaskManagement);

        $paging = $this->request->params['paging']['TaskManagement'];
        $taskManagement = $this->addRowNumber($taskManagement);
        $data = array(
            'limit' => $paging['limit'],
            'page' => $paging['page'],
            'items' => $taskManagement,
            'total_items' => $paging['count']
        );

        $this->setResponseData($taskManagement, $success, $message);
    }

    /**
     * View method
     *
     * @param string|null $id Task Management id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $taskManagement = $this->TaskManagement->get($id, [
            'contain' => ['Instansis', 'Lfts', 'Rghts', 'ParentTaskManagement', 'ChildTaskManagement']
        ]);

        $this->set('taskManagement', $taskManagement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $taskManagement = $this->TaskManagement->newEntity();
        if ($this->request->is('post')) {
            $taskManagement = $this->TaskManagement->patchEntity($taskManagement, $this->request->getData());
            if ($this->TaskManagement->save($taskManagement)) {
                $this->Flash->success(__('The task management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The task management could not be saved. Please, try again.'));
        }
        $instansis = $this->TaskManagement->Instansis->find('list', ['limit' => 200]);
        $lfts = $this->TaskManagement->Lfts->find('list', ['limit' => 200]);
        $rghts = $this->TaskManagement->Rghts->find('list', ['limit' => 200]);
        $parentTaskManagement = $this->TaskManagement->ParentTaskManagement->find('list', ['limit' => 200]);
        $this->set(compact('taskManagement', 'instansis', 'lfts', 'rghts', 'parentTaskManagement'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Task Management id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $taskManagement = $this->TaskManagement->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $taskManagement = $this->TaskManagement->patchEntity($taskManagement, $this->request->getData());
            if ($this->TaskManagement->save($taskManagement)) {
                $this->Flash->success(__('The task management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The task management could not be saved. Please, try again.'));
        }
        $instansis = $this->TaskManagement->Instansis->find('list', ['limit' => 200]);
        $lfts = $this->TaskManagement->Lfts->find('list', ['limit' => 200]);
        $rghts = $this->TaskManagement->Rghts->find('list', ['limit' => 200]);
        $parentTaskManagement = $this->TaskManagement->ParentTaskManagement->find('list', ['limit' => 200]);
        $this->set(compact('taskManagement', 'instansis', 'lfts', 'rghts', 'parentTaskManagement'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Task Management id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $taskManagement = $this->TaskManagement->get($id);
        if ($this->TaskManagement->delete($taskManagement)) {
            $this->Flash->success(__('The task management has been deleted.'));
        } else {
            $this->Flash->error(__('The task management could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
