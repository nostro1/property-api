<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Workflow Controller
 *
 * @property \App\Model\Table\WorkflowTable $Workflow
 *
 * @method \App\Model\Entity\Workflow[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WorkflowController extends ApiController
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->Auth->allow(['']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Instansis', 'Objeks', 'JenisProses', 'Forms', 'TemplateData', 'Penggunas']
        ];
        $workflow = $this->paginate($this->Workflow);

        $this->set(compact('workflow'));
    }

    /**
     * View method
     *
     * @param string|null $id Workflow id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $workflow = $this->Workflow->get($id, [
            'contain' => ['Instansis', 'Objeks', 'JenisProses', 'Forms', 'TemplateData', 'Penggunas']
        ]);

        $this->set('workflow', $workflow);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $success = false;
        $message = '';
        $this->Workflow->setInstansi($this->getCurrentInstansi());

        $workflow = $this->Workflow->newEntity();
        if ($this->request->is('post')) {
            $workflow = $this->Workflow->patchEntity($workflow, $this->request->getData());
            if ($this->Workflow->save($workflow)) {
                $success = true;
                $message = __('Dokumen berhasil disimpan');
            } else {
                $message = __('Dokumen tidak berhasil disimpan');
            }
        }
        $this->setResponseData($workflow, $success, $message);
    }

    /**
     * Edit method
     *
     * @param string|null $id Workflow id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $workflow = $this->Workflow->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $workflow = $this->Workflow->patchEntity($workflow, $this->request->getData());
            if ($this->Workflow->save($workflow)) {
                $this->Flash->success(__('The workflow has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The workflow could not be saved. Please, try again.'));
        }
        $instansis = $this->Workflow->Instansis->find('list', ['limit' => 200]);
        $objeks = $this->Workflow->Objeks->find('list', ['limit' => 200]);
        $jenisProses = $this->Workflow->JenisProses->find('list', ['limit' => 200]);
        $forms = $this->Workflow->Forms->find('list', ['limit' => 200]);
        $templateData = $this->Workflow->TemplateData->find('list', ['limit' => 200]);
        $penggunas = $this->Workflow->Penggunas->find('list', ['limit' => 200]);
        $this->set(compact('workflow', 'instansis', 'objeks', 'jenisProses', 'forms', 'templateData', 'penggunas'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Workflow id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $success = true;
        $message = '';

        $this->request->allowMethod(['post', 'delete']);
        $workflow = $this->Workflow->get($id);
        if ($this->Workflow->delete($workflow)) {
           $success = true;
            $message = __('Dokumen berhasil disimpan');
        } else {
            $message = __('Dokumen tidak berhasil disimpan');
        }

        $this->setResponseData($workflow, $success, $message);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function workflowByObjekId($id)
    {
        $success = true;
        $message = '';
        $this->Workflow->setInstansi($this->getCurrentInstansi());
        
        $workflow = $this->Workflow->find('all', [
            'contain' => ['Pengguna'],
            'conditions' => [
                'Workflow.del' => 0,
                'Workflow.objek_id' => $id
            ]
        ]);

        $data = array(
            'items' => $workflow
        );
 
        $this->setResponseData($data, $success, $message);
    }
}
