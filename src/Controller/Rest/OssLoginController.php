<?php
namespace App\Controller\Rest;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use App\Service\DataSincService;
use App\Service\OssService;
use Cake\I18n\Time;
use App\Model\Entity\Pegawai;
use App\Model\Entity\Pengguna;
use App\Service\AlurProsesService;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * OssLogin Controller
 *
 *
 * @method \App\Model\Entity\OssLogin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OssLoginController extends RestController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['getSsoToken', 'getUsername']);
        $this->ssoURL = 'https://services.oss.go.id/';
        $this->statusId = null;
        $this->logId = null;
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Pegawai = $this->loadModel('Pegawai');
        $this->Pengguna = $this->loadModel('Pengguna');
    }

    private function getKey() {
        $status = true;
        $responData = null;
        $message = 'Terjadi masalah komunikasi dengan OSS';

        try {
            $serviceTable = TableRegistry::get('service_eksternal');
            $serviceData = $serviceTable->find()->select()
                ->where(['nama' => 'OSS'])
                ->first();

            //Get Oss key
            if (!empty($serviceData)) {
                $data = [
                    'securityKey' => [
                        'user_akses' => $serviceData['username'],
                        'pwd_akses' => $serviceData['password']
                    ]
                ];

                $http = new Client();
                $response = $http->post(
                    $this->ssoURL . 'sendSecurityKey',
                    json_encode($data),
                    [
                        'type' => 'json'
                    ]
                );

                //If Succes get the key then get the Token
                if ($response->code == 200) {
                    $key = json_decode($response->body);
                    $return = [
                        'status' => true,
                        'key' => $key->responsendSecurityKey->key
                    ];
                }
            }
        } catch (\Exception $e) {
            $this->logId = DataSincService::setLog(
                $this->logId,
                [
                    'proses' => 'Koneksi bermasalah',
                    'status' => false,
                    'data' => json_encode($e->getMessage())
                ]
            );

            $return = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        return $return;
    }

    public function getSsoToken()
    {
        $status = true;
        $responData = null;
        $message = 'Terjadi masalah komunikasi dengan OSS';

        try {
            $serviceTable = TableRegistry::get('service_eksternal');
            $serviceData = $serviceTable->find()->select()
                ->where(['nama' => 'OSS'])
                ->first();

            //Get Oss key
            if (!empty($serviceData)) {
                $data = [
                    'securityKey' => [
                        'user_akses' => $serviceData['username'],
                        'pwd_akses' => $serviceData['password']
                    ]
                ];

                $http = new Client();
                $response = $http->post(
                    $this->ssoURL . 'sendSecurityKey',
                    json_encode($data),
                    [
                        'type' => 'json',
                        'auth' => ['username' => '', 'password' => '']
                    ]
                );

                //If Succes get the key then get the Token
                if ($response->code == 200) {
                    $key = json_decode($response->body);

                    $data = [
                        'ATTACHSSO' => [
                            'broker_id' => sha1('SSO_SICANTIK_010'.Time::now()->format('Y-m-d')),'broker_secret' => sha1('8izwiL9trwF'.Time::now()->format('Y-m-d'))
                            ]
                        ];

                    $this->logId = DataSincService::setLog(
                        $this->logId,
                        [
                            'proses'=>'Init get token ',
                            'status' => true,
                            'data' => [
                                'data' => $data,
                                'key' => $key->responsendSecurityKey->key]
                        ]
                    );

                    //Get the oken using the key
                    $http = new Client();
                    $response = $http->post(
                        $this->ssoURL . 'sso/attachSSO',
                        json_encode($data),
                        [
                            'type' => 'json',
                            'headers' => [
                                'OSS-API-KEY' => $key->responsendSecurityKey->key
                            ]
                        ]
                    );

                    //If succes get token then Login to OSS
                    if ($response->code == 200) {
                        $this->logId = DataSincService::setLog(
                            $this->logId,
                            [
                                'proses'=>'Pengambilan token berhasil ',
                                'status' => true,
                                'data' => json_decode($response->body)
                            ]
                        );

                        $ossToken = json_decode($response->body)->responattachSSO->token;
                        $reqData = $this->request->data;
                        $data = [
                            'LOGINSSO' => [
                                'username' => $reqData['username'],
                                'password' => $reqData['password'],
                                'token' => $ossToken
                            ]
                        ];

                        //Login to OSS
                        $http = new Client();
                        $response = $http->post(
                            $this->ssoURL . '/sso/loginSSO',
                            json_encode($data),
                            [
                                'type' => 'json',
                                'headers' => [
                                    'OSS-API-KEY' => $key->responsendSecurityKey->key
                                ]
                            ]
                        );

                        if ($response->code == 200) {
                            $responLogin = json_decode($response->body)->responloginSSO->data_user;

                            $this->logId = DataSincService::setLog(
                                $this->logId,
                                [
                                    'proses'=>'Login ke OSS berhasil',
                                    'status' => true,
                                    'data' => $responLogin
                                ]
                            );

                            //TODO : Akan di tambah fungsi untuk konversi kode instansi, menunggu aturan dari OSS
                            if ($responLogin->role != 11) {
                                $pegawai = $this->Pegawai->newEntity();
                                $pegawai['nama'] = $responLogin->fullname;
                                $pegawai['nip'] = $responLogin->nik;
                                $pegawai['no_hp'] = $responLogin->hp;
                                $pegawai['instansi_id'] = DataSincService::getInstansiId($responLogin->kd_instansi);

                                if (!$this->Pegawai->save($pegawai)) {
                                    $this->logId = DataSincService::setLog(
                                        $this->logId,
                                        [
                                            'proses' => 'Simpan data pegawai error',
                                            'status' => false,
                                            'data' => $pegawai,
                                            'keterangan' => $pegawai->getErrors()
                                        ]
                                    );
                                    $status = false;
                                } else {
                                    $pegawai['pengguna'] = $this->Pengguna->newEntity();
                                    $pegawai['pengguna']['username'] = $reqData['username'];
                                    $pegawai['pengguna']['password'] = $reqData['password'];
                                    $pegawai['pengguna']['email'] = $responLogin->email;
                                    $pegawai['pengguna']['peran_id'] = 2;
                                    $pegawai['pengguna']['pegawai_id'] = $pegawai['id'];

                                    if (!$this->Pengguna->save($pegawai['pengguna'])) {
                                        $this->logId = DataSincService::setLog(
                                            $this->logId,
                                            [
                                                'proses' => 'Simpan data pengguna error',
                                                'status' => false,
                                                'data' => $pegawai['pengguna'],
                                                'keterangan' => $pegawai->getErrors()
                                            ]
                                        );
                                        $status = false;
                                    } else {
                                        $message = 'Login berhasil';
                                    }
                                }
                            } else {
                                $status = false;
                                $message = 'Hanya role OSS admin yang bisa login';
                            }
                            

                        } else {
                            $this->logId = DataSincService::setLog(
                                $this->logId,
                                [
                                    'proses'=>'Login ke OSS gagal dengan username : ' . $reqData['username'],
                                    'status' => false,
                                    'data' => json_decode($response->body)
                                ]
                            );

                            $message = 'Silahkan periksa kembali username dan password anda';
                            $status = false;
                        }

                    } else {
                        $this->logId = DataSincService::setLog(
                            $this->logId,
                            [
                                'proses'=>'Pengambilan token gagal ',
                                'status' => false,
                                'data' => json_decode($response->body)
                            ]
                        );

                        $message = 'Pengambilan token gagal';
                        $status = false;
                    }

                } else {
                    $this->logId = DataSincService::setLog(
                        $this->logId,
                        [
                            'proses' => 'Pengambilan key gagal',
                            'status' => false,
                            'data' => json_decode($response->body)
                        ]
                    );

                    $message = 'Pengambilan key gagal';
                    $status = false;
                }

            } else {
                $this->logId = DataSincService::setLog(
                    $this->logId,
                    [
                        'proses'=>'Seting akses belum di definisikan',
                        'status' => false,
                        'data' => null
                    ]
                );

                $status = false;
            }

        } catch (\Exception $e) {
            $this->logId = DataSincService::setLog(
                $this->logId,
                [
                    'proses'=>'Koneksi bermasalah ',
                    'status' => false,
                    'data' => json_encode($e)
                ]
            );

            $status = false;
        }

        $this->setResponseData($responData, $status, $message);
    }
    
    public function getUsername($token, $kdIzin) {
        $status = true;
        $responData = null;
        $message = 'Terjadi masalah komunikasi dengan OSS';

        $getKey = $this->getKey();

        if ($getKey['status']) {
            $data = [
                'GETPROFILE' => [ 
                    'user_token' => $token 
                ]
            ];

            $http = new Client();
            $response = $http->post(
                $this->ssoURL . '/sso/getProfile',
                json_encode($data),
                [
                    'type' => 'json',
                    'headers' => [
                        'OSS-API-KEY' => $getKey['key']
                    ]
                ]
            );
            
            $this->logId = DataSincService::setLog(
                $this->logId,
                [
                    'proses'=>'Get profile',
                    'status' => true,
                    'data' => json_decode($response->body)
                ]
            );

            if ($response->code == 200) {
                //Validasi apakah user sudah terdaftar
                $ossUser = json_decode($response->body);
                $sicantikUser = $this->validate($ossUser->respongetProfile->data_user->username, $kdIzin);
                $responData = [
                    'username' => $ossUser->respongetProfile->data_user->username,
                    'token' => $sicantikUser['token'],
                    'redirect' => $sicantikUser['redirect']
                ];
                $message = 'Validasi user';
            } else {
                $status = false;
                $message = 'Terjadi masalah saat get user';
                $responData = [
                    'token' => $token,
                    'key' => $getKey['key'],
                    'respon' => json_decode($response->body),
                ];
            }
        } else {
            $status = false;
            $message = 'Terjadi masalah saat get key';
        }
        
        $this->setResponseData($responData, true, $message);
    }

    private function validate($username, $kdIzin)
    {
        $data = [
            'token' => null,
            'redirect' => null,
        ];
        $instansiId = null;
        $token = null;

        $this->PermohonanIzin = $this->loadModel('PermohonanIzin');

        $pengguna = $this->Pengguna->find('all', [
                'fields' => [
                    'id', 'password'
                ],
                'contain' => [
                    'Peran' => ['fields' => ['instansi_id']],
                    'Pegawai' => ['fields' => ['instansi_id']]
                ],
                'conditions' => [
                    'username' => $username
                ]
            ])
            ->first();

        if (!empty($pengguna)) {
            if (!is_null($pengguna['pegawai']['instansi_id'])) {
                $instansiId = $pengguna['pegawai']['instansi_id'];
            } elseif (!is_null($pengguna['peran']['instansi_id'])) {
                $instansiId = $pengguna['pegawai']['peran'];
            }

            $expireTime = 12 * 3600;

            $token = JWT::encode([
                'sub' => $pengguna['id'],
                'exp' =>  time() + $expireTime
            ], Security::salt());

            $data['token'] = $token;

            if (!is_null($instansiId)) {
                $permohonanIzin = $this->PermohonanIzin->find('all', [
                        'fields' => ['id'],
                        'conditions' => [
                            'instansi_id' => $instansiId,
                            'kd_izin' => $kdIzin
                        ]
                ])
                ->first();

                if (!empty($permohonanIzin)) {
                    $alurProsesTbl = TableRegistry::get('proses_permohonan');

                    $alurProses = $alurProsesTbl->find()
                        ->select(['tautan', 'form_id', 'template_data_id'])
                        ->where([
                            'permohonan_izin_id' => $permohonanIzin['id'],
                            'status' => AlurProsesService::STATUS_PROSES,
                            'tautan !=' => AlurProsesService::TAUTAN_REPORT
                        ])
                        ->first();

                    if (empty($alurProses)) {
                        $alurProses = $alurProsesTbl->find()
                            ->select(['id', 'tautan', 'form_id', 'template_data_id'])
                            ->where([
                                'permohonan_izin_id' => $permohonanIzin['id'],
                                'status' => AlurProsesService::STATUS_SELESAI,
                                'tautan !=' => AlurProsesService::TAUTAN_REPORT
                            ])
                            ->order(['id' => 'DESC'])
                            ->first();

                    }

                    switch ($alurProses['tautan']) {
                        case AlurProsesService::TAUTAN_FORM_ADD:
                            $data['redirect'] = str_replace("-", "/", AlurProsesService::TAUTAN_FORM_ADD) .'/' . $alurProses['form_id'] . '/' . $permohonanIzin['id'] . '/' . $alurProses['id'];
                            break;
                        case AlurProsesService::TAUTAN_PERMOHONAN_EDIT:
                            $data['redirect'] = str_replace("-edit", "/edit", AlurProsesService::TAUTAN_PERMOHONAN_EDIT) . '/' . $permohonanIzin['id'];
                            break;
                        case AlurProsesService::TAUTAN_SIGNATURE:
                            $data['redirect'] = str_replace("-", "/", AlurProsesService::TAUTAN_SIGNATURE) . '/' . $permohonanIzin['id'] . '/' . $alurProses['template_data_id'];
                            break;
                        default:
                            $data['redirect'] = AlurProsesService::TAUTAN_PROSES_NIB;
                    }
                    
                } else {
                    $data['redirect'] = AlurProsesService::TAUTAN_PROSES_NIB;
                }
            }
        }
        
        return $data;
    }
}
