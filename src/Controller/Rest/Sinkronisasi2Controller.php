<?php

namespace App\Controller\Rest;

use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Cake\I18n\Time;
use App\Service\AlurProsesService;
use App\Service\DataSincService;
use App\Service\OssService;
use Cake\Datasource\ConnectionManager;
use XPDF\PdfToText;
use App\Service\FileService;

class Sinkronisasi2Controller extends RestController
{
    public function initialize()
    {
        parent::initialize();
        $this->autoRender = false;
        $this->Auth->allow(['postNIB', 'postData', 'testApi', 'cleanJD', 'readImg']);
        $this->status = true;
        $this->keterangan = [];
        $this->idx = -1;
        $this->logId = null;
        $this->logKeterangan = null;
        $this->withReturn = true;
        $this->statusId = null;
        $this->logId = null;
    }

    /**
     * This will be called via eval from postData.
     * @param null $nibData
     */
    private function permohonan($nibData = null)
    {
        $instansiId = null;
        $permohonan = null;
        $jenisIzinId = null;

        $this->logId = DataSincService::setLog(
            $this->logId,
            ['proses' => 'Create permohonan ',
                'status' => true,
                'data' => json_encode($nibData),
                'keterangan' => 'Init create permohonan'
            ]
        );

        $tmp = substr($nibData['kd_daerah'], 0, 4);

        if (substr($tmp, 2, 2) != "00") {
            $kab = substr($tmp, 0, 2) . "." . substr($tmp, 2, 2);
        } else {
            $kab = substr($tmp, 0, 2);
        }

        $jenis_izin = substr($nibData['kd_izin'], 9, 3);
        $InstansiTbl = TableRegistry::get('unit');
        $InstansiData = $InstansiTbl->find()->select();

        if (substr($nibData['kd_daerah'], 0, 4) == '00000') {
            $InstansiData->where(['instansi_code' => substr($nibData['kd_izin'], 0, 3), 'tipe' => 'I']);
        } else {
            $InstansiData->where(['kode_daerah' => $kab, 'tipe' => 'I']);
        }
        $instansiId = $InstansiData->first()['id'];

        if (empty($instansiId)) {
            //Jika tidak di kenali instansinya
            $this->status = false;
            $this->logId = DataSincService::setLog(
                $this->logId,
                ['proses' => 'Get Instansi ',
                    'status' => false,
                    'data' => null,
                    'keterangan' => 'Instansi dengan kode ' . $kab . ' tidak ditemukan '
                ]
            );

            $this->statusId = DataSincService::setStatusOSS(
                $this->statusId,
                false,
                ['status' => 2,
                    'kd_izin' => $nibData['kd_izin'],
                    'id_izin' => $nibData['id_izin'],
                    'uraian' => 'Instansi dengan kode ' . $kab . ' tidak ditemukan '
                ]
            );
        } else {
            //Jika dikenali ambil jenis izin yg ada
            $jiTbl = TableRegistry::get('jenis_izin');
            $jiData = $jiTbl->find()->select();
            $jiData->where(['instansi_id' => $instansiId, 'kode_oss' => $jenis_izin]);

            $tipePemohon = empty($this->perusahaan_id) ? 'PERORANGAN' : 'PERUSAHAAN';

            if ($jiData->count() > 0) {
                foreach ($jiData as $ji) {
                    $jenisIzinId = $ji['id'];
                    //permohona sudah ada atau tidak
                    if (!$this->permohonanExist($this->nib, $this->oss_id, $nibData)) {
                        $permohonan = AlurProsesService::registerPermohonanIzin(
                            ['pemohon_id' => $this->pemohon_id,
                                'perusahaan_id' => $this->perusahaan_id,
                                'jenis_izin_id' => $ji['id'],
                                'tipe_pemohon' => $tipePemohon,
                                'jenis_permohonan' => 'Baru',
                                'instansi_id' => $instansiId,
                                'nib_id' => $this->nib,
                                'oss_id' => $this->oss_id,
                                'kode_oss' => $nibData['kd_izin'],
                                'kd_izin' => $nibData['id_izin'],
                                'is_active' => FALSE,
                                'id_proyek' => $nibData['id_proyek'],
                                'kd_daerah' => $nibData['kd_daerah'],
                                'dibuat_oleh' => 'OSS010',
                                'tgl_dibuat' => Time::now()
                            ]
                        );

                        if ($permohonan) {
                            $this->statusId = DataSincService::setStatusOSS(
                                $this->statusId,
                                true,
                                ['status' => 1,
                                    'kd_izin' => $nibData['kd_izin'],
                                    'id_izin' => $nibData['id_izin'],
                                    'uraian' => 'Permohonan dengan jenis izin ' . $nibData['kd_izin'] . ' Terbentuk di daerah kode ' . $kab . '|' . $this->nib
                                ]
                            );
                        } else {
                            $this->status = false;
                            $this->statusId = DataSincService::setStatusOSS(
                                $this->statusId,
                                true,
                                ['status' => 1,
                                    'kd_izin' => $nibData['kd_izin'],
                                    'id_izin' => $nibData['id_izin'],
                                    'uraian' => json_encode(AlurProsesService::getErrors())
                                ]
                            );
                        }
                    } else {
                        $this->statusId = DataSincService::setStatusOSS(
                            $this->statusId,
                            true,
                            ['status' => 1,
                                'kd_izin' => $nibData['kd_izin'],
                                'id_izin' => $nibData['id_izin'],
                                'uraian' => 'Data Permohonan sudah di perbarui'
                            ]
                        );
                    }
                }
            } else {
                $this->logId = DataSincService::setLog(
                    $this->logId,
                    ['proses' => 'Get Jenis izin ',
                        'status' => false,
                        'data' => null,
                        'keterangan' => 'Jenis izin dengan kode ' . $jenis_izin . ' tidak ditemukan '
                    ]
                );

                $this->statusId = DataSincService::setStatusOSS(
                    $this->statusId,
                    false,
                    ['status' => 2,
                        'kd_izin' => $nibData['kd_izin'],
                        'id_izin' => $nibData['id_izin'],
                        'uraian' => 'Jenis izin dengan kode ' . $jenis_izin . ' tidak ditemukan '
                    ]
                );
            }
        }
    }

    public function postNIB($sincId = null)
    {
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '2G');

        $this->nib_id = null;
        $this->withReturn = false;
        $this->response = $this->response->withType('json');

        $data = $this->request->getData();
        $this->nib_data = $data;

        $nibTbl = TableRegistry::get('data_nib');
        $nibData = $nibTbl->find()->select();
        $nibData->where([
            'nib' => $data['dataNIB']['nib'],
            'tipe_dokumen' => $data['dataNIB']['tipe_dokumen'],
            'tipe_dokumen <>' => 5
        ]);

        $this->logId = DataSincService::setLog($this->logId,
            ['proses' => 'Init postnib ',
                'status' => true,
                'data' => $data,
                'keterangan' => null
            ]
        );

        if ($nibData->count() > 0) {
            $response = $this->response->withStringBody(json_encode([
                'STATUS' => 2,
                'KETERANGAN' => 'NIB Sudah diproses'
            ]));

            $this->logId = DataSincService::setLog($this->logId,
                ['proses' => 'Verifikasi postnib ',
                    'status' => true,
                    'data' => $nibData,
                    'keterangan' => 'NIB Sudah diproses'
                ]
            );

            return $response;
        } else {
            //cek tipe dokumennya
            //jika baru langsung proses
            //jika penolakan lsg penetapan drop
            /*
            (9:Original, 5:Update, 3:Pencabutan, 4:Pembatalan)
            */
            if ($data['dataNIB']['tipe_dokumen'] == 3 || $data['dataNIB']['tipe_dokumen'] == 4) {
                $permohonanIzinTbl = TableRegistry::get('permohonan_izin');
                $permohonanIzinTbl->updateAll(
                    ['status_penetapan' => 'ditolak', 'catatan_rekomendasi' => 'perubahan status dari oss'],
                    ['nib' => $data['dataNIB']['nib']]
                );
            } else {
                $this->postData($sincId);
            }
        }

        $procRespon = DataSincService::getResponse($this->statusId);

        $status = $procRespon['status'] ? 1 : 2;
        $detail = is_array($procRespon['detail']) ? $procRespon['detail'] : array(json_decode($procRespon['detail']));

        $response = $this->response->withStringBody(
            json_encode(['responreceiveNIB' => [
                'status' => $status,
                'keterangan' => $detail
            ]
            ])
        );


        /*         $response["responreceiveNIB"] = json_decode($procRespon);
                $response = $this->response->withStringBody(json_encode($response)); */
        return $response;
    }

    public function postData($sincId = null, $fdata = null, $fkey = null, $fkeydata = null)
    {
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '2G');

        $key = null;
        $recData = null;
        $proses = null;
        $mainPost = false;
        $exist = 0;

        //screening berdasarkan pola data perlayer
        if (empty($fdata)) { //inisialisasi awal
            $data = $this->request->getData();
            if (is_null($this->logId)) {
                $this->logId = DataSincService::setLog($this->logId,
                    ['proses' => 'Init postdata ',
                        'status' => true,
                        'data' => $data,
                        'keterangan' => $this->request->getQuery('proses')
                    ]
                );
            }
        } else { //data turunan
            $data = $fdata;
        }

        $sinchron = TableRegistry::get('data_sinc');
        $sinc = $sinchron->find()->select(['id', 'index' => 'index', 'key' => 'key', 'proses' => 'proses']);
        $sinc->where(['id' => $sincId]);

        if (!empty($sinc)) {
            foreach ($sinc as $s) {
                if (array_key_exists($s['index'], $data)) {
                    $recData = $data[$s['index']];
                    $proses = $s['proses'];
                    $key = !empty($fkey) ? $fkey : $s['key'];
                }
            }

            if (!empty($recData)) {
                foreach ($recData as $idx => $val) { //validasi array atau objek
                    $index = $idx;
                    break;
                }

                if (is_string($index)) { //single row
                    if (!empty($proses)) {
                        call_user_func(array($this, $proses), $recData[$i]);
                    }
                    $this->saveData($sincId, $recData, $key, $fkeydata);
                } else { //multi row
                    for ($i = 0; $i < count($recData);) {
                        if (!empty($proses)) {//proses fungsi
                            call_user_func(array($this, $proses), $recData[$i]);
                        }
                        $this->saveData($sincId, $recData[$i], $key, $fkeydata);
                        $i = $i + 1;
                    }
                }
            } else {
                $this->statusId = DataSincService::setStatusOSS($this->statusId, false,
                    ['status' => 2,
                        'keterangan' => 'Periksa kembali mapingan anda ' . $sincId
                    ]
                );
            }
        }

        if ($this->withReturn && empty($fdata)) {
            $proses = $this->request->getQuery('proses');

            if (!is_null($this->statusId)) {
                $procRespon = DataSincService::getResponse($this->statusId);
            } else {
                $procRespon['status'] = 1;
                $procRespon['detail'] = $proses . ' Berhasil dijalankan';
                $procRespon = $procRespon;
            }

            if (is_array($procRespon['detail'])) {
                $procRespon = json_encode($procRespon['detail']);
            }

            if (!is_null($proses)) {
                $respons["respon" . $proses] = json_decode($procRespon['detail']);
            } else {
                $respons = json_decode($procRespon);
            }
            $response = $this->response->withStringBody(json_encode($respons));
            return $response;
        }
    }

    public function saveData($sincId = null, $Data = null, $fkey = null, $fkeydata = null)
    {
        $keydata = null;
        $condition = [];
        $keterangan = null;

        if (!empty($Data)) {
            $columns = TableRegistry::get('data_sinc_detail');
            $column = $columns->find();

            $column->select([
                'tabel' => 'c.nama_datatabel',
                'kolom' => 'b.data_kolom',
                'panjang' => 'b.panjang',
                'rec_kolom' => 'data_sinc_detail.oss_kolom',
                'is_key' => 'data_sinc_detail.is_key', 'value_type' => 'data_sinc_detail.value_type',
                'maping' => 'data_sinc_detail.maping'
            ]);
            $column->innerjoin(['b' => 'data_kolom'], ['b.id = data_sinc_detail.data_kolom_id']);
            $column->innerjoin(['c' => 'datatabel'], ['c.id = b.datatabel_id']);
            $column->innerjoin(['d' => 'data_sinc'], ['d.id=data_sinc_detail.data_sinc_id']);

            $column->where(['data_sinc_id' => $sincId, 'data_sinc_detail.del' => 0]);

            $column->order(['c.nama_datatabel']);
            $activeTable = null;
            $tables = null;

            foreach ($column as $c) { //screening maping per layer API
                $id = null;

                if ($activeTable != $c['tabel']) {
                    if (!empty($table)) {
                        $table['tgl_dibuat'] = Time::now();
                        $table['dibuat_oleh'] = 'Sinc System';
                        $table['status_id'] = $this->statusId;
                        $table['log_id'] = $this->logId;
                        //validasi jika data sudah pernah ada sesuai kolom kunci maka akan update
                        if (!empty($condition)) {
                            $table = $this->cekData($condition, $activeTable, $table);
                        }

                        try {
                            if ($tables->save($table)) {
                                $status = true;
                                if (!empty($condition)) {
                                    $this->statusId = DataSincService::setStatusOSS(
                                        $this->statusId,
                                        $status,
                                        [
                                            'status' => 1,
                                            'uraian' => 'Update data '.$activeTable.' berhasil'
                                        ]
                                    );
                                }

                                $this->statusId = $table['status_id'];
                            } else {
                                $status = false;
                                $this->statusId = DataSincService::setStatusOSS(
                                        $this->statusId,
                                        $status,
                                        [
                                            'status' => 2,
                                            'uraian' => 'Terjadi masalah saat menyimpan data '.$activeTable
                                        ]
                                    );
                                $keterangan = $table->getErrors();
                            }
                        } catch (\Exception $e) {
                            $status = false;
                            $this->statusId = DataSincService::setStatusOSS(
                                    $this->statusId,
                                    $status,
                                    [
                                        'status' => 2,
                                        'uraian' => 'Terjadi masalah saat menyimpan data :'.$activeTable
                                    ]
                            );
                            $keterangan = $e->getMessage();
                        }

                        $this->logId = DataSincService::setLog(
                            $this->logId,
                            [
                                'proses' => 'Simpan data ' . $activeTable,
                                'status' => $status,
                                'data' => $table,
                                'keterangan' => $keterangan
                            ]
                        );

                        $condition = [];

                        //set global variabel yang bisa diakses oleh keseluruhan fungsi
                        if ($table['id']) {
                            eval('$this->' . $activeTable . '_id = ' . $table['id'] . ';');
                        }

                        $tableKey = substr($fkey, 0, strlen($fkey) - 3);
                        if ($tableKey == $activeTable) {
                            $fkeydata = $table['id'];
                        }
                    }
                    $activeTable = $c['tabel'];
                    $tables = TableRegistry::get($activeTable);

                    $table = $tables->newEntity();

                    if (!empty($fkeydata)) {
                        $table[$fkey] = $fkeydata;
                    }
                }
                //Maping kolom
                if ($c['value_type'] == 'ref_index') {//original nama kolom di data
                    if (array_key_exists($c['rec_kolom'], $Data)) {
                        $table[$c['kolom']] = substr($Data[$c['rec_kolom']],0,$c['panjang']);
                        /*
                        Begin - Proses konversi ke nilai yang di kenali sistem
                        */
                        if (!is_null($c['maping'])) {
                            //dari di maping ke data_umum
                            $mapingTbl = TableRegistry::get('c_data_umum');

                            $maping = $mapingTbl->find()->select()
                                ->where(['grup_kode' => $c['maping'],
                                    'kode' => $Data[$c['rec_kolom']]
                                ])
                                ->first();

                            $table[$c['kolom']] = $maping['keterangan'];
                        }
                        /*
                        End - Proses konversi ke nilai yang di kenali sistem
                        */
                    }
                } elseif ($c['value_type'] == 'value') {
                    $table[$c['kolom']] = $c['rec_kolom'];
                } elseif ($c['value_type'] == 'ref_object') {
                    eval('$table["' . $c['kolom'] . '"] = ' . $c['rec_kolom'] . ";");
                }

                if ($c['is_key'] == 'x') {
                    //if ($c['value_type'] != 'ref_object') {
                        $this->{$c['kolom']} = $table[$c['kolom']];//$Data[$c['rec_kolom']];
                        $condition[$c['kolom']] = $table[$c['kolom']];//$Data[$c['rec_kolom']];
                    /* } else {
                        eval('$this->' . $c['kolom'] . ' = ' . $c['rec_kolom'] . ";");
                        eval('$value = ' . $c['rec_kolom'] . ";");
                        $condition[$c['kolom']] = $value;
                    } */
                    
                }
            }

            if (!empty($table)) {
                $table['tgl_dibuat'] = Time::now();
                $table['dibuat_oleh'] = 'Sinc System';
                $table['status_id'] = $this->statusId;
                $table['log_id'] = $this->logId;

                //validasi jika data sudah pernah ada sesuai kolom kunci maka akan update
                if (!empty($condition)) {
                    $table = $this->cekData($condition, $activeTable, $table);
                }

                try {
                    if ($tables->save($table)) {
                        $status = true;
                        if (!empty($condition)) {
                            $this->statusId = DataSincService::setStatusOSS(
                                $this->statusId,
                                $status,
                                [
                                    'status' => 1,
                                    'uraian' => 'Update data '.$activeTable.' berhasil'
                                ]
                            );
                        }

                        $this->statusId = $table['status_id'];
                    } else {
                        $status = true;
                        $this->statusId = DataSincService::setStatusOSS(
                                $this->statusId,
                                $status,
                                [
                                    'status' => 2,
                                    'uraian' => 'Terjadi masalah saat menyimpan data '.$activeTable
                                ]
                        );
                    }

                    $this->logId = DataSincService::setLog($this->logId,
                        ['proses' => 'Simpan data ' . $activeTable,
                            'status' => $status,
                            'data' => $table,
                            'keterangan' => $table->getErrors()
                        ]
                    );
                } catch (\Exception $e) {
                    $status = false;
                    $this->statusId = DataSincService::setStatusOSS(
                            $this->statusId,
                            $status,
                            [
                                'status' => 2,
                                'uraian' => 'Terjadi masalah saat menyimpan data :'.$activeTable
                            ]
                    );

                    $this->logId = DataSincService::setLog(
                        $this->logId,
                        ['proses' => 'Simpan data ' . $activeTable,
                            'status' => $status,
                            'data' => $table,
                            'keterangan' => $e->getMessage()
                        ]
                    );
                }

                if ($table['id']) {
                    eval('$this->' . $activeTable . '_id = ' . $table['id'] . ';');
                }

                $tableKey = substr($fkey, 0, strlen($fkey) - 3);

                if ($tableKey == $activeTable) {
                    $fkeydata = $table['id'];
                }
            }

            $childs = TableRegistry::get('data_sinc');
            $child = $childs->find();
            $child->select(['id' => 'id']);
            $child->where(['parent_id' => $sincId, 'parent_id is not' => null]);

            foreach ($child as $ch) { //screening ke child layer jika ada
                $this->postData($ch['id'], $Data, $fkey, $fkeydata);
            }
        }
    }

    //Set status respon atas proses yang ada
    private function setStatus($kdIzin = null, $status = null, $uraian = null, $idIzin = null)
    {
        $this->idx = $this->idx + 1;
        $this->keterangan[$this->idx]['kd_izin'] = $kdIzin;
        $this->keterangan[$this->idx]['id_izin'] = $idIzin;
        if ($status) {
            $status = 1;
        } else {
            $status = 2;
        }
        $this->keterangan[$this->idx]['status'] = $status;
        $this->keterangan[$this->idx]['uraian'] = $uraian;
    }

    /*
    Validasi data berdasarkan key yang di tunjuk pada proses maping, jika ada maka akan mengupdate data yang ada.
    */
    private function cekData($condition, $dataTable, $entity)
    {
        $table = TableRegistry::get($dataTable);
        $data = $table->find()->select(['id'])
            ->where([$condition])
            ->first();

        $this->logId = DataSincService::setLog($this->logId,
            ['proses' => 'Cek Data ' . $dataTable,
                'condition' => $condition,
                'origin' => $entity,
                'new' => $data
            ]
        );

        if (!empty($data)) {
            $entity['id'] = $data['id'];
        }

        return $entity;
    }

    private function permohonanExist($nibId, $ossId, $nibData)
    {
        $status = false;
        $permohonanIzinTbl = TableRegistry::get('permohonan_izin');
        $permohonanIzin = $permohonanIzinTbl
            ->find()
            ->select()
            ->where([
                'nib_id' => $nibId,
                'oss_id' => $ossId,
                'kode_oss' => $nibData['kd_izin'],
                'kd_izin' => $nibData['id_izin'],
                'id_proyek' => $nibData['id_proyek'],
                'kd_daerah' => $nibData['kd_daerah']
            ])
            ->first();

        if (!empty($permohonanIzin)) {
            $status = true;
        }

        return $status;
    }

    public function cleanJD() {
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '2G');
        
        // ConnectionManager::config('test2', [
        //     'className' => 'Cake\Database\Connection',
        //     'driver' => 'Cake\Database\Driver\Postgres',
        //     'persistent' => false,
        //     'host' => 'localhost',
        //     /**
        //      * CakePHP will use the default DB port based on the driver selected
        //      * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
        //      * the following line and set the port accordingly
        //      */
        //     //'port' => 'non_standard_port_number',
        //     'username' => 'postgres',
        //     'password' => 'rahasia',
        //     'database' => 'sc060121',
        //     'encoding' => 'utf8',
        //     'timezone' => 'Asia/Jakarta'
        // ]);
        // $con     = ConnectionManager::get('test2');

        $listNamaJD = $con->execute(
            "Select nama 
            from (select lower(kode) as nama, count(*) as jumlah from jenis_dokumen group by kode) jenis_dok
            where jumlah > 1
            order by jumlah desc"
        )->fetchAll('assoc');

        foreach ($listNamaJD as $jenisDoc) {
            $maxIdJD = $con->execute(
                "select * from (select jenis_dokumen_id as id, count(*) as jumlah
                from dokumen_pemohon 
                where jenis_dokumen_id in (Select id
                                from jenis_dokumen
                                where lower(kode) = '{$jenisDoc['nama']}')
                group by jenis_dokumen_id) as dp
                order by jumlah desc
                limit 1"
            )->fetchAll('assoc');

            if(!empty($maxIdJD)){
                $con->execute(
                    "update dokumen_pemohon set jenis_dokumen_id = {$maxIdJD[0]['id']}
                    where jenis_dokumen_id in (Select id
                                    from jenis_dokumen
                                    where lower(kode) = '{$jenisDoc['nama']}')
                    and jenis_dokumen_id <> {$maxIdJD[0]['id']}"
                );

                $maxIdJD = null;
                $maxIdJD = $con->execute(
                    "select * from (select jenis_dokumen_id as id, count(*) as jumlah
                    from persyaratan 
                    where jenis_dokumen_id in (Select id
                                    from jenis_dokumen
                                    where lower(kode) = '{$jenisDoc['nama']}')
                    group by jenis_dokumen_id) as dp
                    order by jumlah desc
                    limit 1"
                )->fetchAll('assoc');
    
                if(!empty($maxIdJD)){
                    $con->execute(
                        "update persyaratan set jenis_dokumen_id = {$maxIdJD[0]['id']}
                        where jenis_dokumen_id in (Select id
                                        from jenis_dokumen
                                        where lower(kode) = '{$jenisDoc['nama']}')
                        and jenis_dokumen_id <> {$maxIdJD[0]['id']}"
                    );
                } 
            }           
        }

        $response = $this->response->withStringBody(json_encode($listNamaJD));
        return $response;
    }

    public function testApi() {
        $this->logId = DataSincService::setLog(
            $this->logId,
            ['proses' => 'Test API 2',
                'status' => 'ok',
                'data' => $this->request->getData(),
                'keterangan' => 'Berhasil'
            ]
        );

        return;
    }

    public function readImg($total = 0) {
        /* echo (new TesseractOCR('BT-00579.pdf'))
        ->run(); */
        /* $rentang = ((strtotime('2021-06-17') - strtotime('2021-06-10'))/3600)/24;
        $rentang = $rentang + 1;
        $kejadian = $rentang - (7 * floor($rentang/7));

        echo $kejadian; */

        $docObj = new FileService(WWW_ROOT . "test.txt");
        //$docObj = new DocxConversion("test.docx");
        //$docObj = new DocxConversion("test.xlsx");
        //$docObj = new DocxConversion("test.pptx");
        $docText= $docObj->convertToText();
        echo $docText;


    }
}
