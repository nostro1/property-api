<?php

namespace App\Controller\Rest;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Cake\I18n\Time;
use App\Service\AlurProsesService;
use App\Service\DataSincService;

class SinkronisasiController extends RestController
{
    public function initialize()
    {
        parent::initialize();
        $this->autoRender = false;
        $this->status = true;
        $this->keterangan = [];
        $this->idx = -1;
        $this->log_id = null;
        $this->log_keterangan = null;
        $this->withReturn = true;
        $this->statusId = null;
        $this->logId = null;

        $this->Auth->allow(['postNIB', 'postData', 'sendStatus', 'sendStatusFinal', 'getToken']);
    }

    /**
     * Penampungan ceklist
     * Terbentuk baik di proses internal ataupun lempar keluar
     */
    public function setToChecklist($nibData = null, $instansiId = null, $jenisIzinId = null)
    {
        $clTbl = TableRegistry::get('oss_checklist');
        $clEnt = $clTbl->newEntity();

        $clEnt['nib_id'] = $this->nib_id;
        $clEnt['oss_id'] = $this->oss_id;
        $clEnt['kd_izin'] = $nibData['kd_izin'];
        //$clEnt['kd_dokumen'] = $nibData['kd_dokumen'];
        $clEnt['nama_izin'] = $nibData['nama_izin'];
        $clEnt['instansi'] = $nibData['instansi'];
        $clEnt['flag_checklist'] = $nibData['flag_checklist'];
        $clEnt['instansi_id'] = $instansiId;
        $clEnt['jenis_izin_id'] = $jenisIzinId;

        $clTbl->save($clEnt);
        if (is_null($jenisIzinId)) {
            $this->forward = true;
        }
    }

    /**
     * This will be called via eval from postData.
     * @param null $nibData
     */
    private function permohonan($nibData = null)
    {
        $instansiId = null;
        $permohonan = null;
        $jenisIzinId = null;

        if ($nibData['flag_checklist'] != 'T') {
            $tmp = substr($nibData['kd_daerah'], 0, 4);

            if (substr($tmp, 2, 2) != "00") {
                $kab = substr($tmp, 0, 2) . "." . substr($tmp, 2, 2);
            } else {
                $kab = substr($tmp, 0, 2);
            }

            $jenis_izin = substr($nibData['kd_izin'], 9, 3);
            $InstansiTbl = TableRegistry::get('unit');
            $InstansiData = $InstansiTbl->find()->select();

            if (substr($nibData['kd_daerah'], 0, 4) == '00000') {
                $InstansiData->where(['instansi_code' => substr($nibData['kd_izin'], 0, 3), 'tipe' => 'I']);
            } else {
                $InstansiData->where(['kode_daerah' => $kab, 'tipe' => 'I']);
            }

            $instansiId = $InstansiData->first()['id'];

            if (empty($instansiId)) {
                $tblLog = TableRegistry::get('oss_log');
                $log = $tblLog->newEntity();
                $this->setOSSLog($this->nib_id, 'Permohonan', 'Kode : ' . $kab . ' Instansi Id tidak di kenali');
                $this->status = false;
            }

            $jiTbl = TableRegistry::get('jenis_izin');
            $jiData = $jiTbl->find()->select();
            $jiData->where(['instansi_id' => $instansiId, 'kode_oss' => $jenis_izin]);
            $tipePemohon = empty($this->perusahaan_id) ? 'PERORANGAN' : 'PERUSAHAAN';

            if ($jiData->count() > 0) {
                foreach ($jiData as $ji) {
                    $jenisIzinId = $ji['id'];
                    $dataPermohonan = [
                        'pemohon_id' => $this->pemohon_id,
                        'perusahaan_id' => $this->perusahaan_id,
                        'jenis_izin_id' => $ji['id'],
                        'tipe_pemohon' => $tipePemohon,
                        'jenis_permohonan' => 'Baru',
                        'instansi_id' => $instansiId,
                        'nib_id' => $this->nib_id,
                        'oss_id' => $this->oss_id,
                        'kode_oss' => $nibData['kd_izin'],
                        'kd_izin' => $nibData['id_izin'],
                        'is_active' => FALSE,
                        'id_proyek' => $nibData['id_proyek'],
                        'kd_daerah' => $nibData['kd_daerah'],
                        'dibuat_oleh' => 'OSS010',
                        'tgl_dibuat' => date('Y/m/d')
                    ];

                    $permohonan = AlurProsesService::registerPermohonanIzin($dataPermohonan);

                    if ($permohonan) {
                        $this->setOSSLog($this->nib_id, 'Permohonan', 'Permohonan dengan jenis izin ' . $nibData['kd_izin'] . ' Terbentuk di instansi ' . $instansiId . '|' . $this->nib_id);
                        $this->setStatus(
                            $nibData['kd_izin'],
                            true,
                            'Permohonan dengan jenis izin ' . $nibData['id_izin'] . ' Terbentuk di daerah kode ' . $kab . '|' . $this->nib_id, $nibData['id_izin']
                        );
                    } else {
                        $this->setOSSLog($this->nib_id, 'Error Permohonan', json_encode(AlurProsesService::getErrors()));
                        $this->status = false;
                        $this->setStatus($nibData['kd_izin'], false, json_encode(AlurProsesService::getErrors()), $nibData['id_izin']);
                    }
                }
            } else {
                $this->setOSSLog($this->nib_id, 'Permohonan Tidak terbentuk', 'Jenis Izin : ' . $nibData['kd_izin'] . ' Belum Terdaftar di daerah ' . $kab);
                $this->status = false;
                $this->setStatus(
                    $nibData['kd_izin'],
                    $this->status,
                    'Jenis Izin : ' . $nibData['kd_izin'] . ' Belum Terdaftar di ' . $kab, $nibData['id_izin']
                );
                $notExist = true;
            }
        }
    }

    public function postNIB($sinc_id = null)
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '1G');

        $this->source_url = null;
        $this->nib_id = null;
        $this->withReturn = false;
        $this->response = $this->response->withType('json');

        $data = $this->request->getData();
        $this->nib_data = $data;
        $this->setOSSLog($data['dataNIB']['nib'], 'Receive NIB', json_encode($this->request->getData()));

        $nibTbl = TableRegistry::get('data_nib');
        $nibData = $nibTbl->find()->select();
        $nibData->where(['nib' => $data['dataNIB']['nib'], 'tipe_dokumen' => $data['dataNIB']['tipe_dokumen']]);

        if ($nibData->count() > 0) {
            $response = $this->response->withStringBody(json_encode(['responreceiveNIB' => [
                'status' => 2,
                'keterangan' => 'NIB Sudah diproses'
            ]]));
            return $response;
        } else {
            //cek tipe dokumennya
            //jika baru langsung proses
            //jika penolakan lsg penetapan drop
            /*
            (9:Original, 5:Update, 3:Pencabutan, 4:Pembatalan)
            */
            if ($data['dataNIB']['tipe_dokumen'] == 3 || $data['dataNIB']['tipe_dokumen'] == 4) {
                $permohonanIzinTbl = TableRegistry::get('permohonan_izin');
                $permohonanIzinTbl->updateAll(
                    ['status_penetapan' => 'ditolak', 'catatan_rekomendasi' => 'perubahan status dari oss'],
                    ['nib' => $data['dataNIB']['nib']]
                );
            } else {
                $this->postData($sinc_id);
            }
        }

        $this->status = $this->status ? 1 : 2;

        $response = $this->response->withStringBody(
            json_encode([
                'responreceiveNIB' => [
                    'status' => $this->status,
                    'keterangan' => $this->keterangan
                ]
            ])
        );
        return $response;
    }

    public function postData($sinc_id = null, $fdata = null, $fkey = null, $fkeydata = null)
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '1G');

        $key = null;
        $recData = null;
        $proses = null;
        $mainPost = false;
        $exist = 0;

        //screening berdasarkan pola data perlayer
        if (empty($fdata)) { //inisialisasi awal
            $data = $this->request->getData();
        } else { //data turunan
            $data = $fdata;
        }

        $sinchron = TableRegistry::get('data_sinc');
        $sinc = $sinchron->find()->select(['index' => 'index', 'key' => 'key', 'proses' => 'proses']);
        $sinc->where(['id' => $sinc_id]);

        if (!empty($sinc)) {
            foreach ($sinc as $s) {
                if (array_key_exists($s['index'], $data)) {
                    $recData = $data[$s['index']];
                    $proses = $s['proses'];
                    $key = !empty($fkey) ? $fkey : $s['key'];
                }
            }

            if (!empty($recData)) {
                foreach ($recData as $idx => $val) { //validasi array atau objek
                    $index = $idx;
                    break;
                }

                if (is_string($index)) { //single row
                    if (!empty($proses)) {
                        //jalankan proses
                        call_user_func(array($this, $proses), $recData[$index]);
                    }
                    $this->saveData($sinc_id, $recData, $key, $fkeydata);
                } else { //multi row
                    for ($i = 0; $i < count($recData); $i++) {
                        if (!empty($proses)) {//proses fungsi
                            call_user_func(array($this, $proses), $recData[$i]);
                        }
                        $this->saveData($sinc_id, $recData[$i], $key, $fkeydata);
                    }
                }
            } else {
                $this->statusId = DataSincService::setStatusOSS(
                    $this->statusId,
                    false,
                    [
                        'status' => 2,
                        'keterangan' => 'Data diterima kosong'
                    ]
                );
            }

        } else {
            $this->statusId = DataSincService::setStatusOSS(
                $this->statusId,
                false,
                [
                    'status' => 2,
                    'keterangan' => 'Data tidak dapat di sinkronisasikan silahkan periksa setingan anda'
                ]
            );
        }

        if ($this->withReturn) {
            $proses = $this->request->getQuery('proses');

            if (!is_null($this->statusId)) {
                $procRespon = DataSincService::getResponse($this->statusId);
            } else {
                $procRespon['status'] = 1;
                $procRespon['keterangan'] = $proses . ' Berhasil dijalankan';
                $procRespon = json_encode($procRespon);
            }

            if (!is_null($proses)) {
                $respons["respon" . $proses] = json_decode($procRespon);
            } else {
                $respons = json_decode($procRespon);
            }

            $response = $this->response->withStringBody(json_encode($respons));
            return $response;
        }
    }

    public function saveData($sinc_id = null, $Data = null, $fkey = null, $fkeydata = null)
    {
        $keydata = null;

        if (!empty($Data)) {
            $dataSincDetailTable = TableRegistry::get('data_sinc_detail');

            $details = $dataSincDetailTable->find()
                ->select([
                    'tabel' => 'c.nama_datatabel',
                    'kolom' => 'b.data_kolom',
                    'rec_kolom' => 'data_sinc_detail.oss_kolom'
                ])
                ->innerJoin(['b' => 'data_kolom'], ['b.id = data_sinc_detail.data_kolom_id'])
                ->innerJoin(['c' => 'datatabel'], ['c.id = b.datatabel_id'])
                ->innerJoin(['d' => 'data_sinc'], ['d.id=data_sinc_detail.data_sinc_id'])
                ->where(['data_sinc_id' => $sinc_id, 'data_sinc_detail.del' => 0])
                ->order(['c.nama_datatabel']);

            $tableName = null;
            $entityData = [];
            $entityTable = null;
            $entity = null;

            foreach ($details as $c) { //screening maping per layer API
                $id = null;
                $keterangan = null;

                if ($tableName != $c['tabel']) {
                    $tableName = $c['tabel'];
                    $entityTable = TableRegistry::get($tableName);
                    $entity = !empty($id) ? $entityTable->get($id) : $entityTable->newEntity();

                    //Khusus table nib agar tidak di simpan ulang
                    if ($tableName == 'nib' || $tableName == 'pemohon' || $tableName == 'perusahaan') {
                        $nib = $entityTable->find()->select(['id']);

                        if ($tableName == 'nib') {
                            $nib->where(['nib' => $Data['nib']]);
                        } else {
                            $nib->where(['nib_id' => $this->nib_id]);
                        }

                        if ($nib->first()) {
                            $this->nib_id = $nib->id;
                        }
                    }

                    if (!empty($fkeydata)) {
                        $entityData[$fkey] = $fkeydata;
                    }

                    if ($entity instanceof Entity) {
                        if (in_array($tableName, ['pemohon', 'perusahaan'])) {
                            $entityData['nib_id'] = $this->nib_id;
                        }

                        $this->setOSSLog($this->nib_id, 'Simpan data', 'Tabel : ' . $tableName . ' data : ' . json_encode($entityData));

                        //status sama log generate dulu untuk dapatkan idnya
                        $entityData['status_id'] = $this->statusId;
                        $entityData['log_id'] = $this->logId;

                        $entity = $entityTable->patchEntity($entity, $entityData);

                        if ($entityTable->save($entity)) {
                            $status = true;
                        } else {
                            $status = false;
                            $keterangan = $entity->getErrors();
                        }

                        if (is_null($this->statusId)) {
                            $this->statusId = $entityData['statusId'];
                        }

                        $this->logId = DataSincService::setLog(
                            $this->logId,
                            ['proses' => 'Simpan data ' . $tableName,
                                'status' => $status,
                                'data' => $entityData,
                                'keterangan' => $keterangan
                            ]
                        );

                        $tableKey = substr($fkey, 0, strlen($fkey) - 3);

                        if ($tableKey == $tableName) {
                            $fkeydata = $entityData['id'];
                        }

                        switch ($tableName) {
                            case 'perusahaan':
                                $this->perusahaan_id = $entityData['id'];
                                break;
                            case 'pemohon':
                                $this->pemohon_id = $entityData['id'];
                                break;
                            case 'data_nib':
                                $this->nib_id = $entityData['nib'];
                                $this->oss_id = $entityData['oss_id'];
                                break;
                        }
                    }

                }

                /**parsing jenis dokumen */
                if (substr($c['rec_kolom'], 0, 8) == 'jenis_id') {
                    $Data[$c['rec_kolom']] = $Data[$c['rec_kolom']] == '01' ? 'KTP' : 'Paspor';
                }

                //Maping kolom
                if (array_key_exists($c['rec_kolom'], $Data)) {
                    $entityData[$c['kolom']] = $Data[$c['rec_kolom']];
                }
            }

            if (!empty($entityData) && $entity instanceof Entity) {
                if (in_array($tableName, ['pemohon', 'perusahaan'])) {
                    $entityData['nib_id'] = $this->nib_id;
                }

                $this->setOSSLog('', 'Simpan data', 'Tabel : ' . $tableName . ' data : ' . json_encode($entityData));

                // status sama log generate dulu untuk dapatkan idnya
                $entityData['status_id'] = $this->statusId;
                $entityData['log_id'] = $this->logId;

                $entity = $entityTable->patchEntity($entity, $entityData);

                if ($entityTable->save($entity)) {
                    $status = true;
                } else {
                    $status = false;
                    $keterangan = $entity->getErrors();
                }

                if (is_null($this->statusId)) {
                    $this->statusId = $entityData['statusId'];
                }

                $this->logId = DataSincService::setLog(
                    $this->logId,
                    [
                        'proses' => 'Simpan data ' . $tableName,
                        'status' => $status,
                        'data' => $entityData,
                        'keterangan' => $keterangan
                    ]
                );

                $tableKey = substr($fkey, 0, strlen($fkey) - 3);

                if ($tableKey == $tableName) {
                    $fkeydata = $entityData['id'];
                }

                // Set perusahaan_id dan pemohon_id
                switch ($tableName) {
                    case 'perusahaan':
                        $this->perusahaan_id = $entityData['id'];
                        break;
                    case 'pemohon':
                        $this->pemohon_id = $entityData['id'];
                        break;
                }
            }

            $dataSincTable = TableRegistry::get('data_sinc');
            $dataSincRecords = $dataSincTable->find();
            $dataSincRecords->select(['id' => 'id']);
            $dataSincRecords->where(['parent_id' => $sinc_id, 'parent_id is not' => null]);

            foreach ($dataSincRecords as $ch) { //screening ke child layer jika ada
                $this->postData($ch['id'], $Data, $fkey, $fkeydata);
            }
        }
    }

    //Set status respon atas proses yang ada
    private function setStatus($kdIzin = null, $status = null, $uraian = null, $idIzin = null)
    {
        $this->idx = $this->idx + 1;
        $status = $status ? 1 : 2;

        $this->keterangan[$this->idx]['kd_izin'] = $kdIzin;
        $this->keterangan[$this->idx]['id_izin'] = $idIzin;
        $this->keterangan[$this->idx]['status'] = $status;
        $this->keterangan[$this->idx]['uraian'] = $uraian;
    }

    public function forwardCL($data = null)
    {
        $cl = [];

        $clTbl = TableRegistry::get('oss_checklist');
        $clData = $clTbl->find()->select([
            'id' => 'oss_checklist.id',
            'instansi_id' => 'oss_checklist.instansi_id',
            'kd_izin' => 'oss_checklist.kd_izin',
            'kd_dokumen' => 'oss_checklist.kd_dokumen',
            'nama_izin' => 'oss_checklist.nama_izin',
            'instansi' => 'oss_checklist.instansi',
            'flag_checklist' => 'oss_checklist.flag_checklist',
            'ws_url' => 'b.ws_url'
        ]);

        $clData->innerJoin(['b' => 'unit'], ['b.id = oss_checklist.instansi_id']);
        $clData->where(['nib_id' => $this->nib_id, 'jenis_izin_id is ' => null, 'oss_checklist.status_send is ' => null]);
        $clData->order(['instansi_id']);

        foreach ($clData as $cd) {
            if (!array_key_exists($cd['instansi_id'], $cl)) {
                $cl[$cd['instansi_id']] = array();
            }
            $idx = count($cl[$cd['instansi_id']]);

            $cl[$cd['instansi_id']][$idx]['instansi_id'] = $cd['instansi_id'];
            $cl[$cd['instansi_id']][$idx]['kd_izin'] = $cd['kd_izin'];
            $cl[$cd['instansi_id']][$idx]['kd_dokumen'] = $cd['kd_dokumen'];
            $cl[$cd['instansi_id']][$idx]['nama_izin'] = $cd['nama_izin'];
            $cl[$cd['instansi_id']][$idx]['instansi'] = $cd['instansi'];
            $cl[$cd['instansi_id']][$idx]['flag_checklist'] = $cd['flag_checklist'];
            $cl[$cd['instansi_id']][$idx]['ws_url'] = $cd['ws_url'];

            $clUpdate = $clTbl->get($cd['id']);
            $clUpdate['status_send'] = 'true';
            $clTbl->save($clUpdate);
        }

        foreach ($cl as $cl_instansi) {
            if (!empty($cl_instansi[0]['ws_url'])) {
                $this->nib_data['dataNIB']['data_checklist'] = $cl_instansi;
                $this->setOSSLog($this->nib_data['dataNIB']['nib'], 'Inisial Forwarding', json_encode($this->nib_data));

                $http = new Client();
                $response = $http->post(
                    $cl_instansi[0]['ws_url'],
                    $this->nib_data,
                    [
                        'type' => 'application/json',
                        'headers' => ['OSS-API-KEY' => ''],
                        'auth' => ['username' => '', 'password' => ''],
                        'ssl_verify_peer' => false,
                        'ssl_verify_peer_name' => false
                    ]
                );

                if ($response->code != 200) {
                    $this->status = false;
                    $this->setStatus($cl_instansi[0]['kd_izin'], false, $response->code . "|" . $response->withStringBody);
                    $this->setOSSLog($this->nib_data['dataNIB']['nib'], 'ERROR Forwarding', $response->code . "|" . $response->withStringBody);
                } else {
                    $data = $response->withStringBody;
                    $kdIzin = null;
                    $status = null;
                    $keterangan = null;

                    if (property_exists($data, 'keterangan')) {
                        foreach ($data->keterangan as $d) {
                            if (property_exists($d, 'kd_izin')) {
                                $kdIzin = $d->kd_izin;
                            }

                            if (property_exists($d, 'status')) {
                                if ($d->status = 1) {
                                    $status = true;
                                } else {
                                    $status = false;
                                }
                            }

                            if (property_exists($d, 'uraian')) {
                                $keterangan = $d->uraian;
                            }

                            $this->setStatus($kdIzin, $status, $keterangan);
                        }
                    }

                    $this->setOSSLog($this->nib_data['dataNIB']['nib'], 'Forwarding OK', $response->withStringBody);
                }
            } else {
                $this->setStatus($cl_instansi[0]['instansi_id'], false, "tidak terhubung ke " . $cl_instansi[0]['kd_izin']);
                $this->setOSSLog($this->nib_data['dataNIB']['nib'], 'Forwarding Error', "Tidak ada url terdaftar");
            }
        }
    }

    public function sendStatus()
    {
        $data = $this->request->getData();
        $data = [
            'IZINSTATUS' => [
                'nib' => $data['nib_id'],
                'oss_id' => $data['oss_id'],
                'kd_izin' => $data['kd_izin'],
                'kd_instansi' => $data['kd_instansi'],
                'kd_status' => $data['kd_status'],
                'tgl_status' => $data['tgl_status'],
                'nip_status' => $data['nip_status'],
                'nama_status' => $data['nama_status'],
                'keterangan' => $data['keterangan'],
                'status_izin' => $data['status_izin']
            ]
        ];

        $this->setOSSLog($data['nib_id'], 'Forward Status', $data);
        $token = $this->getToken();

        $http = new Client();
        $resp = $http->post(
            $this->source_url . '/receiveLicenseStatus',
            json_encode($data),
            [
                'type' => 'json',
                'headers' => ['OSS-API-KEY' => $token],
                'auth' => ['username' => '', 'password' => '']
            ]
        );

        if ($resp->code != 200) {
            $this->setOSSLog($data['nib_id'], 'Forward Status Error', $resp->body);
        } else {
            $this->setOSSLog($data['nib_id'], 'Forward Status OK', $resp->body);
        }

        $response = $this->response->withType('json');
        $response = $response->withStatus($resp->code);
        $response->withStringBody($response->withStringBody);

        return $response;
    }

    public function sendStatusFinal()
    {
        $data = $this->request->getData();
        $data = [
            'IZINFINAL' => [
                'nib' => $data['nib_id'],
                'oss_id' => $data['oss_id'],
                'kd_izin' => $data['kd_izin'],
                'nomor_izin' => $data['nomor_izin'],
                'tgl_terbit_izin' => $data['tgl_terbit_izin'],
                'tgl_berlaku_izin' => $data['tgl_berlaku_izin'],
                'nama_ttd' => $data['nama_ttd'],
                'nip_ttd' => $data['nip_ttd'],
                'jabatan_ttd' => $data['jabatan_ttd'],
                'status_izin' => $data['status_izin'],
                'file_izin' => $data['file_izin'],
                'data_pnbp' => [
                    'kd_akun' => $data['data_pnbp']['kd_akun'],
                    'kd_penerimaan' => $data['data_pnbp']['kd_penerimaan'],
                    'nominal' => $data['data_pnbp']['nominal']
                ]
            ]
        ];

        $this->setOSSLog($data['nib_id'], 'Forward Status Final', $data);
        $token = $this->getToken();

        $http = new Client();
        $response = $http->post(
            $this->source_url . '/receiveLicense',
            json_encode($data),
            [
                'type' => 'json',
                'headers' => ['OSS-API-KEY' => $token],
                'auth' => ['username' => '', 'password' => '']
            ]
        );

        if ($response->code != 200) {
            $this->setOSSLog($data['nib_id'], 'Forward Status Final>error', $response->withStringBody);
        } else {
            $this->setOSSLog($data['nib_id'], 'Forward Status Final>ok', $response->withStringBody);
        }
    }

    public function getToken()
    {
        $data = null;/* array('securityKey' => array('user_akses' => 'OSS010', 'pwd_akses' => '68c52ed46b02d35863b15c96c7486412')); */
        $token = null;

        $sincs = TableRegistry::get('data_sinc');
        $sinc = $sincs->find()->select();
        $sinc->where(['masa_berlaku >=' => Time::now(), 'id' => 1]);

        foreach ($sinc as $s) {
            $token = $s['aktif_token'];
            $this->source_url = $s['source_url'];
        }

        if (empty($token)) {
            $data = null;
            $sinc_login = TableRegistry::get('data_sinc');
            $login = $sincs->find()->select();
            $login->where(['id' => 1]);

            foreach ($login as $l) {
                $data = array('securityKey' => array('user_akses' => $l['user_akses'], 'pwd_akses' => $l['pwd_akses']));
                $this->source_url = $l['pwd_akses'];
            }

            if (!empty($data)) {
                $http = new Client();
                $response = $http->post($this->source_url . '/sendSecurityKey', json_encode($data), ['type' => 'json',
                    'auth' => ['username' => '', 'password' => '']
                ]);
                if ($response->code != 200) {
                    $tblLog = TableRegistry::get('oss_log');
                    $log = $tblLog->newEntity();
                    $log['log'] = $response->withStringBody;
                    $tblLog->save($log);
                    $this->setOSSLog(null, 'Send Security>Error', $response->withStringBody);
                } else {
                    $tblLog = TableRegistry::get('data_sinc');
                    $log = $tblLog->get(1);
                    $log['aktif_token'] = $response->json['responsendSecurityKey']['key'];
                    $log['masa_berlaku'] = $response->json['responsendSecurityKey']['masa_berlaku'];
                    $tblLog->save($log);
                    $token = $response->json['responsendSecurityKey']['key'];
                }
            }
        }

        return $token;
    }

    private function setOSSLog($nib_id = null, $status = null, $keterangan = null)
    {
        $logTable = TableRegistry::get('oss_log');

        if (is_null($this->log_id)) {
            $logEntity = $logTable->newEntity();

            $this->log_keterangan = [[$status => $keterangan]];
            $logEntity->nib = $nib_id;
            $logEntity->status = $status;
            $logEntity->log = json_encode($this->log_keterangan);
        } else {
            $logEntity = $logTable->get($this->log_id);
            array_push($this->log_keterangan, [$status => $keterangan]);
            $logEntity->log = json_encode($this->log_keterangan);
        }

        if (!$logTable->save($logEntity)) {
            return $this->setResponseData([], false, "Failed to save OSS Log");
        }

        if (is_null($this->log_id)) {
            // inisial penerimaan sudah terjadi?
            $cekLog = $logTable->find()->select();
            $cekLog->where(['nib' => $nib_id, 'status' => 'Receive NIB']);

            if ($cekLog->count() > 0 && $status != 'Receive NIB') {
                $this->log_id = $logEntity->id;
            }
        }
    }
}
