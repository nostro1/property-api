<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AssessmentParticipantResult Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $assessment_participant_id
 * @property int|null $assessment_question_id
 * @property string|null $answer
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\AssessmentParticipant $assessment_participant
 * @property \App\Model\Entity\AssessmentQuestion $assessment_question
 */
class AssessmentParticipantResult extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'assessment_participant_id' => true,
        'assessment_question_id' => true,
        'answer' => true,
        'instansi' => true,
        'assessment_participant' => true,
        'assessment_question' => true
    ];
}
