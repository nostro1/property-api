<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Billing Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int $person_id
 * @property int|null $periode
 * @property string|null $status
 * @property string|null $total
 * @property \Cake\I18n\FrozenDate|null $due_date
 * @property \Cake\I18n\FrozenDate|null $payment_date
 * @property int|null $paid
 * @property int|null $tax
 *
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\BillingDetail[] $billing_detail
 */
class Billing extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'person_id' => true,
        'periode' => true,
        'status' => true,
        'total' => true,
        'due_date' => true,
        'payment_date' => true,
        'paid' => true,
        'tax' => true,
        'unit' => true,
        'person' => true,
        'billing_detail' => true
    ];
}
