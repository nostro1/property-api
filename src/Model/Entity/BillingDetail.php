<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BillingDetail Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int $billing_id
 * @property string|null $billing_item
 * @property int|null $qty
 * @property int|null $amount
 *
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Billing $billing
 */
class BillingDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'billing_id' => true,
        'billing_item' => true,
        'qty' => true,
        'unit' => true,
        'amount' => true,
        'billing' => true
    ];
}
