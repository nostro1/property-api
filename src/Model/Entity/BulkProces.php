<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BulkProces Entity
 *
 * @property int $id
 * @property int $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $process_type
 * @property string|null $status
 * @property int|null $number_of_Thread
 * @property int|null $total_process
 * @property int|null $running_process
 *
 * @property \App\Model\Entity\Instansi $instansi
 */
class BulkProces extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'process_type' => true,
        'status' => true,
        'number_of_Thread' => true,
        'total_process' => true,
        'running_process' => true,
        'instansi' => true
    ];
}
