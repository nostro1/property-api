<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CPegawaiDetail Entity
 *
 * @property int $id
 * @property int $instansi_id
 * @property string|null $data_labels
 * @property int $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenDate|null $tgl_dibuat
 * @property string|null $diubah_oleh
 * @property \Cake\I18n\FrozenDate|null $tgl_diubah
 * @property int|null $pegawai_id
 * @property string|null $spesimen_tanda_tangan
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Pegawai $pegawai
 */
class CPegawaiDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'instansi_id' => true,
        'data_labels' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'diubah_oleh' => true,
        'tgl_diubah' => true,
        'pegawai_id' => true,
        'spesimen_tanda_tangan' => true,
        'instansi' => true,
        'pegawai' => true
    ];
}
