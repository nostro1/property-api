<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Content Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string $title
 * @property int|null $content_category_id
 * @property string|null $status
 * @property \Cake\I18n\FrozenDate|null $publish_begin
 * @property \Cake\I18n\FrozenDate|null $publish_end
 * @property string|null $contents
 * @property string|null $short_desc
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\ContentCategory $content_category
 * @property \App\Model\Entity\ContentDetail[] $content_detail
 */
class Content extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'title' => true,
        'content_category_id' => true,
        'status' => true,
        'publish_begin' => true,
        'publish_end' => true,
        'contents' => true,
        'short_desc' => true,
        'instansi' => true,
        'img_cover' => true,
        'content_detail' => true
    ];
}
