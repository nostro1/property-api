<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContentComment Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $content_id
 * @property int|null $pengguna_id
 * @property string|null $comment
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Content $content
 * @property \App\Model\Entity\Pengguna $pengguna
 */
class ContentComment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'content_id' => true,
        'pengguna_id' => true,
        'comment' => true,
        'instansi' => true,
        'content' => true,
        'pengguna' => true
    ];
}
