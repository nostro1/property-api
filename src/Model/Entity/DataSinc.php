<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DataSinc Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string $key
 * @property string|null $keterangan
 * @property int|null $parent_id
 * @property string $index
 * @property string|null $data_labels
 * @property string|null $aktif_token
 * @property string|null $user_akses
 * @property string|null $pwd_akses
 * @property string|null $proses
 * @property int|null $del
 * @property \Cake\I18n\FrozenTime|null $masa_berlaku
 * @property string|null $source_url
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\ParentDataSinc $parent_data_sinc
 * @property \App\Model\Entity\ChildDataSinc[] $child_data_sinc
 * @property \App\Model\Entity\DataSincDetail[] $data_sinc_detail
 * @property \App\Model\Entity\DataSincLog[] $data_sinc_log
 */
class DataSinc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'key' => true,
        'keterangan' => true,
        'parent_id' => true,
        'index' => true,
        'data_labels' => true,
        'aktif_token' => true,
        'user_akses' => true,
        'pwd_akses' => true,
        'proses' => true,
        'del' => true,
        'masa_berlaku' => true,
        'source_url' => true,
        'instansi' => true,
        'parent_data_sinc' => true,
        'child_data_sinc' => true,
        'data_sinc_detail' => true,
        'data_sinc_log' => true
    ];
}
