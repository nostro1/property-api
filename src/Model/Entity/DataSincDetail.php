<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DataSincDetail Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $data_sinc_id
 * @property int $data_kolom_id
 * @property string|null $oss_type_kolom
 * @property string|null $oss_kolom
 * @property string|null $data_labels
 * @property int|null $del
 * @property string|null $is_key
 * @property string|null $value_type
 * @property string|null $maping
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\DataSinc $data_sinc
 * @property \App\Model\Entity\DataKolom $data_kolom
 */
class DataSincDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'data_sinc_id' => true,
        'data_kolom_id' => true,
        'oss_type_kolom' => true,
        'oss_kolom' => true,
        'data_labels' => true,
        'del' => true,
        'is_key' => true,
        'value_type' => true,
        'maping' => true,
        'instansi' => true,
        'data_sinc' => true,
        'data_kolom' => true
    ];
}
