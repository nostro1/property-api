<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentCategory Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string $category_code
 * @property int|null $lft_id
 * @property int|null $rght_id
 * @property int|null $parent_id
 * @property string $category_label
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Lft $lft
 * @property \App\Model\Entity\Rght $rght
 * @property \App\Model\Entity\ParentDocumentCategory $parent_document_category
 * @property \App\Model\Entity\ChildDocumentCategory[] $child_document_category
 * @property \App\Model\Entity\DocumentCategoryRole[] $document_category_role
 */
class DocumentCategory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'category_code' => true,
        'lft_id' => true,
        'rght_id' => true,
        'parent_id' => true,
        'category_label' => true,
        'instansi' => true,
        'lft' => true,
        'rght' => true,
        'parent_document_category' => true,
        'child_document_category' => true,
        'document_category_role' => true
    ];
}
