<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentCategoryRole Entity
 *
 * @property int $id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int $document_category_id
 * @property int $unit_id
 * @property int|null $jabatan_id
 *
 * @property \App\Model\Entity\DocumentCategory $document_category
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Jabatan $jabatan
 */
class DocumentCategoryRole extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'document_category_id' => true,
        'unit_id' => true,
        'jabatan_id' => true,
        'document_category' => true,
        'unit' => true,
        'jabatan' => true
    ];
}
