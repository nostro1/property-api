<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DokumenRepositori Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string $jenis_dokumen
 * @property string $kategori_dokumen
 * @property string|null $judul
 * @property string|null $ringkasan
 * @property string|null $dokumen_lokasi
 * @property string|null $kode_dokumen
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\DokumenAkse[] $dokumen_akses
 */
class DocumentRepository extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'tgl_dibuat' => false,
        'diubat_oleh' => false,
        'tgl_diubah' => false,
        'diubah_oleh' => false
    ];
}
