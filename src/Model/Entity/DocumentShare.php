<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentShare Entity
 *
 * @property int $id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int $dokumen_repositori_id
 * @property int|null $pengguna_id
 * @property bool $is_public
 *
 * @property \App\Model\Entity\DokumenRepositori $dokumen_repositori
 * @property \App\Model\Entity\Pengguna $pengguna
 */
class DocumentShare extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'document_repository_id' => true,
        'pengguna_id' => true,
        'is_public' => true,
        'document_repository' => true,
        'pengguna' => true
    ];
}
