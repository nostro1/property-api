<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentSloc Entity
 *
 * @property int $id
 * @property int $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int $document_repository_id
 * @property string|null $tipe_pekerjaan
 * @property string|null $unit_kerja
 * @property string|null $no_box_lama
 * @property string|null $no_box
 * @property string|null $klasifikasi_arsip
 * @property string|null $document_Category
 * @property string|null $kode_arsip
 * @property string|null $keterangan
 * @property string|null $tahun_dari
 * @property string|null $tahun_sampai
 * @property string|null $media
 * @property string|null $tingkat_keaslian
 * @property int|null $jumlah
 * @property string|null $satuan
 * @property string|null $kondisi
 * @property string|null $foto_sebelum
 * @property string|null $foto_sesudah
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\DocumentRepository $document_repository
 */
class DocumentSloc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'document_repository_id' => true,
        'tipe_pekerjaan' => true,
        'unit_kerja' => true,
        'no_box_lama' => true,
        'no_box' => true,
        'klasifikasi_arsip' => true,
        'document_Category' => true,
        'kode_arsip' => true,
        'keterangan' => true,
        'tahun_dari' => true,
        'tahun_sampai' => true,
        'media' => true,
        'tingkat_keaslian' => true,
        'jumlah' => true,
        'satuan' => true,
        'kondisi' => true,
        'foto_sebelum' => true,
        'foto_sesudah' => true,
        'instansi' => true,
        'document_repository' => true
    ];
}
