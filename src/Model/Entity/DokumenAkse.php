<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DokumenAkse Entity
 *
 * @property int $id
 * @property int|null $dokumen_repositori_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $pengguna_id
 *
 * @property \App\Model\Entity\DokumenRepositori $dokumen_repositori
 * @property \App\Model\Entity\Pengguna $pengguna
 */
class DokumenAkse extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dokumen_repositori_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'pengguna_id' => true,
        'document_repository' => true,
        'pengguna' => true
    ];
}
