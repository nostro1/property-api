<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mail Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $mail_category
 * @property string|null $mail_type
 * @property string|null $mail_subject
 * @property string|null $mail_to
 * @property string|null $mail_from
 * @property string|null $content_formated
 * @property string|null $content
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\DocumentRepository[] $document_repository
 */
class Mail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'mail_category' => true,
        'mail_type' => true,
        'mail_no' => true,
        'mail_subject' => true,
        'mail_to' => true,
        'mail_from' => true,
        'content_formated' => true,
        'content' => true,
        'instansi' => true,
        'document_repository' => true
    ];
}
