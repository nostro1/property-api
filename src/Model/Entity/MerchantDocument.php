<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MerchantDocument Entity
 *
 * @property int $id
 * @property int|null $unit_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $doc_type
 * @property string|null $path
 *
 * @property \App\Model\Entity\Unit $unit
 */
class MerchantDocument extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unit_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'doc_type' => true,
        'path' => true,
        'unit' => true
    ];
}
