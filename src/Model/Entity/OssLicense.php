<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OssLicense Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property string|null $data_labels
 * @property int $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenDate|null $tgl_dibuat
 * @property \Cake\I18n\FrozenDate|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string $nib_id
 * @property string $oss_id
 * @property string $kode_izin
 * @property string $id_izin
 * @property string $kode_daerah
 * @property int|null $tipe_dokumen
 * @property \Cake\I18n\FrozenDate|null $tgl_terbit_izin
 * @property \Cake\I18n\FrozenDate|null $tgl_berlaku_izin
 * @property string $status
 * @property \Cake\I18n\FrozenDate|null $tgl_status
 * @property string $keterangan
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Nib $nib
 * @property \App\Model\Entity\Oss $oss
 */
class OssLicense extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'data_labels' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'nib_id' => true,
        'oss_id' => true,
        'kode_izin' => true,
        'id_izin' => true,
        'kode_daerah' => true,
        'tipe_dokumen' => true,
        'tgl_terbit_izin' => true,
        'tgl_berlaku_izin' => true,
        'status' => true,
        'tgl_status' => true,
        'keterangan' => true,
        'instansi' => true,
        'nib' => true,
        'oss' => true
    ];
}
