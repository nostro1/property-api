<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OssSendPtpLog Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $nib_id
 * @property string|null $oss_id
 * @property string|null $kode_izin
 * @property string|null $id_izin
 * @property string|null $kode_daerah
 * @property string|null $nomor_ptp
 * @property \Cake\I18n\FrozenDate|null $tgl_terbit_ptp
 * @property string|null $status_ptp
 * @property string|null $pemohon
 * @property string|null $kantah_kabupaten
 * @property string|null $nama_provinsi
 * @property string|null $keterangan
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Nib $nib
 * @property \App\Model\Entity\Oss $oss
 */
class OssSendPtpLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'nib_id' => true,
        'oss_id' => true,
        'kode_izin' => true,
        'id_izin' => true,
        'kode_daerah' => true,
        'nomor_ptp' => true,
        'tgl_terbit_ptp' => true,
        'status_ptp' => true,
        'pemohon' => true,
        'kantah_kabupaten' => true,
        'nama_provinsi' => true,
        'keterangan' => true,
        'instansi' => true,
        'nib' => true,
        'oss' => true
    ];
}
