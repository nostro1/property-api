<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Person Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $name
 * @property string|null $idcard
 * @property string|null $address
 * @property string|null $contact
 * @property string|null $email
 * @property string|null $otp
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Billing[] $billing
 * @property \App\Model\Entity\PersonProperty[] $person_property
 */
class Person extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'name' => true,
        'idcard' => true,
        'address' => true,
        'contact' => true,
        'email' => true,
        'otp' => true,
        'instansi' => true,
        'billing' => true,
        'person_property' => true
    ];
}
