<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PersonProject Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $person_id
 * @property int|null $project_id
 * @property int|null $credit
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\Project $project
 */
class PersonProject extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'person_id' => true,
        'project_id' => true,
        'credit' => true,
        'instansi' => true,
        'person' => true,
        'project' => true
    ];
}
