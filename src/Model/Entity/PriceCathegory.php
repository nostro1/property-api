<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PriceCathegory Entity
 *
 * @property int $id
 * @property int $instansi_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $price_cathegory
 * @property \Cake\I18n\FrozenDate|null $begin_date
 * @property \Cake\I18n\FrozenDate|null $end_date
 * @property string|null $label1
 * @property string|null $label2
 * @property string|null $label3
 * @property string|null $label4
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\ProductPrice[] $product_price
 */
class PriceCathegory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'price_cathegory' => true,
        'begin_date' => true,
        'end_date' => true,
        'label1' => true,
        'label2' => true,
        'label3' => true,
        'label4' => true,
        'instansi' => true,
        'product_price' => true
    ];
}
