<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductPrice Entity
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property int|null $price_cathegory_id
 * @property \Cake\I18n\FrozenDate|null $begin_date
 * @property \Cake\I18n\FrozenDate|null $end_date
 * @property string|null $unit
 * @property int|null $price
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\PriceCathegory $price_cathegory
 */
class ProductPrice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'price_cathegory_id' => true,
        'begin_date' => true,
        'end_date' => true,
        'unit' => true,
        'price' => true,
        'product' => true,
        'price_cathegory' => true
    ];
}
