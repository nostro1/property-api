<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TaskManagement Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int|null $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $label
 * @property \Cake\I18n\FrozenTime|null $tgl_mulai
 * @property \Cake\I18n\FrozenTime|null $tgl_selesai
 * @property string|null $jumlah
 * @property int|null $lft_id
 * @property int|null $rght_id
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Lft $lft
 * @property \App\Model\Entity\Rght $rght
 * @property \App\Model\Entity\ParentTaskManagement $parent_task_management
 * @property \App\Model\Entity\ChildTaskManagement[] $child_task_management
 */
class TaskManagement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'label' => true,
        'tgl_mulai' => true,
        'tgl_selesai' => true,
        'jumlah' => true,
        'lft_id' => true,
        'rght_id' => true,
        'parent_id' => true,
        'instansi' => true,
        'lft' => true,
        'rght' => true,
        'parent_task_management' => true,
        'child_task_management' => true
    ];
}
