<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ticket Entity
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $del
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $venue
 * @property string|null $type
 * @property \Cake\I18n\FrozenDate|null $available_date
 * @property int|null $duration
 * @property string|null $unit
 * @property int|null $quantity
 * @property int|null $booked
 *
 * @property \App\Model\Entity\Product $product
 */
class Ticket extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'del' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'venue' => true,
        'type' => true,
        'available_date' => true,
        'duration' => true,
        'unit' => true,
        'quantity' => true,
        'booked' => true,
        'product' => true
    ];
}
