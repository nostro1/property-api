<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Workflow Entity
 *
 * @property int $id
 * @property int|null $instansi_id
 * @property int $del
 * @property string|null $data_labels
 * @property string|null $dibuat_oleh
 * @property \Cake\I18n\FrozenTime|null $tgl_dibuat
 * @property \Cake\I18n\FrozenTime|null $tgl_diubah
 * @property string|null $diubah_oleh
 * @property string|null $objek_tipe
 * @property string|null $proses_tipe
 * @property int|null $objek_id
 * @property int|null $jenis_proses_id
 * @property string|null $nama_proses
 * @property string|null $tautan
 * @property int|null $form_id
 * @property int|null $template_data_id
 * @property int|null $pengguna_id
 * @property string|null $status
 *
 * @property \App\Model\Entity\Instansi $instansi
 * @property \App\Model\Entity\Objek $objek
 * @property \App\Model\Entity\JenisProse $jenis_prose
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\TemplateData $template_data
 * @property \App\Model\Entity\Pengguna $pengguna
 */
class Workflow extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'instansi_id' => true,
        'del' => true,
        'data_labels' => true,
        'dibuat_oleh' => true,
        'tgl_dibuat' => true,
        'tgl_diubah' => true,
        'diubah_oleh' => true,
        'objek_tipe' => true,
        'proses_tipe' => true,
        'objek_id' => true,
        'jenis_proses_id' => true,
        'nama_proses' => true,
        'tautan' => true,
        'form_id' => true,
        'template_data_id' => true,
        'pengguna_id' => true,
        'status' => true,
        'instansi' => true,
        'objek' => true,
        'jenis_prose' => true,
        'form' => true,
        'template_data' => true,
        'pengguna' => true
    ];
}
