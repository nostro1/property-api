<?php
namespace App\Model\Table;

use App\Model\Entity\AssessmentEvent;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Service\DataSincService;


/**
 * AssessmentEvent Model
 *
 */
class AssessmentEventTable extends AppTable
{

    const STATUS_AKTIF = 'aktif';
    const STATUS_TIDAK_AKTIF = 'tidak aktif';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->strictDelete = true;
        $this->strictUpdate = true;

        $this->setTable('assessment_event');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('FormAssessment', [
            'foreignKey' => 'form_assessment_id'
        ]);

        $this->hasMany('AssessmentParticipant', [
            'foreignKey' => 'assessment_event_id',
            'joinType' => 'LEFT'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->allowEmpty('diubah_oleh');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('assesor_type')
            ->maxLength('assesor_type', 25)
            ->allowEmpty('assesor_type');

        $validator
            ->date('publish_begin')
            ->allowEmpty('publish_begin');

        $validator
            ->date('publish_end')
            ->allowEmpty('publish_end');

        $validator
            ->scalar('status')
            ->maxLength('status', 100)
            ->allowEmpty('status');

        return $validator;
    }
}
