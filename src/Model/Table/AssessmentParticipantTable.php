<?php
namespace App\Model\Table;

use App\Model\Entity\AssessmentParticipant;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Service\DataSincService;


/**
 * AssessmentParticipant Model
 *
 */
class AssessmentParticipantTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->strictDelete = true;
        $this->strictUpdate = true;

        $this->setTable('assessment_participant');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AssessmentEvent', [
            'foreignKey' => 'assessment_event_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('AssessmentParticipantResult', [
            'foreignKey' => 'assessment_participant_id'
        ]);


        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->allowEmpty('dibuat_oleh');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->allowEmpty('diubah_oleh');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->date('assessment_date')
            ->allowEmpty('assessment_date');

        $validator
            ->scalar('participant_name')
            ->maxLength('participant_name', 25)
            ->allowEmpty('participant_name');

        return $validator;
    }
}
