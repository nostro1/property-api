<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CPegawaiDetail Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\PegawaisTable|\Cake\ORM\Association\BelongsTo $Pegawais
 *
 * @method \App\Model\Entity\CPegawaiDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\CPegawaiDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CPegawaiDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CPegawaiDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CPegawaiDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CPegawaiDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CPegawaiDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CPegawaiDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class CPegawaiDetailTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('c_pegawai_detail');

        $this->belongsTo('Instansi', [
            'foreignKey' => 'instansi_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Pegawai', [
            'foreignKey' => 'pegawai_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->requirePresence('del', 'create')
            ->notEmpty('del');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('spesimen_tanda_tangan')
            ->maxLength('spesimen_tanda_tangan', 1000)
            ->allowEmpty('spesimen_tanda_tangan');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Instansis'));
        $rules->add($rules->existsIn(['pegawai_id'], 'Pegawais'));

        return $rules;
    }
}
