<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContentCategory Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\LftsTable|\Cake\ORM\Association\BelongsTo $Lfts
 * @property \App\Model\Table\RghtsTable|\Cake\ORM\Association\BelongsTo $Rghts
 * @property \App\Model\Table\ContentCategoryTable|\Cake\ORM\Association\BelongsTo $ParentContentCategory
 * @property \App\Model\Table\ContentCategoryTable|\Cake\ORM\Association\HasMany $ChildContentCategory
 * @property \App\Model\Table\ContentsTable|\Cake\ORM\Association\HasMany $Contents
 *
 * @method \App\Model\Entity\ContentCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContentCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContentCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContentCategory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContentCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class ContentCategoryTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('content_category');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id'
        ]);
        $this->belongsTo('Rghts', [
            'foreignKey' => 'rght_id'
        ]);
        $this->belongsTo('ParentContentCategory', [
            'className' => 'ContentCategory',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildContentCategory', [
            'className' => 'ContentCategory',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Contents', [
            'foreignKey' => 'content_category_id',
            'joinType' => 'LEFT'
        ]);
        
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->requirePresence('del', 'create')
            ->notEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('category_code')
            ->maxLength('category_code', 255)
            ->requirePresence('category_code', 'create')
            ->notEmpty('category_code');

        $validator
            ->scalar('category_label')
            ->maxLength('category_label', 500)
            ->requirePresence('category_label', 'create')
            ->notEmpty('category_label');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentContentCategory'));

        return $rules;
    }

    Public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        if ($entity->isDirty('category_label')) {
            $entity->slug = strtolower(str_replace(" ", "-", $entity->category_label));
        }
    }
}
