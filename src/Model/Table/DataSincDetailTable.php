<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DataSincDetail Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\DataSincsTable|\Cake\ORM\Association\BelongsTo $DataSincs
 * @property \App\Model\Table\DataKolomsTable|\Cake\ORM\Association\BelongsTo $DataKoloms
 *
 * @method \App\Model\Entity\DataSincDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\DataSincDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DataSincDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DataSincDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DataSincDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DataSincDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DataSincDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DataSincDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class DataSincDetailTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('data_sinc_detail');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instansi', [
            'foreignKey' => 'instansi_id'
        ]);
        $this->belongsTo('DataSinc', [
            'foreignKey' => 'data_sinc_id'
        ]);
        $this->belongsTo('DataKolom', [
            'foreignKey' => 'data_kolom_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('oss_type_kolom')
            ->maxLength('oss_type_kolom', 5)
            ->allowEmpty('oss_type_kolom');

        $validator
            ->scalar('oss_kolom')
            ->maxLength('oss_kolom', 1000)
            ->allowEmpty('oss_kolom');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('is_key')
            ->maxLength('is_key', 2)
            ->allowEmpty('is_key');

        $validator
            ->scalar('value_type')
            ->maxLength('value_type', 25)
            ->allowEmpty('value_type');

        $validator
            ->scalar('maping')
            ->maxLength('maping', 25)
            ->allowEmpty('maping');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Instansi'));
        $rules->add($rules->existsIn(['data_sinc_id'], 'DataSinc'));
        $rules->add($rules->existsIn(['data_kolom_id'], 'DataKolom'));

        return $rules;
    }
}
