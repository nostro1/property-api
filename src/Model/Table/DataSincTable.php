<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DataSinc Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\DataSincTable|\Cake\ORM\Association\BelongsTo $ParentDataSinc
 * @property \App\Model\Table\DataSincTable|\Cake\ORM\Association\HasMany $ChildDataSinc
 * @property \App\Model\Table\DataSincDetailTable|\Cake\ORM\Association\HasMany $DataSincDetail
 * @property \App\Model\Table\DataSincLogTable|\Cake\ORM\Association\HasMany $DataSincLog
 *
 * @method \App\Model\Entity\DataSinc get($primaryKey, $options = [])
 * @method \App\Model\Entity\DataSinc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DataSinc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DataSinc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DataSinc|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DataSinc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DataSinc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DataSinc findOrCreate($search, callable $callback = null, $options = [])
 */
class DataSincTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('data_sinc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instansi', [
            'foreignKey' => 'instansi_id'
        ]);
        $this->belongsTo('ParentDataSinc', [
            'className' => 'DataSinc',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildDataSinc', [
            'className' => 'DataSinc',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('DataSincDetail', [
            'foreignKey' => 'data_sinc_id'
        ]);
        $this->hasMany('DataSincLog', [
            'foreignKey' => 'data_sinc_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('key')
            ->maxLength('key', 1000)
            ->requirePresence('key', 'create')
            ->notEmpty('key');

        $validator
            ->scalar('keterangan')
            ->maxLength('keterangan', 255)
            ->allowEmpty('keterangan');

        $validator
            ->scalar('index')
            ->maxLength('index', 100)
            ->requirePresence('index', 'create')
            ->notEmpty('index');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('aktif_token')
            ->maxLength('aktif_token', 10000)
            ->allowEmpty('aktif_token');

        $validator
            ->scalar('user_akses')
            ->maxLength('user_akses', 100)
            ->allowEmpty('user_akses');

        $validator
            ->scalar('pwd_akses')
            ->maxLength('pwd_akses', 100)
            ->allowEmpty('pwd_akses');

        $validator
            ->scalar('proses')
            ->maxLength('proses', 100)
            ->allowEmpty('proses');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->dateTime('masa_berlaku')
            ->allowEmpty('masa_berlaku');

        $validator
            ->scalar('source_url')
            ->allowEmpty('source_url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Instansi'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentDataSinc'));

        return $rules;
    }
}
