<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Filesystem\Folder;

/**
 * DocumentCategory Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\LftsTable|\Cake\ORM\Association\BelongsTo $Lfts
 * @property \App\Model\Table\RghtsTable|\Cake\ORM\Association\BelongsTo $Rghts
 * @property \App\Model\Table\DocumentCategoryTable|\Cake\ORM\Association\BelongsTo $ParentDocumentCategory
 * @property \App\Model\Table\DocumentCategoryTable|\Cake\ORM\Association\HasMany $ChildDocumentCategory
 * @property \App\Model\Table\DocumentCategoryRoleTable|\Cake\ORM\Association\HasMany $DocumentCategoryRole
 *
 * @method \App\Model\Entity\DocumentCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentCategory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentCategoryTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('document_category');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id'
        ]);

        $this->belongsTo('ParentDocumentCategory', [
            'className' => 'DocumentCategory',
            'foreignKey' => 'parent_id'
        ]);
        
        $this->hasMany('ChildDocumentCategory', [
            'className' => 'DocumentCategory',
            'foreignKey' => 'parent_id'
        ]);

        $this->hasMany('DocumentCategoryRole', [
            'foreignKey' => 'document_category_id'
        ]);

        $this->hasMany('DocumentRepository', [
            'foreignKey' => 'document_category_id'
        ]);
        
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('category_code')
            ->maxLength('category_code', 255)
            ->allowEmpty('category_code');

        $validator
            ->scalar('category_label')
            ->maxLength('category_label', 500)
            ->allowEmpty('category_label');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentDocumentCategory'));

        return $rules;
    }

    Public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        if ($entity->isNew()) {
            $entity->version    = '1.0';
            $folder = new Folder(ROOT . DS . 'webroot'. DS . 'files');
            $folder->create(str_replace(' ', '_', strtolower($entity->category_label)));
        }
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity)
    {
        if ($entity->isDirty('category_label')) {
            $folderold = ROOT . DS . 'webroot'. DS . 'files' . DS . str_replace(' ', '_', strtolower($entity->getOriginal('category_label')));
            $foldernew = ROOT . DS . 'webroot'. DS . 'files' . DS . str_replace(' ', '_', strtolower($entity->category_label));
            if (!rename($folderold, $foldernew)) {
                throw new Exception('Rename folder '.$entity->getOriginal('category_label').' gagal, silahkan coba lagi');
            }
        }

        if ($entity->isDirty('del')) {
            $folder = ROOT . DS . 'webroot'. DS . 'files' . DS . str_replace(' ', '_', strtolower($entity->category_label));
            
            if (!rename($folder, $folder . '_del')) {
                throw new Exception('Rename folder '.$entity->getOriginal('category_label').' gagal, silahkan coba lagi');
            }
        }
    }
}
