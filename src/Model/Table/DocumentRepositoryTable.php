<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Entity;
use ArrayObject;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * DocumentRepository Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\DokumenAksesTable|\Cake\ORM\Association\HasMany $DokumenAkses
 *
 * @method \App\Model\Entity\DocumentRepository get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentRepository newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentRepository[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentRepository|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentRepository|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentRepository patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentRepository[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentRepository findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentRepositoryTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('document_repository');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id'
        ]);

        $this->hasMany('DokumenAkses', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('DocumentAssign', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('DocumentShare', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('DocumentComment', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Workflow', [
            'foreignKey' => 'objek_id',
            'joinType' => 'LEFT',
            'conditions' => [
                'Workflow.objek_tipe' => 'dokumen',
            ],
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del', 'create');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('document_type')
            ->maxLength('document_type', 15)
            ->requirePresence('document_type', 'create')
            ->notEmpty('document_type');

        $validator
            ->scalar('document_category')
            ->maxLength('document_category', 255)
            //->requirePresence('document_category', 'create')
            ->allowEmpty('document_category');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        $validator
            ->scalar('summary')
            ->allowEmpty('summary');

        $validator
            ->scalar('document_path')
            ->maxLength('document_path', 1000)
            ->allowEmpty('document_path');

        $validator
            ->scalar('document_code')
            ->allowEmpty('document_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));

        return $rules;
    }

    Public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        if ($entity->isNew()) {
            $entity->version    = '1.0';
        } else {
            if ($entity->isDirty('document_path')) {                
                $splitVersion = explode(".", $entity->version);
                $subVersion = $splitVersion[1] + 1;
                $entity->version = $splitVersion[0] . '.' . $subVersion;
                 
                if (!$entity->isDirty('summary') && !$entity->isDirty('no_rename')) { //rename
                    $folder = ROOT . DS . 'webroot'. DS . 'files'. DS . str_replace(' ', '_', strtolower($entity->document_category)) . DS;
                    if (!rename($folder . $entity->getOriginal('document_path'), $folder . $entity->document_path)) {
                        throw new Exception('Rename file '.$entity->getOriginal('document_path').' gagal, silahkan coba lagi');
                    }
                }
            } else {
                if ($entity->getOriginal('document_category') != $entity->document_category) { //perubahan folder
                    $folderNew = ROOT . DS . 'webroot'. DS . 'files'. DS . str_replace(' ', '_', strtolower($entity->document_category)) . DS;
                    $folderOld = ROOT . DS . 'webroot'. DS . 'files'. DS . str_replace(' ', '_', strtolower($entity->getOriginal('document_category'))) . DS;
                    
                    /* if(rename($folderOld .  $entity->document_path, $folderNew . $entity->document_path) == false) {
                        throw new Exception('Pemindahan file '. $entity->document_path.' gagal, silahkan coba lagi');
                    } */

                    $file = new File($folderOld.$entity->document_path, true);
                    $file->copy($folderNew.$entity->document_path);
                }
            }
        }
    }
}
