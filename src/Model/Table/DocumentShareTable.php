<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentShare Model
 *
 * @property \App\Model\Table\DokumenRepositorisTable|\Cake\ORM\Association\BelongsTo $DokumenRepositoris
 * @property \App\Model\Table\PenggunasTable|\Cake\ORM\Association\BelongsTo $Penggunas
 *
 * @method \App\Model\Entity\DocumentShare get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentShare newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentShare[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentShare|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentShare|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentShare patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentShare[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentShare findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentShareTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('document_share');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('DocumentRepository', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Pengguna', [
            'foreignKey' => 'pengguna_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->requirePresence('del', 'create')
            ->notEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->boolean('is_public')
            ->allowEmpty('diubah_oleh');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['document_repository_id'], 'DocumentRepository'));
        $rules->add($rules->existsIn(['pengguna_id'], 'Pengguna'));

        return $rules;
    }
}
