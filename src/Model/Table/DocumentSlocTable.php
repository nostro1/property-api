<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentSloc Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\DocumentRepositoriesTable|\Cake\ORM\Association\BelongsTo $DocumentRepositories
 *
 * @method \App\Model\Entity\DocumentSloc get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentSloc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentSloc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentSloc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentSloc|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentSloc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentSloc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentSloc findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentSlocTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('document_sloc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DocumentRepositories', [
            'foreignKey' => 'document_repository_id',
            'joinType' => 'LEFT'
        ]);

        
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('tipe_pekerjaan')
            ->maxLength('tipe_pekerjaan', 255)
            ->allowEmpty('tipe_pekerjaan');

        $validator
            ->scalar('unit_kerja')
            ->maxLength('unit_kerja', 255)
            ->allowEmpty('unit_kerja');

        $validator
            ->scalar('no_box_lama')
            ->maxLength('no_box_lama', 25)
            ->allowEmpty('no_box_lama');

        $validator
            ->scalar('no_box')
            ->maxLength('no_box', 25)
            ->allowEmpty('no_box');

        $validator
            ->scalar('klasifikasi_arsip')
            ->maxLength('klasifikasi_arsip', 25)
            ->allowEmpty('klasifikasi_arsip');

        $validator
            ->scalar('document_Category')
            ->maxLength('document_Category', 100)
            ->allowEmpty('document_Category');

        $validator
            ->scalar('kode_arsip')
            ->maxLength('kode_arsip', 25)
            ->allowEmpty('kode_arsip');

        $validator
            ->scalar('keterangan')
            ->maxLength('keterangan', 10000)
            ->allowEmpty('keterangan');

        $validator
            ->scalar('tahun_dari')
            ->maxLength('tahun_dari', 4)
            ->allowEmpty('tahun_dari');

        $validator
            ->scalar('tahun_sampai')
            ->maxLength('tahun_sampai', 4)
            ->allowEmpty('tahun_sampai');

        $validator
            ->scalar('media')
            ->maxLength('media', 25)
            ->allowEmpty('media');

        $validator
            ->scalar('tingkat_keaslian')
            ->maxLength('tingkat_keaslian', 100)
            ->allowEmpty('tingkat_keaslian');

        $validator
            ->allowEmpty('jumlah');

        $validator
            ->scalar('satuan')
            ->maxLength('satuan', 25)
            ->allowEmpty('satuan');

        $validator
            ->scalar('kondisi')
            ->maxLength('kondisi', 100)
            ->allowEmpty('kondisi');

        $validator
            ->scalar('foto_sebelum')
            ->maxLength('foto_sebelum', 1000)
            ->allowEmpty('foto_sebelum');

        $validator
            ->scalar('foto_sesudah')
            ->maxLength('foto_sesudah', 1000)
            ->allowEmpty('foto_sesudah');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));
        $rules->add($rules->existsIn(['document_repository_id'], 'DocumentRepositories'));

        return $rules;
    }
}
