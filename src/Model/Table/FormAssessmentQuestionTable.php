<?php
namespace App\Model\Table;

use App\Model\Entity\FormAssessmentQuestion;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Service\DataSincService;


/**
 * FormAssessmentQuestion Model
 *
 */
class FormAssessmentQuestionTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->strictDelete = true;
        $this->strictUpdate = true;

        $this->setTable('form_assessment_question');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('FormAssessment', [
            'foreignKey' => 'form_assessment_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('AnswerOptions', [
            'foreignKey' => 'form_assessment_question_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

            $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->allowEmpty('dibuat_oleh');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->allowEmpty('diubah_oleh');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->integer('sequence')
            ->requirePresence('sequence')
            ->notEmpty('sequence');

        $validator
            ->scalar('question_text')
            ->maxLength('question_text', 1000)
            ->allowEmpty('question_text');

        $validator
            ->scalar('answer_type')
            ->maxLength('answer_type', 100)
            ->allowEmpty('answer_type');

        return $validator;
    }

}
