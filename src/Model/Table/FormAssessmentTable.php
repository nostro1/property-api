<?php
namespace App\Model\Table;

use App\Model\Entity\FormAssessment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Service\DataSincService;


/**
 * FormAssessment Model
 *
 */
class FormAssessmentTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->strictDelete = true;
        $this->strictUpdate = true;

        $this->setTable('form_assessment');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instansi', [
            'foreignKey' => 'instansi_id'
        ]);

        $this->hasMany('FormAssessmentQuestion', [
            'foreignKey' => 'form_assessment_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('AssessmentEvent', [
            'foreignKey' => 'form_assessment_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasOne('JenisIzin', [
            'foreignKey' => 'form_assessment_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
            'joinType'   => 'LEFT'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->allowEmpty('dibuat_oleh');

        $validator
            ->date('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->allowEmpty('diubah_oleh');

        $validator
            ->date('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmpty('description');

        $validator
            ->scalar('assesor_type')
            ->maxLength('assesor_type', 25)
            ->allowEmpty('assesor_type');

        $validator
            ->scalar('assessment_type')
            ->maxLength('assessment_type', 20)
            ->allowEmpty('assessment_type');

        return $validator;
    }
}
