<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PersonProperty Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Instansis
 * @property |\Cake\ORM\Association\BelongsTo $People
 * @property |\Cake\ORM\Association\BelongsTo $PropertyUnits
 *
 * @method \App\Model\Entity\PersonProperty get($primaryKey, $options = [])
 * @method \App\Model\Entity\PersonProperty newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PersonProperty[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PersonProperty|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PersonProperty|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PersonProperty patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PersonProperty[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PersonProperty findOrCreate($search, callable $callback = null, $options = [])
 */
class PersonPropertyTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('person_property');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id'
        ]);

        $this->belongsTo('Person', [
            'foreignKey' => 'person_id'
        ]);

        $this->belongsTo('PropertyUnit', [
            'foreignKey' => 'property_unit_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ],
            ],
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events'        => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ],
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('type')
            ->maxLength('type', 25)
            ->allowEmpty('type');

        $validator
            ->scalar('status')
            ->maxLength('status', 25)
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));
        $rules->add($rules->existsIn(['person_id'], 'Person'));
        $rules->add($rules->existsIn(['property_unit_id'], 'PropertyUnit'));

        return $rules;
    }
}
