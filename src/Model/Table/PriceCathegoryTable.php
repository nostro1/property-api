<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PriceCathegory Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\ProductPriceTable|\Cake\ORM\Association\HasMany $ProductPrice
 *
 * @method \App\Model\Entity\PriceCathegory get($primaryKey, $options = [])
 * @method \App\Model\Entity\PriceCathegory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PriceCathegory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PriceCathegory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceCathegory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceCathegory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PriceCathegory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PriceCathegory findOrCreate($search, callable $callback = null, $options = [])
 */
class PriceCathegoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('price_cathegory');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instansis', [
            'foreignKey' => 'instansi_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ProductPrice', [
            'foreignKey' => 'price_cathegory_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'tgl_dibuat' => 'new',
                    'tgl_diubah' => 'existing',
                ]
            ]
        ]);

        $this->addBehavior('Muffin/Footprint.Footprint', [
            'events' => [
                'Model.beforeSave' => [
                    'dibuat_oleh' => 'new',
                    'diubah_oleh' => 'existing',
                ]
            ],
            'propertiesMap' => [
                'dibuat_oleh' => '_footprint.username',
                'diubah_oleh' => '_footprint.username',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('price_cathegory')
            ->maxLength('price_cathegory', 1000)
            ->allowEmpty('price_cathegory');

        $validator
            ->date('begin_date')
            ->allowEmpty('begin_date');

        $validator
            ->date('end_date')
            ->allowEmpty('end_date');

        $validator
            ->scalar('label1')
            ->maxLength('label1', 255)
            ->allowEmpty('label1');

        $validator
            ->scalar('label2')
            ->maxLength('label2', 255)
            ->allowEmpty('label2');

        $validator
            ->scalar('label3')
            ->maxLength('label3', 255)
            ->allowEmpty('label3');

        $validator
            ->scalar('label4')
            ->maxLength('label4', 255)
            ->allowEmpty('label4');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Instansis'));

        return $rules;
    }
}
