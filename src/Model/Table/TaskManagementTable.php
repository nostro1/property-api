<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaskManagement Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\LftsTable|\Cake\ORM\Association\BelongsTo $Lfts
 * @property \App\Model\Table\RghtsTable|\Cake\ORM\Association\BelongsTo $Rghts
 * @property \App\Model\Table\TaskManagementTable|\Cake\ORM\Association\BelongsTo $ParentTaskManagement
 * @property \App\Model\Table\TaskManagementTable|\Cake\ORM\Association\HasMany $ChildTaskManagement
 *
 * @method \App\Model\Entity\TaskManagement get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaskManagement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaskManagement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaskManagement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaskManagement|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaskManagement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaskManagement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaskManagement findOrCreate($search, callable $callback = null, $options = [])
 */
class TaskManagementTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('task_management');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instansis', [
            'foreignKey' => 'instansi_id'
        ]);
        $this->belongsTo('Lfts', [
            'foreignKey' => 'lft_id'
        ]);
        $this->belongsTo('Rghts', [
            'foreignKey' => 'rght_id'
        ]);
        $this->belongsTo('ParentTaskManagement', [
            'className' => 'TaskManagement',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildTaskManagement', [
            'className' => 'TaskManagement',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->allowEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('label')
            ->maxLength('label', 10000)
            ->allowEmpty('label');

        $validator
            ->dateTime('tgl_mulai')
            ->allowEmpty('tgl_mulai');

        $validator
            ->dateTime('tgl_selesai')
            ->allowEmpty('tgl_selesai');

        $validator
            ->scalar('jumlah')
            ->maxLength('jumlah', 255)
            ->allowEmpty('jumlah');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Instansis'));
        $rules->add($rules->existsIn(['lft_id'], 'Lfts'));
        $rules->add($rules->existsIn(['rght_id'], 'Rghts'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentTaskManagement'));

        return $rules;
    }
}
