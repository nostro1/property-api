<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Workflow Model
 *
 * @property \App\Model\Table\InstansisTable|\Cake\ORM\Association\BelongsTo $Instansis
 * @property \App\Model\Table\ObjeksTable|\Cake\ORM\Association\BelongsTo $Objeks
 * @property \App\Model\Table\JenisProsesTable|\Cake\ORM\Association\BelongsTo $JenisProses
 * @property \App\Model\Table\FormsTable|\Cake\ORM\Association\BelongsTo $Forms
 * @property \App\Model\Table\TemplateDataTable|\Cake\ORM\Association\BelongsTo $TemplateData
 * @property \App\Model\Table\PenggunasTable|\Cake\ORM\Association\BelongsTo $Penggunas
 *
 * @method \App\Model\Entity\Workflow get($primaryKey, $options = [])
 * @method \App\Model\Entity\Workflow newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Workflow[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Workflow|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workflow|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workflow patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Workflow[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Workflow findOrCreate($search, callable $callback = null, $options = [])
 */
class WorkflowTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('workflow');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Unit', [
            'foreignKey' => 'instansi_id'
        ]);

        $this->belongsTo('JenisProses', [
            'foreignKey' => 'jenis_proses_id'
        ]);
        
        $this->belongsTo('DocumentRepository', [
            'foreignKey' => 'objek_id'
        ]);

        $this->belongsTo('Form', [
            'foreignKey' => 'form_id'
        ]);

        $this->belongsTo('TemplateData', [
            'foreignKey' => 'template_data_id'
        ]);

        $this->belongsTo('Pengguna', [
            'foreignKey' => 'pengguna_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('del')
            ->notEmpty('del');

        $validator
            ->scalar('data_labels')
            ->allowEmpty('data_labels');

        $validator
            ->scalar('dibuat_oleh')
            ->maxLength('dibuat_oleh', 25)
            ->allowEmpty('dibuat_oleh');

        $validator
            ->dateTime('tgl_dibuat')
            ->allowEmpty('tgl_dibuat');

        $validator
            ->dateTime('tgl_diubah')
            ->allowEmpty('tgl_diubah');

        $validator
            ->scalar('diubah_oleh')
            ->maxLength('diubah_oleh', 25)
            ->allowEmpty('diubah_oleh');

        $validator
            ->scalar('objek_tipe')
            ->maxLength('objek_tipe', 25)
            ->allowEmpty('objek_tipe');

        $validator
            ->scalar('proses_tipe')
            ->maxLength('proses_tipe', 100)
            ->allowEmpty('proses_tipe');

        $validator
            ->scalar('nama_proses')
            ->maxLength('nama_proses', 255)
            ->allowEmpty('nama_proses');

        $validator
            ->scalar('tautan')
            ->maxLength('tautan', 1000)
            ->allowEmpty('tautan');

        $validator
            ->scalar('status')
            ->maxLength('status', 25)
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instansi_id'], 'Unit'));
        $rules->add($rules->existsIn(['jenis_proses_id'], 'JenisProses'));
        $rules->add($rules->existsIn(['form_id'], 'Form'));
        $rules->add($rules->existsIn(['template_data_id'], 'TemplateData'));
        $rules->add($rules->existsIn(['pengguna_id'], 'Pengguna'));

        return $rules;
    }
}
