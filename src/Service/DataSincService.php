<?php

namespace App\Service;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class DataSincService extends AuthService
{
    /*
     * Fungsi Pendukung singkronisasi data
     */
    public static function setLog($id = null, $logData = null)
    {
        $logTable = TableRegistry::get('data_sinc_log');

        if (is_null($id)) {
            $cekLog = $logTable->find()
                ->select(['id'])
                ->where([
                    'log' => json_encode($logData),
                    'tgl_dibuat' => Time::now()
                ])
                ->first();

            if (!empty($cekLog)) {
                $log = $logTable->get($cekLog['id']);
            } else {
                $log = $logTable->newEntity();
            }
        } else {
            $log = $logTable->get($id);
        }

        if (is_null($log['log']) || empty($logData)) {
            $log['log'] = json_encode($logData);
        } else {
            if (is_object(json_decode($log['log']))) {
                $lastLog = array(json_decode($log['log']));
            } else {
                $lastLog = json_decode($log['log']);
            }
            array_push($lastLog, $logData);
            $log['log'] = json_encode($lastLog);
        }

        $logTable->save($log);
        return $log['id'];
    }

    public static function setStatusOSS($id = null, $statusData = true, $detailData = null)
    {
        $statusTable = TableRegistry::get('data_sinc_status');

        if (is_null($id)) {
            $status = $statusTable->newEntity();
            $status['status'] = $statusData;

            $statusExist = $statusTable
                ->find('all')
                ->where([
                    'tgl_dibuat' => Time::now(),
                    'status' => $statusData,
                    'detail' => json_encode($detailData)
                ])
                ->extract('id')
                ->toArray();

            if (!empty($statusExist)) {
                $status = $statusTable->get($statusExist[0]);
            }

        } else {
            $status = $statusTable->get($id);
            //Jika ada perubahan dari true ke false rubah status
            if ($status['status'] && !$statusData) {
                $status['status'] = $statusData;
            }
        }

        if (is_null($status['detail']) || empty($detailData)) {
            $status['detail'] = json_encode($detailData);
        } else {
            if (is_object(json_decode($status['detail']))) {
                $lastDetail = array(json_decode($status['detail']));
            } else {
                $lastDetail = json_decode($status['detail']);
            }
            array_push($lastDetail, $detailData);
            $status['detail'] = json_encode($lastDetail);
        }

        $statusTable->save($status);

        return $status['id'];
    }

    public static function getResponse($id)
    {
        $statusTable = TableRegistry::get('data_sinc_status');
        $status = $statusTable->get($id);

        return [
            'status' => $status['status'],
            'detail' => $status['detail']
        ];
    }

    public static function getInstansiId($kode_daerah)
    {
        $tmp = substr($kode_daerah, 0, 4);

        if (substr($tmp, 2, 2) != "00") {
            $kab = substr($tmp, 0, 2) . "." . substr($tmp, 2, 2);
        } else {
            $kab = substr($tmp, 0, 2);
        }

        $InstansiTbl = TableRegistry::get('unit');
        $InstansiData = $InstansiTbl->find()->select();

        if (substr($kode_daerah, 0, 4) == '00000') {
            $InstansiData->where(['instansi_code' => substr($kode_daerah, 0, 3), 'tipe' => $InstansiTbl::TIPE_INSTANSI]);
        } else {
            $InstansiData->where(['kode_daerah' => $kab, 'tipe' => $InstansiTbl::TIPE_INSTANSI]);
        }
        $instansiId = $InstansiData->first()['id'];

        return $instansiId;
    }
}
