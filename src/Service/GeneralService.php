<?php
/**
 * Created by PhpStorm.
 * User: core
 * Date: 02/10/16
 * Time: 11:33
 */

namespace App\Service;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Client;
use Cake\Utility\Inflector;
use Cake\Utility\Xml;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Routing\Router;

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;

class GeneralService extends AuthService
{
    /*
     * Function for general purpose
     */
    public static function generateQrLogo($key, $value, $targetKey = null, $matches, $logo = 'logo-kominfo.png')
    {
        $width = 100;
        //$userDefinedWidth = intval(str_replace('_qrlogo_','', $matches[0]));

       /*  if ($userDefinedWidth > 0) {
            $width = (int) $userDefinedWidth;
        } */

        $dir = new Folder(
        ROOT . DS . 'tmp' . DS . 'cache' . DS . 'qrcode',
        true
        );

        // Generate QR Code Image
        if ($value) {
            $qrCode = new QrCode($value);
            $qrCode->setSize($width);

            // Set advanced options
            $qrCode->setWriterByName('png');
            $qrCode->setMargin(10);
            $qrCode->setEncoding('UTF-8');
            $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
            $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
            $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
            $qrCode->setLogoPath(ROOT . DS . 'webroot' . DS . 'img' . DS . $logo);

            $qrCode->setLogoSize(25, 25);
            $qrCode->setRoundBlockSize(false);
            $qrCode->setValidateResult(false);
            $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);

            // Directly output the QR code
            //header('Content-Type: '.$qrCode->getContentType());
            $qrCode->writeString();

            // Save it to a file
            $filename = date('YmdHis') . rand().'.png';
            $imagePath = $dir->path . DS . $filename;

            $qrCode->writeFile($imagePath);

        }
        
        if (file_exists($imagePath)) {
            if ($targetKey) {
                $key = $targetKey;
            }

            // remove size suffix
            $key = preg_replace('/(_[0-9]+)/', '', $key);

            return [
                    $key,
                    ['src' => $imagePath, 'swh' => $width]
                ];
        } else {
            return false;
        }
    }
}
