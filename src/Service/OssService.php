<?php
/**
 * Created by PhpStorm.
 * User: core
 * Date: 02/10/16
 * Time: 11:33
 */

namespace App\Service;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Client;
use App\Service\DataSincService;
use App\Service\ReportGeneratorService;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Routing\Router;

class OssService extends AuthService
{
    private static $baseUrl = null;
    private static $logId = null;
    /*
     * Fungsi Pendukung OSS
     */
    public static function getToken()
    {
        $token = null;
        self::$logId = null;

        $serviceTable = TableRegistry::get('service_eksternal');
        $serviceData = $serviceTable->find()->select()
            ->where(['nama' => 'OSS'])
            ->first();
        
        self::$baseUrl = $serviceData['base_url'];

        if (empty($serviceData)) return null;

        if (
            !is_null($serviceData['token']) &&
            date('Y-m-d h:i:sa', strtotime($serviceData['waktu_berlaku_token'])) >= Time::now()
        )
            return $serviceData['token'];

        $data = [
            'securityKey' => [
                'user_akses' => $serviceData['username'],
                'pwd_akses' => $serviceData['password']
            ]
        ];

        try {
            $http = new Client();
            $response = $http->post(self::$baseUrl . '/sendSecurityKey',
                json_encode($data),
                [
                    'type' => 'json',
                    'auth' => ['username' => '', 'password' => '']
                ]
            );

            if ($response->code != 200) {
                self::$logId = DataSincService::setLog(self::$logId,
                    ['proses'=>'Get Token error ', 'status' => false, 'data' => $response->body]
                );
                return false;
            } 

            $service = $serviceTable->get($serviceData['id']);
            $service['token'] = $response->json['responsendSecurityKey']['key'];
            $service['waktu_berlaku_token'] = $response->json['responsendSecurityKey']['masa_berlaku'];
            $serviceTable->save($service);

            $token = $response->json['responsendSecurityKey']['key'];

            self::$logId = DataSincService::setLog(self::$logId,
                ['proses'=>'Get Token succes ', 'status' => true, 'data' => $token]
            );
        } catch (\Exception $e) {
            self::$logId = DataSincService::setLog(self::$logId,
                ['proses'=>'Get Token error ', 'status' => false, 'data' => json_encode($e)]
            );
        }

        return $token;
    }
    
    public function setOSSLog($nib_id = null, $status = null, $keterangan = null)
    {
        $tblLog = TableRegistry::get('oss_log');
        $log = $tblLog->newEntity();
        $log['nib'] = $nib_id;
        $log['status'] = $status;
        $log['log'] = $keterangan;
        $tblLog->save($log);
    }
    
    public static function sendStatus($entity)
    {
        $token = self::getToken();

        if (is_null(self::$baseUrl)) return;

        $permohonanTbl = TableRegistry::get('permohonan_izin');
        $permohonanData = $permohonanTbl->find()
        ->select(['id', 'nib_id', 'id_proyek', 'oss_id', 'kd_izin', 'kode_oss', 'kode_instansi' => 'b.instansi_code'])
        ->leftJoin(['b' => 'unit'], ['b.id = permohonan_izin.instansi_id'])
        ->where(['permohonan_izin.id' => $entity->id, 'nib_id is not' => null])
        ->first();

        $data = null;
        if (!empty($permohonanData)) {
            $kode = null;
            if (!is_null($permohonanData['nib_id'])) {
                $prosesPermohonanTable = TableRegistry::get('proses_permohonan');

                $prosesPermohonan = $prosesPermohonanTable->find()
                    ->select(['kode' => 'b.kode_oss', 'nama_proses' => 'b.nama_proses'])
                    ->innerJoin(['b' => 'jenis_proses'], ['b.id = proses_permohonan.jenis_proses_id'])
                    ->where(['permohonan_izin_id' => $permohonanData['id'], 
                            'status' => AlurProsesService::STATUS_PROSES])
                    ->order(['proses_permohonan.id asc'])
                    ->last();

                if (empty($prosesPermohonan)) {
                    $jenisProsesTable = TableRegistry::get('jenis_proses');
                    $jenisProses = $jenisProsesTable->find()
                        ->select(['kode' => 'kode_oss', 'nama_proses'])
                        ->where(['id' => 18])
                        ->first();
                    $kode = $jenisProses['kode'];
                    $nama_proses = $jenisProses['nama_proses'];
                } else {
                    $kode = $prosesPermohonan['kode'];
                    $nama_proses = $prosesPermohonan['nama_proses'];
                }

                $data = [
                    'IZINSTATUS' => [
                        'nib' => $permohonanData['nib_id'],
                        'id_proyek' => $permohonanData['id_proyek'],
                        'oss_id' => $permohonanData['oss_id'],
                        'id_izin' => $permohonanData['kd_izin'],
                        'kd_izin' => $permohonanData['kode_oss'],
                        'kd_instansi' => $permohonanData['kode_instansi'],
                        'kd_status' => $kode,
                        'tgl_status' => Time::now(),
                        'nip_status' => '',
                        'nama_status' => $nama_proses,
                        'keterangan' => $nama_proses,
                        'status_izin' => ''
                    ]
                ];

                self::$logId = DataSincService::setLog(
                    self::$logId,
                    ['proses' => 'Send Status ', 'status' => true, 'token' => $token, 'data' => $data]
                );

                try {
                    $http = new Client();
                    $response = $http->post(
                        self::$baseUrl . '/receiveLicenseStatus',
                        json_encode($data),
                        [
                            'type' => 'json',
                            'headers' => ['OSS-API-KEY' => self::getToken()],
                            'auth' => ['username' => '', 'password' => '']
                        ]
                    );

                    if ($response->code != 200) {
                        self::$logId = DataSincService::setLog(self::$logId,
                            ['proses'=>'Send Status error', 'status' => false, 'data' => $response->body]
                        );
                    } else {
                        self::$logId = DataSincService::setLog(self::$logId,
                            ['proses'=>'Send Status ok', 'status' => true, 'data' => $response->body]
                        );
                    }
                
                } catch (\Exception $e) {
                    $this->logId = DataSincService::setLog(
                        $this->logId,
                        [
                            'proses' => 'Koneksi bermasalah',
                            'status' => false,
                            'data' => json_encode($e->getMessage())
                        ]
                    );

                    $return = [
                        'status' => false,
                        'message' => $e->getMessage()
                    ];
                }
            }
        }
    }

    public static function receiveLicense($entity, $statusDitetapkan)
    {
        $file = null;
        $token = self::getToken();

        if (is_null(self::$baseUrl)) return;

        $permohonanTbl = TableRegistry::get('permohonan_izin');
        $permohonanData = $permohonanTbl->find()
            ->select([
                'id' => 'permohonan_izin.id', 
                'no_permohonan',
                'instansi_id' => 'permohonan_izin.instansi_id',
                'nib_id',
                'id_proyek',
                'oss_id',
                'kd_izin' => 'permohonan_izin.kd_izin',
                'kode_oss',
                'kd_daerah',
                'status_penetapan'
            ])
            ->where([
                'permohonan_izin.id' => $entity->id,
                'nib_id is not' => null
            ])
            ->first();

        $statusIzin = $permohonanData['status_penetapan'] == $statusDitetapkan ? '50' : '90';
        $data = null;

        if (!empty($permohonanData)) {

            if (!is_null($permohonanData['nib_id'])) {
                $unitNomenklaturTable = TableRegistry::get('unit_nomenklatur');
                $unitNomenklatur = $unitNomenklaturTable->find()
                    ->select(['nama_daerah'])
                    ->where(['unit_id' => $permohonanData['instansi_id']])
                    ->first();

                $prosesPermohonanTable = TableRegistry::get('proses_permohonan');
                $fileIzin = $prosesPermohonanTable->find()
                    ->select(['file_signed_report'])
                    ->where(['permohonan_izin_id' => $entity->id, 'file_signed_report is not' => null])
                    ->first();
                
                if (!empty($fileIzin)) {
                    $fileIzin['file_signed_report'] = ReportGeneratorService::getResultFolderPath() . DS . $fileIzin['file_signed_report'];
                    $file = Router::url(str_replace(ROOT, '', $fileIzin['file_signed_report']), true);
                }

                $data = [
                    'IZINFINAL' => [
                        'nib' => $permohonanData['nib_id'],
                        'id_proyek' => $permohonanData['id_proyek'],
                        'oss_id' => $permohonanData['oss_id'],
                        'id_izin' => $permohonanData['kd_izin'],
                        'kd_izin' => $permohonanData['kode_oss'],
                        'kd_daerah' => $permohonanData['kd_daerah'],
                        'nomor_izin' => $permohonanData['no_permohonan'],
                        'tgl_terbit_izin' => null,
                        'tgl_berlaku_izin' => null,
                        'nama_ttd' => $unitNomenklatur['nama_daerah'],
                        'nip_ttd' => '',
                        'jabatan_ttd' => '',
                        'status_izin' => $statusIzin,
                        'file_izin' => $file,
                        'keterangan' => 'Penetapan Izin',
                        'data_pnbp' => [
                            'kd_akun' => '',
                             'kd_penerimaan' => '',
                             'nominal' => ''
                        ]
                    ]
                ];

                if ($statusIzin == '50') {
                    $izinTbl = TableRegistry::get('Izin');
                    $izinData = $izinTbl->find()
                        ->select([
                            'mulai_berlaku',
                            'akhir_berlaku'
                        ])
                        ->where([
                            'permohonan_izin_id' => $entity->id
                        ])
                        ->first();

                    if (!empty($izinData)) {
                        $data['IZINFINAL']['tgl_terbit_izin'] = self::formatTanggalOss($izinData['mulai_berlaku']);
                        $data['IZINFINAL']['tgl_berlaku_izin'] = self::formatTanggalOss($izinData['akhir_berlaku']);
                    }
                    
                }

                self::$logId = DataSincService::setLog(
                    self::$logId,
                    ['proses'=> 'Kirim receiveLicense ', 'status' => true, 'token' => $token, 'data' => $data]
                );

                try {
                    $http = new Client();
                    $response = $http->post(
                        self::$baseUrl . '/receiveLicense',
                        json_encode($data),
                        [
                            'type' => 'json',
                            'headers' => ['OSS-API-KEY' => $token],
                            'auth' => ['username' => '', 'password' => '']
                        ]
                    );

                    if ($response->code != 200) {
                        self::$logId = DataSincService::setLog(
                            self::$logId,
                            ['proses'=>'Error receiveLicense ', 'status' => true, 'data' => $response->body]
                        );
                    } else {
                        self::$logId = DataSincService::setLog(
                            self::$logId,
                            ['proses'=>'Sukses receiveLicense ', 'status' => true, 'data' => $response->body]
                        );
                    }
                } catch (\Exception $e) {
                    $this->logId = DataSincService::setLog(
                        $this->logId,
                        [
                            'proses' => 'Koneksi bermasalah',
                            'status' => false,
                            'data' => json_encode($e->getMessage())
                        ]
                    );

                    $return = [
                        'status' => false,
                        'message' => $e->getMessage()
                    ];
                }
            }
        }
    }

    public static function receivePostAudit($izinData)
    {
        $token = self::getToken();

        if ($token) {
            $data = [
                'receivePostAudit' => [
                    'nib' => $izinData['nib_id'],
                    'id_proyek' => $izinData['id_proyek'],
                    'oss_id' => $izinData['oss_id'],
                    'id_izin' => $izinData['kd_izin'],
                    'kd_izin' => $izinData['kode_oss'],
                    'kd_daerah' => $izinData['kd_daerah'],
                    'status_validasi' => $izinData['status_validasi'],
                    'tgl_validasi' => Time::now(),
                    'keterangan' => $izinData['keterangan_status']
            ]];

            self::$logId = DataSincService::setLog(
                self::$logId,
                ['proses'=>'Kirim Postaudit ', 'status' => true, 'data' => $data]
            );

            $http = new Client();
            $response = $http->post(
                        self::$baseUrl . '/api/receivePostAudit',
                        json_encode($data),
                        [
                            'type' => 'json',
                            'headers' => ['OSS-API-KEY' => $token],
                            'auth' => ['username' => '', 'password' => '']
                        ]
            );

            if ($response->code != 200) {
                self::$logId = DataSincService::setLog(
                    self::$logId,
                    [
                        'proses'=>'Error Postaudit : ', 
                        'status' => true, 
                        'data' => $response->body
                    ]
                );
            } else {
                self::$logId = DataSincService::setLog(
                    self::$logId,
                    ['proses'=>'Sukses Postaudit ', 'status' => true, 'data' => $response->body]
                );
            }
        }
        
    }

    public function getInstansiId($kdDaerah) {
        $tmp = substr($kdDaerah, 0, 4);

        if (substr($tmp, 2, 2) != "00") {
            $kab = substr($tmp, 0, 2) . "." . substr($tmp, 2, 2);
        } else {
            $kab = substr($tmp, 0, 2);
        }

        $jenis_izin = substr($nibData['kd_izin'], 9, 3);
        $InstansiTbl = TableRegistry::get('unit');
        $InstansiData = $InstansiTbl->find()->select();

        if (substr($kdDaerah, 0, 4) == '00000') {
            $InstansiData->where(['instansi_code' => substr($kdDaerah, 0, 3), 'tipe' => 'I']);
        } else {
            $InstansiData->where(['kode_daerah' => $kab, 'tipe' => 'I']);
        }

        return $InstansiData->first()['id'];
    }
    
    private static function formatTanggalOss($tanggal) {
        $newFormat = new Date($tanggal);
        
        return $newFormat->format('Y-m-d');
    }
}


