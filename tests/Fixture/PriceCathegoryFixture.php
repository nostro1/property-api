<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PriceCathegoryFixture
 *
 */
class PriceCathegoryFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'price_cathegory';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'instansi_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'del' => ['type' => 'integer', 'length' => 10, 'default' => '0', 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'dibuat_oleh' => ['type' => 'string', 'length' => 25, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'tgl_dibuat' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'tgl_diubah' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'diubah_oleh' => ['type' => 'string', 'length' => 25, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'price_cathegory' => ['type' => 'string', 'length' => 1000, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'begin_date' => ['type' => 'date', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'end_date' => ['type' => 'date', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'label1' => ['type' => 'string', 'length' => 255, 'default' => '', 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'label2' => ['type' => 'string', 'length' => 255, 'default' => '', 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'label3' => ['type' => 'string', 'length' => 255, 'default' => '', 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'label4' => ['type' => 'string', 'length' => 255, 'default' => '', 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'instansi_id' => 1,
                'del' => 1,
                'dibuat_oleh' => 'Lorem ipsum dolor sit a',
                'tgl_dibuat' => 1667880595,
                'tgl_diubah' => 1667880595,
                'diubah_oleh' => 'Lorem ipsum dolor sit a',
                'price_cathegory' => 'Lorem ipsum dolor sit amet',
                'begin_date' => '2022-11-08',
                'end_date' => '2022-11-08',
                'label1' => 'Lorem ipsum dolor sit amet',
                'label2' => 'Lorem ipsum dolor sit amet',
                'label3' => 'Lorem ipsum dolor sit amet',
                'label4' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
