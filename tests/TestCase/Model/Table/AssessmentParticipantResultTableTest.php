<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssessmentParticipantResultTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AssessmentParticipantResultTable Test Case
 */
class AssessmentParticipantResultTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AssessmentParticipantResultTable
     */
    public $AssessmentParticipantResult;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.assessment_participant_result',
        'app.instansis',
        'app.assessment_participants',
        'app.assessment_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AssessmentParticipantResult') ? [] : ['className' => AssessmentParticipantResultTable::class];
        $this->AssessmentParticipantResult = TableRegistry::getTableLocator()->get('AssessmentParticipantResult', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AssessmentParticipantResult);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
