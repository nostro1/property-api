<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BillingDetailTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BillingDetailTable Test Case
 */
class BillingDetailTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BillingDetailTable
     */
    public $BillingDetail;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.billing_detail',
        'app.instansis',
        'app.billings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BillingDetail') ? [] : ['className' => BillingDetailTable::class];
        $this->BillingDetail = TableRegistry::getTableLocator()->get('BillingDetail', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BillingDetail);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
