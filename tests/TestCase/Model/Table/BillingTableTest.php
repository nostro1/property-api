<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BillingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BillingTable Test Case
 */
class BillingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BillingTable
     */
    public $Billing;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.billing',
        'app.instansis',
        'app.people',
        'app.billing_detail'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Billing') ? [] : ['className' => BillingTable::class];
        $this->Billing = TableRegistry::getTableLocator()->get('Billing', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Billing);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
