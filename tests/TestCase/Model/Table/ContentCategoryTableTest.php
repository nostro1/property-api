<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentCategoryTable Test Case
 */
class ContentCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentCategoryTable
     */
    public $ContentCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.content_category',
        'app.instansis',
        'app.lfts',
        'app.rghts',
        'app.contents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContentCategory') ? [] : ['className' => ContentCategoryTable::class];
        $this->ContentCategory = TableRegistry::getTableLocator()->get('ContentCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
