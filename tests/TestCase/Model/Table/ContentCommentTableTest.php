<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentCommentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentCommentTable Test Case
 */
class ContentCommentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentCommentTable
     */
    public $ContentComment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.content_comment',
        'app.instansis',
        'app.contents',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContentComment') ? [] : ['className' => ContentCommentTable::class];
        $this->ContentComment = TableRegistry::getTableLocator()->get('ContentComment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentComment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
