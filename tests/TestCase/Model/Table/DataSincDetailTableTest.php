<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DataSincDetailTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DataSincDetailTable Test Case
 */
class DataSincDetailTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DataSincDetailTable
     */
    public $DataSincDetail;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.data_sinc_detail',
        'app.instansis',
        'app.data_sincs',
        'app.data_koloms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DataSincDetail') ? [] : ['className' => DataSincDetailTable::class];
        $this->DataSincDetail = TableRegistry::getTableLocator()->get('DataSincDetail', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DataSincDetail);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
