<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DataSincTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DataSincTable Test Case
 */
class DataSincTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DataSincTable
     */
    public $DataSinc;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.data_sinc',
        'app.instansis',
        'app.data_sinc_detail',
        'app.data_sinc_log'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DataSinc') ? [] : ['className' => DataSincTable::class];
        $this->DataSinc = TableRegistry::getTableLocator()->get('DataSinc', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DataSinc);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
