<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentAssignTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentAssignTable Test Case
 */
class DocumentAssignTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentAssignTable
     */
    public $DocumentAssign;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_assign',
        'app.dokumen_repositoris',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentAssign') ? [] : ['className' => DocumentAssignTable::class];
        $this->DocumentAssign = TableRegistry::getTableLocator()->get('DocumentAssign', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentAssign);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
