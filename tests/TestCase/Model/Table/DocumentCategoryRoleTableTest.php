<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentCategoryRoleTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentCategoryRoleTable Test Case
 */
class DocumentCategoryRoleTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentCategoryRoleTable
     */
    public $DocumentCategoryRole;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_category_role',
        'app.document_categories',
        'app.units',
        'app.jabatans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentCategoryRole') ? [] : ['className' => DocumentCategoryRoleTable::class];
        $this->DocumentCategoryRole = TableRegistry::getTableLocator()->get('DocumentCategoryRole', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentCategoryRole);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
