<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentCategoryTable Test Case
 */
class DocumentCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentCategoryTable
     */
    public $DocumentCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_category',
        'app.instansis',
        'app.lfts',
        'app.rghts',
        'app.document_category_role'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentCategory') ? [] : ['className' => DocumentCategoryTable::class];
        $this->DocumentCategory = TableRegistry::getTableLocator()->get('DocumentCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
