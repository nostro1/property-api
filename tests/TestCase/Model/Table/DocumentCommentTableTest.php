<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentCommentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentCommentTable Test Case
 */
class DocumentCommentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentCommentTable
     */
    public $DocumentComment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_comment',
        'app.dokumen_repositoris',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentComment') ? [] : ['className' => DocumentCommentTable::class];
        $this->DocumentComment = TableRegistry::getTableLocator()->get('DocumentComment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentComment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
