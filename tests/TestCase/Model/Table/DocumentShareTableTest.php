<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentShareTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentShareTable Test Case
 */
class DocumentShareTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentShareTable
     */
    public $DocumentShare;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_share',
        'app.dokumen_repositoris',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentShare') ? [] : ['className' => DocumentShareTable::class];
        $this->DocumentShare = TableRegistry::getTableLocator()->get('DocumentShare', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentShare);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
