<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentSlocTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentSlocTable Test Case
 */
class DocumentSlocTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentSlocTable
     */
    public $DocumentSloc;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_sloc',
        'app.instansis',
        'app.document_repositories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentSloc') ? [] : ['className' => DocumentSlocTable::class];
        $this->DocumentSloc = TableRegistry::getTableLocator()->get('DocumentSloc', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentSloc);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
