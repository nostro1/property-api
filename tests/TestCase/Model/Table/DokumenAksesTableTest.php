<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DokumenAksesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DokumenAksesTable Test Case
 */
class DokumenAksesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DokumenAksesTable
     */
    public $DokumenAkses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dokumen_akses',
        'app.dokumen_repositoris',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DokumenAkses') ? [] : ['className' => DokumenAksesTable::class];
        $this->DokumenAkses = TableRegistry::getTableLocator()->get('DokumenAkses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DokumenAkses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
