<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DokumenRepositoriTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DokumenRepositoriTable Test Case
 */
class DokumenRepositoriTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DokumenRepositoriTable
     */
    public $DokumenRepositori;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dokumen_repositori',
        'app.instansis',
        'app.dokumen_akses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DokumenRepositori') ? [] : ['className' => DokumenRepositoriTable::class];
        $this->DokumenRepositori = TableRegistry::getTableLocator()->get('DokumenRepositori', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DokumenRepositori);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
