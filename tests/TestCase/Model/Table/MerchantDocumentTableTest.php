<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MerchantDocumentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MerchantDocumentTable Test Case
 */
class MerchantDocumentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MerchantDocumentTable
     */
    public $MerchantDocument;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.merchant_document',
        'app.units'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MerchantDocument') ? [] : ['className' => MerchantDocumentTable::class];
        $this->MerchantDocument = TableRegistry::getTableLocator()->get('MerchantDocument', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MerchantDocument);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
