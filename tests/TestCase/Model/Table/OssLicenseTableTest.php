<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OssLicenseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OssLicenseTable Test Case
 */
class OssLicenseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OssLicenseTable
     */
    public $OssLicense;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.oss_license',
        'app.instansis',
        'app.nibs',
        'app.osses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OssLicense') ? [] : ['className' => OssLicenseTable::class];
        $this->OssLicense = TableRegistry::getTableLocator()->get('OssLicense', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OssLicense);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
