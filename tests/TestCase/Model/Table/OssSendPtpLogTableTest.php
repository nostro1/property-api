<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OssSendPtpLogTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OssSendPtpLogTable Test Case
 */
class OssSendPtpLogTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OssSendPtpLogTable
     */
    public $OssSendPtpLog;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.oss_send_ptp_log',
        'app.instansis',
        'app.nibs',
        'app.osses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OssSendPtpLog') ? [] : ['className' => OssSendPtpLogTable::class];
        $this->OssSendPtpLog = TableRegistry::getTableLocator()->get('OssSendPtpLog', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OssSendPtpLog);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
