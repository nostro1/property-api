<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PersonPropertyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PersonPropertyTable Test Case
 */
class PersonPropertyTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PersonPropertyTable
     */
    public $PersonProperty;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.person_property',
        'app.instansis',
        'app.people',
        'app.property_units'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PersonProperty') ? [] : ['className' => PersonPropertyTable::class];
        $this->PersonProperty = TableRegistry::getTableLocator()->get('PersonProperty', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PersonProperty);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
