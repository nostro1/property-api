<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PriceCathegoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PriceCathegoryTable Test Case
 */
class PriceCathegoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PriceCathegoryTable
     */
    public $PriceCathegory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.price_cathegory',
        'app.instansis',
        'app.product_price'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PriceCathegory') ? [] : ['className' => PriceCathegoryTable::class];
        $this->PriceCathegory = TableRegistry::getTableLocator()->get('PriceCathegory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PriceCathegory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
