<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductImageTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductImageTable Test Case
 */
class ProductImageTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductImageTable
     */
    public $ProductImage;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_image',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductImage') ? [] : ['className' => ProductImageTable::class];
        $this->ProductImage = TableRegistry::getTableLocator()->get('ProductImage', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductImage);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
