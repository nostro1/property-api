<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductPriceTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductPriceTable Test Case
 */
class ProductPriceTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductPriceTable
     */
    public $ProductPrice;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_price',
        'app.products',
        'app.price_cathegories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductPrice') ? [] : ['className' => ProductPriceTable::class];
        $this->ProductPrice = TableRegistry::getTableLocator()->get('ProductPrice', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductPrice);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
