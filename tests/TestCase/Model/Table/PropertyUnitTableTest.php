<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropertyUnitTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropertyUnitTable Test Case
 */
class PropertyUnitTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropertyUnitTable
     */
    public $PropertyUnit;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.property_unit',
        'app.instansis',
        'app.projects',
        'app.person_property'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PropertyUnit') ? [] : ['className' => PropertyUnitTable::class];
        $this->PropertyUnit = TableRegistry::getTableLocator()->get('PropertyUnit', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PropertyUnit);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
