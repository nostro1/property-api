<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaskManagementTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaskManagementTable Test Case
 */
class TaskManagementTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaskManagementTable
     */
    public $TaskManagement;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.task_management',
        'app.instansis',
        'app.lfts',
        'app.rghts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TaskManagement') ? [] : ['className' => TaskManagementTable::class];
        $this->TaskManagement = TableRegistry::getTableLocator()->get('TaskManagement', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaskManagement);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
