<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TenantPropertyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TenantPropertyTable Test Case
 */
class TenantPropertyTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TenantPropertyTable
     */
    public $TenantProperty;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tenant_property',
        'app.instansis',
        'app.tenants',
        'app.property_units'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TenantProperty') ? [] : ['className' => TenantPropertyTable::class];
        $this->TenantProperty = TableRegistry::getTableLocator()->get('TenantProperty', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TenantProperty);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
