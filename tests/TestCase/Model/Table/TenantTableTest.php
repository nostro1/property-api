<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TenantTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TenantTable Test Case
 */
class TenantTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TenantTable
     */
    public $Tenant;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tenant',
        'app.instansis',
        'app.billing'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Tenant') ? [] : ['className' => TenantTable::class];
        $this->Tenant = TableRegistry::getTableLocator()->get('Tenant', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tenant);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
