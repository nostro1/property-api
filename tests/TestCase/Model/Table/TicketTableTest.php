<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TicketTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TicketTable Test Case
 */
class TicketTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TicketTable
     */
    public $Ticket;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ticket',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Ticket') ? [] : ['className' => TicketTable::class];
        $this->Ticket = TableRegistry::getTableLocator()->get('Ticket', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ticket);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
