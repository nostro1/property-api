<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkflowTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkflowTable Test Case
 */
class WorkflowTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkflowTable
     */
    public $Workflow;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.workflow',
        'app.instansis',
        'app.objeks',
        'app.jenis_proses',
        'app.forms',
        'app.template_data',
        'app.penggunas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Workflow') ? [] : ['className' => WorkflowTable::class];
        $this->Workflow = TableRegistry::getTableLocator()->get('Workflow', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Workflow);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
